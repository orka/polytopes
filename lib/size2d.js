/**
 * Describes a two (2D) dimensional Size with x and y attributes
 * Includes variety of methods to aid in manipulating positioning and in time curve mutation of both x and y property
 */
define(['p!-eventful',
    'p!-numerus',
    'p!-assertful',
    'p!-polytopes/error',
    'p!-logger'
], function($eventful,
    $numerus,
    $assertful,
    $error,
    $logger) {
    'use strict';

    var Size2d = $eventful.emitter.subclass('Size2d', {

        constructor: function(__widthNumerus, __heightNumerus) {
            //initialize super constructor
            Size2d.super.constructor.call(this);

            if (!$numerus.number.isOffspring(__widthNumerus) || !$numerus.number.isOffspring(__heightNumerus)) {
                $error($error.TYPE.TYPE_ERROR, 'Arguments invalid - using default 0/0').throw();
            } else {
                this.__widthNumerus = __widthNumerus.clone();
                this.__heightNumerus = __heightNumerus.clone();
            }

            this.width = this.__widthNumerus.__value__;
            this.height = this.__heightNumerus.__value__;

            this.__widthNumerus.addListener('change', this.__setCacheWidth__.bind(this));
            this.__heightNumerus.addListener('change', this.__setCacheHeight__.bind(this));
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Size2d.super.deconstructor.call(this);
            this.__widthNumerus.deconstructor();
            this.__heightNumerus.deconstructor();
            this.__widthNumerus = null;
            this.__heightNumerus = null;
            this.width = null;
            this.height = null;
        },

        /**
         * Clone current point with current or original positions
         * @param {Boolean} __currentPositions if true current positions are cloned
         * @returns {Object} New Instance of Point2D
         */
        clone: function(__currentPositions) {
            return Size2d(this.__widthNumerus.clone(__currentPositions), this.__heightNumerus.clone(__currentPositions));
        },


        updateSize: function () {
            this.__widthNumerus.update();
            this.__heightNumerus.update();
        },

        __setCacheWidth__: function () {
            this.width = this.__widthNumerus.__value__;
        },

        __setCacheHeight__: function () {
            this.height = this.__heightNumerus.__value__;
        },



        //---------------------------------------   SETTERS   ----------------------------------



        setWidth: function(__value, __bypassCurves) {
            this.__widthNumerus.set(__value, __bypassCurves);
            return this;
        },

        setHeight: function(__value, __bypassCurves) {
            this.__heightNumerus.set(__value, __bypassCurves);
            return this;
        },

        setSize: function(__width, __height, __bypassCurves) {
            this.setWidth(__width, __bypassCurves);
            this.setHeight(__height, __bypassCurves);
            return this;
        },


        maxWidth: function(__bypassCurves) {
            this.__widthNumerus.toMax(__bypassCurves);
            return this;
        },

        maxHeight: function(__bypassCurves) {
            this.__heightNumerus.toMax(__bypassCurves);
            return this;
        },

        maxSize: function(__bypassCurves) {
            this.maxWidth(__bypassCurves);
            this.maxHeight(__bypassCurves);
            return this;
        },

        minWidth: function(__bypassCurves) {
            this.__widthNumerus.toMin(__bypassCurves);
            return this;
        },

        minHeight: function(__bypassCurves) {
            this.__heightNumerus.toMin(__bypassCurves);
            return this;
        },

        minSize: function(__bypassCurves) {
            this.minWidth(__bypassCurves);
            this.minHeight(__bypassCurves);
            return this;
        },

        resetWidth: function(__bypassCurves) {
            this.__widthNumerus.reset(__bypassCurves);
            return this;
        },

        resetHeight: function(__bypassCurves) {
            this.__heightNumerus.reset(__bypassCurves);
            return this;
        },

        resetSize: function(__bypassCurves) {
            this.resetWidth(__bypassCurves);
            this.resetHeight(__bypassCurves);
            return this;
        },

        addWidth: function(__value, __bypassCurves) {
            this.setWidth(this.getWidth() + __value, __bypassCurves);
            return this;
        },

        addHeight: function(__value, __bypassCurves) {
            this.setHeight(this.getHeight() + __value, __bypassCurves);
            return this;
        },

        addSize: function(__width, __height, __bypassCurves) {
            this.addWidth(__width, __bypassCurves);
            this.addHeight(__height, __bypassCurves);
            return this;
        },



        //---------------------------------------   END Curve   ----------------------------------



        endWidth: function() {
            this.__widthNumerus.end();
            return this;
        },

        endHeight: function() {
            this.__heightNumerus.end();
            return this;
        },

        endSize: function() {
            this.endHeight();
            this.endWidth();
            return this;
        },



        //---------------------------------------   GETTERS   ----------------------------------



        getWidth: function() {
            return this.width;
        },

        getHeight: function() {
            return this.height;
        },

        getSize: function() {
            return {
                width: this.getWidth(),
                height: this.getHeight()
            };
        },

        getEndWidth: function() {
            return this.__widthNumerus.getEnd();
        },

        getEndHeight: function() {
            return this.__heightNumerus.getEnd();
        },

        getEndSize: function() {
            return {
                width: this.getEndWidth(),
                height: this.getEndHeight()
            };
        },

        getStartWidth: function() {
            return this.__widthNumerus.getStart();
        },

        getStartHeight: function() {
            return this.__heightNumerus.getStart();
        },

        getStartSize: function() {
            return {
                width: this.getStartWidth(),
                height: this.getStartHeight()
            };
        }
    });

    Size2d.fromCoord = function(__width, __height) {
        return Size2d($numerus.number(__width), $numerus.number(__height));
    };

    Size2d.fromBounds = function(__bounds) {
        return Size2d.fromCoord(__bounds.width, __bounds.height);
    };

    Size2d.fromArray = function(__array) {
        return Size2d.fromCoord(__array[0], __array[1]);
    };

    return Size2d;
});
