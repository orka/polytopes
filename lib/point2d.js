/**
 * Describes a two (2D) dimensional point with x and y attributes
 * Includes variety of methods to aid in manipulating positioning and in time curve mutation of both x and y property
 */
define(['p!-eventful',
    'p!-numerus',
    'p!-assertful',
    'p!-polytopes/error',
    'p!-logger'
], function($eventful,
    $numerus,
    $assertful,
    $error,
    $logger) {
    'use strict';

    var Point2d = $eventful.emitter.subclass('Point2d', {

        constructor: function(__xNumerus, __yNumerus) {
            //initialize super constructor
            Point2d.super.constructor.call(this);

            if (!$numerus.number.isOffspring(__xNumerus) || !$numerus.number.isOffspring(__yNumerus)) {
                $error($error.TYPE.TYPE_ERROR, 'Arguments invalid - using default values').throw();
            } else {
                this.__xNumerus = __xNumerus;
                this.__yNumerus = __yNumerus;
            }

            this.x = this.__xNumerus.__value__;
            this.y = this.__yNumerus.__value__;

            this.__xNumerus.addListener('change', this.__setCacheX__.bind(this));
            this.__yNumerus.addListener('change', this.__setCacheY__.bind(this));
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Point2d.super.deconstructor.call(this);
            //
            this.__xNumerus.deconstructor();
            this.__yNumerus.deconstructor();
            this.__xNumerus = null;
            this.__yNumerus = null;
            this.x = null;
            this.y = null;
        },

        /**
         * Clone current point with current or original positions
         * @param {Boolean} __currentPositions if true current positions are cloned
         * @returns {Object} New Instance of Point2D
         */
        clone: function(__currentPositions) {
            return Point2d(this.__xNumerus.clone(__currentPositions), this.__yNumerus.clone(__currentPositions));
        },


        updateCoord: function () {
            this.__xNumerus.update();
            this.__yNumerus.update();
        },

        __setCacheX__: function () {
            this.x = this.__xNumerus.__value__;
        },

        __setCacheY__: function () {
            this.y = this.__yNumerus.__value__;
        },



        //---------------------------------------   SETTERS   ----------------------------------



        setX: function(__value, __bypassCurves) {
            this.__xNumerus.set(__value, __bypassCurves);
            return this;
        },

        setY: function(__value, __bypassCurves) {
            this.__yNumerus.set(__value, __bypassCurves);
            return this;
        },

        setCoord: function(__x, __y, __bypassCurves) {
            this.setX(__x, __bypassCurves);
            this.setY(__y, __bypassCurves);
            return this;
        },


        maxX: function(__bypassCurves) {
            this.__xNumerus.toMax(__bypassCurves);
            return this;
        },

        maxY: function(__bypassCurves) {
            this.__yNumerus.toMax(__bypassCurves);
            return this;
        },

        maxCoord: function(__bypassCurves) {
            this.maxX(__bypassCurves);
            this.maxY(__bypassCurves);
            return this;
        },

        minX: function(__bypassCurves) {
            this.__xNumerus.toMin(__bypassCurves);
            return this;
        },

        minY: function(__bypassCurves) {
            this.__yNumerus.toMin(__bypassCurves);
            return this;
        },

        minCoord: function(__bypassCurves) {
            this.minX(__bypassCurves);
            this.minY(__bypassCurves);
            return this;
        },

        resetX: function(__bypassCurves) {
            this.__xNumerus.reset(__bypassCurves);
            return this;
        },

        resetY: function(__bypassCurves) {
            this.__yNumerus.reset(__bypassCurves);
            return this;
        },

        resetCoord: function(__bypassCurves) {
            this.resetX(__bypassCurves);
            this.resetY(__bypassCurves);
            return this;
        },

        translateX: function(__value, __bypassCurves) {
            this.setX(this.getX() + __value, __bypassCurves);
            return this;
        },

        translateY: function(__value, __bypassCurves) {
            this.setY(this.getY() + __value, __bypassCurves);
            return this;
        },

        translateCoord: function(__x, __y, __bypassCurves) {
            this.translateX(__x, __bypassCurves);
            this.translateY(__y, __bypassCurves);
            return this;
        },



        //---------------------------------------   END Curve   ----------------------------------



        endX: function() {
            this.__xNumerus.end();
            return this;
        },

        endY: function() {
            this.__yNumerus.end();
            return this;
        },

        endCoord: function() {
            this.endY();
            this.endX();
            return this;
        },



        //---------------------------------------   GETTERS   ----------------------------------



        getX: function() {
            return this.x;
        },

        getY: function() {
            return this.y;
        },

        getCoord: function() {
            return {
                x: this.getX(),
                y: this.getY()
            };
        },

        getEndX: function() {
            return this.__xNumerus.getEnd();
        },

        getEndY: function() {
            return this.__yNumerus.getEnd();
        },

        getEndCoord: function() {
            return {
                x: this.getEndX(),
                y: this.getEndY()
            };
        },

        getStartX: function() {
            return this.__xNumerus.getStart();
        },

        getStartY: function() {
            return this.__yNumerus.getStart();
        },

        getStartCoord: function() {
            return {
                x: this.getStartX(),
                y: this.getStartY()
            };
        }
    });

    Point2d.fromCoord = function(__x, __y) {
        return Point2d($numerus.number(__x), $numerus.number(__y));
    };

    Point2d.fromBounds = function(__bounds) {
        return Point2d.fromCoord(__bounds.x, __bounds.y);
    };

    Point2d.fromArray = function(__array) {
        return Point2d.fromCoord(__array[0], __array[1]);
    };

    return Point2d;
});
