/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        INVALID_NESTING_ACTION: 'INVALID_NESTING_ACTION',
        INVALID_RELATIVE: 'INVALID_RELATIVE',
        PARENT_NOT_FOUND: 'PARENT_NOT_FOUND'
    };

    var ErrorClass = $error.subclass('PolytopesError', {
        constructor: function() {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
