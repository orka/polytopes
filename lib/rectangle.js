/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define([
    'p!-polytopes/point2d',
    'p!-polytopes/size2d',
    'p!-polytopes/position2d',
    'p!-polytopes/rectExt/main',
    'p!-polytopes/error',
    'p!-eventful',
    'p!-numerus',
    'p!-assertful',
    'p!-logger'
], function(
    $point2d,
    $size2d,
    $position2d,
    $rectExt,
    $error,
    $eventful,
    $numerus,
    $assertful,
    $logger) {
    'use strict';

    var Rectangle = $position2d.subclass('Rectangle', {

        /**
         * Rectangle class constructor
         * @class Rectangle
         * @param   {Object} __xNumerus       X position :Numerus
         * @param   {Object} __yNumerus       Y position :Numerus
         * @param   {Object} __widthNumerus   Width :Numerus
         * @param   {Object} __heightNumerus  Height :Numerus
         * @param   {Object} __xOriginNumerus X Origin :Numerus
         * @param   {Object} __yOriginNumerus Y Origin :Numerus
         */
        constructor: function(__xNumerus, __yNumerus, __widthNumerus, __heightNumerus, __xOriginNumerus, __yOriginNumerus) {
            Rectangle.super.constructor.call(this, __xNumerus, __yNumerus, __xOriginNumerus, __yOriginNumerus);
            $size2d.prototype.constructor.call(this, __widthNumerus, __heightNumerus);
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Rectangle.super.deconstructor.call(this);
            $size2d.prototype.deconstructor.call(this);
        },

        /**
         * Create object with identical parameters that of current instance.
         * @param   {Boolean} __currentPositions if true - all current values are cloned, else - initial values are cloned
         * @returns {Object}   new Instance of Rectangle Class
         */
        clone: function(__currentPositions) {
            return Rectangle(this.__xNumerus.clone(__currentPositions), this.__yNumerus.clone(__currentPositions), this.__widthNumerus.clone(__currentPositions), this.__heightNumerus.clone(__currentPositions), this.__originXNumerus.clone(__currentPositions), this.__originYNumerus.clone(__currentPositions));
        },

        resetAll: function(__bypassCurves) {
            this.resetPosition(__bypassCurves);
            this.resetSize(__bypassCurves);
            return this;
        },

        updateBounds: function() {
            this.updateSize();
            this.updatePosition();
            return this;
        },

        /**
         * By default all parent get an update too
         * @returns {Object} self
         */
        update: function() {
            this.updateBounds();
            if (this.__parent__) {
                this.__parent__.update();
            }
            return this;
        },



        //------------------------------------      PARENT/CHILD/INSTANCE - TESTS



        isValidRectInstance: function(__rect) {
            if (Rectangle.isOffspring(__rect)) {
                return true;
            }
            return false;
        },

        hasLinkTo: function(__rect) {
            if (!this.__parent__) {
                return false;
            }

            if (this.__parent__ === __rect) {
                return true;
            }

            return this.__parent__.hasLinkTo(__rect);
        },

        /**
         * Strict test of immediate relations
         * @param   {Object}  __rect Instance of Rectangle
         * @returns {Boolean}              true if instance is child of rect object
         */
        isChildOf: function(__rect) {
            if (!__rect || !this.__parent__ || this === __rect || this.__parent__ !== __rect) {
                return false;
            }

            return true;
        },

        /**
         * Strict test of immediate relations
         * @param   {Object}  __childRect Instance of Rectangle
         * @returns {Boolean}  true if argument is a child instance object
         */
        isParentOf: function(__childRect) {
            if (!__childRect || !__childRect.__parent__ || this === __childRect || __childRect.__parent__ !== this) {
                return false;
            }
            return true;
        },



        //------------------------  PARENT MANAGMENT



        /**
         * Set parent to rectangle creating hierarchy to help track positions of multiple nesting of objects
         * @event {error} - if provided Object is not an instance of rectangle class or equal to self
         * @event {parent} - when new parent is set, including overrides
         * @param {Object} __parentRect Instance of Rectangle
         * @returns {Object} self
         */
        setParent: function(__parentRect) {
            if (this.invalidRectError(__parentRect)) {
                return this;
            }

            //emit event in case of override or a new parent
            if (this.__parent__ !== __parentRect) {
                this.emit('parent', __parentRect);
            }

            this.__parent__ = __parentRect;

            return this;
        },

        /**
         * A simple parent getter
         * @returns {Object|undefined} parent object, instance of rectangle
         */
        getParent: function() {
            return this.__parent__;
        },



        //--------------------------------------    SETTERS - only missing APIs



        /**
         * Sets right edge to the supplied value
         * @param {Number} __value Right edge
         * @param {Boolean | NORU} __bypassCurves flag to bypass curves
         * @returns {Object} self
         */
        setRight: function(__value, __bypassCurves) {
            this.setLeft(__value - this.getEndWidth(), __bypassCurves);
            return this;
        },

        /**
         * Sets bottom edge to the supplied value
         * @param {Number} __value bottom edge
         * @param {Boolean | NORU} __bypassCurves flag to bypass curves
         * @returns {Object} self
         */
        setBottom: function(__value, __bypassCurves) {
            this.setTop(__value - this.getEndHeight(), __bypassCurves);
            return this;
        },

        /**
         * Translates right edge to the supplied value
         * @param {Number} __value Right edge
         * @param {Boolean | NORU} __bypassCurves flag to bypass curves
         * @returns {Object} self
         */
        translateRight: function(__value, __bypassCurves) {
            this.translateLeft(__value - this.getEndWidth(), __bypassCurves);
            return this;
        },

        /**
         * Translates bottom edge to the supplied value
         * @param {Number} __value bottom edge
         * @param {Boolean | NORU} __bypassCurves flag to bypass curves
         * @returns {Object} self
         */
        translateBottom: function(__value, __bypassCurves) {
            this.translateTop(__value - this.getEndHeight(), __bypassCurves);
            return this;
        },

        /**
         * Assigns values from the bounds Object
         * @param {Object} __boudns       Bounds Object containing left|top|Width|height
         * @param {Boolean | NORU} __bypassCurves Flag to bypass curves
         * @returns {Object} self
         */
        setBounds: function(__boudns, __bypassCurves) {
            this.setLeft(__boudns.left, __bypassCurves)
                .setTop(__boudns.top, __bypassCurves)
                .setWidth(__boudns.width, __bypassCurves)
                .setHeight(__boudns.height, __bypassCurves);
            return this;
        },

        /**
         * Assigns values from the Array Object
         * @param {Array} __boudns       Bounds Array containing left|top|Width|height in that order
         * @param {Boolean | NORU} __bypassCurves Flag to bypass curves
         * @returns {Object} self
         */
        setArrayBounds: function(__boudns, __bypassCurves) {
            this.setLeft(__boudns[0], __bypassCurves)
                .setTop(__boudns[1], __bypassCurves)
                .setWidth(__boudns[2], __bypassCurves)
                .setHeight(__boudns[3], __bypassCurves);
            return this;
        },



        //--------------------------------------    GETTERS



        //--------------    Right, Bottom


        /**
         * Gets Right edge position with origin in mind
         * @returns {Number} right edge position
         */
        getRight: function() {
            return this.getLeft() + this.getWidth();
        },

        /**
         * Gets Bottom edge position with origin in mind
         * @returns {Number} bottom edge position
         */
        getBottom: function() {
            return this.getTop() + this.getHeight();
        },

        /**
         * Creates simple object left, top, right, bottom defined
         * @returns {Object} with current position values
         */
        getBounds: function() {
            return {
                left: this.getLeft(),
                top: this.getTop(),
                right: this.getRight(),
                bottom: this.getBottom()
            };
        },

        /**
         * Creates simple object left, top, right, bottom defined
         * @returns {Object} with current position values
         */
        getData: function() {
            return {
                left: this.getLeft(),
                top: this.getTop(),
                right: this.getRight(),
                bottom: this.getBottom(),
                x: this.getX(),
                y: this.getY(),
                width: this.getWidth(),
                height: this.getHeight(),
                originX: this.getOriginX(),
                originY: this.getOriginY()
            };
        },

        /**
         * Retrieves the End Right position of rectangle
         * @returns {Object} self
         */
        getEndRight: function() {
            return this.getEndLeft() + this.getEndWidth();
        },

        /**
         * Retrieve the End Bottom position of rectangle
         * @returns {Object} self
         */
        getEndBottom: function() {
            return this.getEndTop() + this.getEndHeight();
        },

        /**
         * Retrieves the End values of top, left, right, bottom of rectangle
         * @returns {Object} with top, left, right, bottom attributes
         */
        getEndBounds: function() {
            return {
                left: this.getEndLeft(),
                top: this.getEndTop(),
                right: this.getEndRight(),
                bottom: this.getEndBottom()
            };
        },

        /**
         * Creates simple object left, top, right, bottom defined
         * @returns {Object} with current position values
         */
        getEndData: function() {
            return {
                left: this.getEndLeft(),
                top: this.getEndTop(),
                right: this.getEndRight(),
                bottom: this.getEndBottom(),
                x: this.getEndX(),
                y: this.getEndY(),
                width: this.getEndWidth(),
                height: this.getEndHeight(),
                originX: this.getOriginEndX(),
                originY: this.getOriginEndY()
            };
        }
    });

    //premix size API
    Rectangle.premix($size2d.prototype);
    //mix Extensions
    Rectangle.mixin($rectExt.errorFunctions);
    Rectangle.mixin($rectExt.rectangleRelativeGetters);
    Rectangle.mixin($rectExt.rectangleRelativeEndGetters);
    Rectangle.mixin($rectExt.rectangleGlobalGetters);
    //intersections
    Rectangle.mixin($rectExt.rectangleParentIntersections);
    Rectangle.mixin($rectExt.rectangleParentEndIntersections);
    Rectangle.mixin($rectExt.rectangleRelativeIntersections);
    Rectangle.mixin($rectExt.rectangleRelativeEndIntersections);
    //positions
    Rectangle.mixin($rectExt.rectanglePositionTests);
    Rectangle.mixin($rectExt.rectangleGlobalPositionTests);
    Rectangle.mixin($rectExt.rectangleEndPositionTests);
    Rectangle.mixin($rectExt.rectangleGlobalEndPositionTests);
    //bonus
    Rectangle.mixin($rectExt.rectangleCenterGetters);
    Rectangle.mixin($rectExt.rectangleScaleFunctions);



    //CREATE METHODS


    Rectangle.fromCoord = function(__x, __y, __width, __height, __xo, __yo) {
        if (__xo && __yo) {
            Rectangle($numerus.number(__x), $numerus.number(__y), $numerus.number(__width), $numerus.number(__height), $numerus.number(__xo), $numerus.number(__yo));
        }
        return Rectangle($numerus.number(__x), $numerus.number(__y), $numerus.number(__width), $numerus.number(__height));
    };

    Rectangle.fromBases = function(__position, __size) {
        return Rectangle(__position.x.clone(), __position.y.clone(), __size.width.clone(), __size.height.clone(), __position.originX.clone(), __position.originY.clone());
    };

    Rectangle.fromBounds = function(__bounds) {
        return Rectangle.fromCoord(__bounds.x, __bounds.y, __bounds.width, __bounds.height, __bounds.originX, __bounds.originY);
    };

    Rectangle.fromArray = function(__array) {
        return Rectangle.fromCoord(__array[0], __array[1], __array[2], __array[3], __array[4], __array[5]);
    };

    return Rectangle;
});
