/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleEndPositionTests = {



        //--------- POSITION TESTS



        /**
         * Tests rect to be within the Y value
         * @public
         * @param   {Number}  __y Y coord
         * @returns {Boolean} true - if __y equal to y coord and less than bottom value
         */
        isOfEndY: function(__y) {
            if (this.badValueError(__y)) {
                return false;
            }
            return this.__isOfEndY__(__y);
        },

        /**
         * Tests rect to be within the Y value
         * @private
         * @param   {Number}  __y Y coord
         * @returns {Boolean} true - if __y equal to y coord and less than bottom value
         */
        __isOfEndY__: function(__y) {
            return this.getEndTop() <= __y && this.getEndBottom() > __y;
        },

        /**
         * Tests rect to be within the X value
         * @public
         * @param   {Number}  __x x coord
         * @returns {Boolean} true - if __x equal to x coord and less than right value
         */
        isOfEndX: function(__x) {
            if (this.badValueError(__x)) {
                return false;
            }
            return this.__isOfEndX__(__x);
        },

        /**
         * Tests rect to be within the X value
         * @private
         * @param   {Number}  __x x coord
         * @returns {Boolean} true - if __x equal to x coord and less than right value
         */
        __isOfEndX__: function(__x) {
            return this.getEndLeft() <= __x && this.getEndRight() > __x;
        },

        /**
         * Tests rect to be within the X|Y value
         * @public
         * @param   {Number}  __x x coord
         * @param   {Number}  __y y coord
         * @returns {Boolean} true - if __x equal to x coord and less than right value AND __y equal to y coord and less than bottom value
         */
        isOfEndCoord: function(__x, __y) {
            if (this.badValueError(__x) || this.badValueError(__y)) {
                return false;
            }
            return this.__isOfEndCoord__(__x, __y);
        },

        /**
         * Tests rect to be within the X|Y value
         * @private
         * @param   {Number}  __x x coord
         * @param   {Number}  __y y coord
         * @returns {Boolean} true - if __x equal to x coord and less than right value AND __y equal to y coord and less than bottom value
         */
        __isOfEndCoord__: function(__x, __y) {
            return this.__isOfEndX__(__x) && this.__isOfEndY__(__y);
        }
    };

    return rectangleEndPositionTests;
});
