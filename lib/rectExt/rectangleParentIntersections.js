/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function($error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleParentIntersections = {



        //partially intersecting



        //vertical tests



        /**
         * Tests vertical intersect with own parent
         * @public
         * @returns {Boolean} true if partially or fully in vertival bounds of parent
         */
        isInParentVertBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isInParentVertBounds__();
        },

        /**
         * Tests vertical intersect with own parent
         * @private
         * @returns {Boolean} true if partially or fully in vertival bounds of parent
         */
        __isInParentVertBounds__: function() {
            //if this bottom side is out of the top side of parent or top side is out of the bottom side of parent
            //or left side is out of the right side of __parent__
            if (this.getBottom() <= 0 || this.getTop() >= this.__parent__.getHeight()) {
                return false;
            }
            return true;
        },



        //Horizintal tests



        /**
         * Tests horizontal intersect with own parent
         * @public
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        isInParentHorBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isInParentHorBounds__();
        },

        /**
         * Tests horizontal intersect with own parent
         * @private
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        __isInParentHorBounds__: function() {
            //if this right side is out of the left side of __parent__
            //or left side is out of the right side of __parent__
            if (this.getRight() <= 0 || this.getLeft() >= this.__parent__.getWidth()) {
                return false;
            }
            return true;
        },



        //Both



        /**
         * Tests if intersect with own parent
         * @public
         * @param {Boolean} __bothAxis Flag to perform strict tests on both axis planes
         * @returns {Boolean} true if partially or fully in bounds of parent
         */
        isInParentBounds: function(__bothAxis) {
            if (this.noParentError() || this.badFlagError(__bothAxis)) {
                return false;
            }
            return this.__isInParentBounds__(__bothAxis);
        },

        /**
         * Tests if intersect with own parent
         * @private
         * @param {Boolean} __bothAxis Flag to perform strict tests on both axis planes
         * @returns {Boolean} true if partially or fully in bounds of parent
         */
        __isInParentBounds__: function(__bothAxis) {
            if (__bothAxis) {
                //strict mode
                return this.__isInParentHorBounds__() && this.__isInParentVertBounds__();
            }
            //either vert or hor
            return this.__isInParentHorBounds__() || this.__isInParentVertBounds__();
        },



        //----------    Fully in bounds inersections



        //vertical



        /**
         * Tests vertical position to own parent
         * @public
         * @returns {Boolean} true if fully in vertival bounds of parent
         */
        isFullyInParentVertBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isFullyInParentVertBounds__();
        },

        /**
         * Tests vertical position to own parent
         * @private
         * @returns {Boolean} true if fully in vertival bounds of parent
         */
        __isFullyInParentVertBounds__: function() {
            //if this bottom side is out of the top side of parent
            //or top side is out of the bottom side of parent
            if (this.getTop() < 0 || this.getBottom() > this.__parent__.getHeight()) {
                return false;
            }
            return true;
        },

        /**
         * Tests vertical position to own parent
         * @public
         * @returns {Boolean} true if fully in horisontal bounds of parent
         */
        isFullyInParentHorBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isFullyInParentHorBounds__();
        },

        /**
         * Tests vertical position to own parent
         * @private
         * @returns {Boolean} true if fully in horisontal bounds of parent
         */
        __isFullyInParentHorBounds__: function() {
            //if this right side is out of the left side of __parent__
            //or left side is out of the right side of __parent__
            if (this.getLeft() < 0 || this.getRight() > this.__parent__.getWidth()) {
                return false;
            }
            return true;
        },

        /**
         * Tests position to own parent
         * @public
         * @param {Boolean} __eitherAxis Flag to loosen the test to be on either axis
         * @returns {Boolean} true if fully in bounds of parent
         */
        isFullyInParentBounds: function(__eitherAxis) {
            if (this.noParentError() || this.badFlagError(__eitherAxis)) {
                return false;
            }
            return this.__isFullyInParentBounds__(__eitherAxis);
        },

        /**
         * Tests position to own parent
         * @private
         * @param {Boolean} __eitherAxis Flag to loosen the test to be on either axis
         * @returns {Boolean} true if fully in bounds of parent
         */
        __isFullyInParentBounds__: function(__eitherAxis) {
            if (__eitherAxis) {
                //loose mode
                return this.__isFullyInParentHorBounds__() || this.__isFullyInParentVertBounds__();
            }
            //strict mode - default
            return this.__isFullyInParentHorBounds__() && this.__isFullyInParentVertBounds__();
        }
    };


    return rectangleParentIntersections;
});
