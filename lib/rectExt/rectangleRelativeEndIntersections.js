/**
 * Rect extention that enables rect to test reative intersecion
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function($error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleRelativeEndIntersections = {



        //partially intersecting



        //vertical tests



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        isInRelativeVertEndBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }

            return this.__isInRelativeVertEndBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        __isInRelativeVertEndBounds__: function(__rectangle) {
            //if this bottom side is out of the top side of parent
            if (this.__getRelativeEndBottom__(__rectangle) <= 0 || this.__getRelativeEndTop__(__rectangle) >= __rectangle.getEndHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        isInRelativeHorEndBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isInRelativeHorEndBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        __isInRelativeHorEndBounds__: function(__rectangle) {
            //if this right side is out of the left side of parent
            if (this.__getRelativeEndRight__(__rectangle) <= 0 || this.__getRelativeEndLeft__(__rectangle) >= __rectangle.getEndWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __bothAxis Flag to test in both axis - strict
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        isInRelativeEndBounds: function(__rectangle, __bothAxis) {
            if (this.noLinkError(__rectangle) || this.badFlagError(__bothAxis)) {
                return false;
            }
            return this.__isInRelativeEndBounds__(__rectangle, __bothAxis);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __bothAxis Flag to enforce the strict test to pass on both axis
         * @returns {Boolean} true if partially or fully in bounds of parent
         */
        __isInRelativeEndBounds__: function(__rectangle, __bothAxis) {
            if (__bothAxis) {
                //strict mode
                return this.__isInRelativeHorEndBounds__(__rectangle) && this.__isInRelativeVertEndBounds__(__rectangle);
            }
            //loose mode
            return this.__isInRelativeHorEndBounds__(__rectangle) || this.__isInRelativeVertEndBounds__(__rectangle);
        },



        //--------- fully in bounds



        //vert



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        isFullyInRelativeVertEndBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isFullyInRelativeVertEndBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        __isFullyInRelativeVertEndBounds__: function(__rectangle) {
            //if this bottom side is out of the top side of parent
            if (this.__getRelativeEndTop__(__rectangle) < 0 || this.__getRelativeEndBottom__(__rectangle) > __rectangle.getEndHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        isFullyInRelativeHorEndBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isFullyInRelativeHorEndBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        __isFullyInRelativeHorEndBounds__: function(__rectangle) {
            //if this right side is out of the left side of parent
            if (this.__getRelativeEndLeft__(__rectangle) < 0 || this.__getRelativeEndRight__(__rectangle) > __rectangle.getEndWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __eitherAxis Flag to test in either axis - non strict
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        isFullyInRelativeEndBounds: function(__rectangle, __eitherAxis) {
            if (this.noLinkError(__rectangle) || this.badFlagError(__eitherAxis)) {
                return false;
            }
            return this.__isFullyInRelativeEndBounds__(__rectangle, __eitherAxis);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __eitherAxis Flag to loosen the test to pass on either axis
         * @returns {Boolean} true if fully in bounds of parent
         */
        __isFullyInRelativeEndBounds__: function(__rectangle, __eitherAxis) {
            if (__eitherAxis) {
                //loose mode
                return this.__isFullyInRelativeHorEndBounds__(__rectangle) || this.__isFullyInRelativeVertEndBounds__(__rectangle);
            }
            //strict mode - defaults
            return this.__isFullyInRelativeHorEndBounds__(__rectangle) && this.__isFullyInRelativeVertEndBounds__(__rectangle);
        }
    };

    return rectangleRelativeEndIntersections;
});
