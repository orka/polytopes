/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function($error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleParentEndIntersections = {

        //end-Partial


        //vertical



        /**
         * Tests vertical intersect with own parent at the end curve of both
         * @public
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        isInParentVertEndBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isInParentVertEndBounds__();
        },

        /**
         * Tests vertical intersect with own parent at the end curve of both
         * @private
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        __isInParentVertEndBounds__: function() {
            //if this bottom side is out of the top side of parent
            //or top side is out of the bottom side of parent
            if (this.getEndBottom() <= 0 || this.getEndTop() >= this.__parent__.getEndHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests horizontal intersect to own parent at the end curve of both
         * @public
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        isInParentHorEndBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isInParentHorEndBounds__();
        },

        /**
         * Tests horizontal intersect to own parent at the end curve of both
         * @private
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        __isInParentHorEndBounds__: function() {
            //if this right side is out of the left side of __parent__
            //or left side is out of the right side of __parent__
            if (this.getEndRight() <= 0 || this.getEndLeft() >= this.__parent__.getEndWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to own parent at the end curve of both
         * @public
         * @param {Boolean} __bothAxis Flag to enforce strict mode test on both axis
         * @returns {Boolean} true if partially or fully bounds of parent
         */
        isInParentEndBounds: function(__bothAxis) {
            if (this.noParentError() || this.badFlagError(__bothAxis)) {
                return false;
            }
            return this.__isInParentEndBounds__(__bothAxis);
        },

        /**
         * Tests intersect to own parent at the end curve of both
         * @param {Boolean} __bothAxis Flag to enforce strict mode test on both axis
         * @returns {Boolean} true if partially or fully bounds of parent
         */
        __isInParentEndBounds__: function(__bothAxis) {
            if (__bothAxis) {
                //trict mode
                return this.__isInParentHorEndBounds__() && this.__isInParentVertEndBounds__();
            }
            //loos mode - default
            return this.__isInParentHorEndBounds__() || this.__isInParentVertEndBounds__();
        },



        //fully - end



        //vert



        /**
         * Tests vertical intersect to own parent at the end curve of both
         * @public
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        isFullyInParentVertEndBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isFullyInParentVertEndBounds__();
        },

        /**
         * Tests vertical intersect to own parent at the end curve of both
         * @private
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        __isFullyInParentVertEndBounds__: function() {
            //if this bottom side is out of the top side of parent
            //or top side is out of the bottom side of parent
            if (this.getEndTop() < 0 || this.getEndBottom() > this.__parent__.getEndHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests horizontal intersect to own parent at the end curve of both
         * @public
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        isFullyInParentHorEndBounds: function() {
            if (this.noParentError()) {
                return false;
            }
            return this.__isFullyInParentHorEndBounds__();
        },

        /**
         * Tests horizontal intersect to own parent at the end curve of both
         * @private
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        __isFullyInParentHorEndBounds__: function() {
            //if this right side is out of the left side of __parent__
            //or left side is out of the right side of __parent__
            if (this.getEndLeft() < 0 || this.getEndRight() > this.__parent__.getEndWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to own parent at the end curve of both
         * @public
         * @param {Boolean} __eitherAxis Flag to loosen the test to pass on either axis
         * @returns {Boolean} true if fully bounds of parent
         */
        isFullyInParentEndBounds: function(__eitherAxis) {
            if (this.noParentError() || this.badFlagError(__eitherAxis)) {
                return false;
            }
            return this.__isFullyInParentEndBounds__(__eitherAxis);
        },

        /**
         * Tests intersect to own parent at the end curve of both
         * @private
         * @param {Boolean} __eitherAxis Flag to loosen the test to pass on either axis
         * @returns {Boolean} true if fully bounds of parent
         */
        __isFullyInParentEndBounds__: function(__eitherAxis) {
            if (__eitherAxis) {
                //loose mode
                return this.__isFullyInParentHorEndBounds__() || this.__isFullyInParentVertEndBounds__();
            }
            //strict mode - default
            return this.__isFullyInParentHorEndBounds__() && this.__isFullyInParentVertEndBounds__();
        }
    };


    return rectangleParentEndIntersections;
});
