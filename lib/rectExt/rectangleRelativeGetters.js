/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleRelativeGetters = {

        /**
         * Iterates through parent rectangles to retrieve own X position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global X position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own X position is returned
         *  @public
         * @param {Object} __rect one of parents of current instance relative to which X position is calculated
         * @returns {Number} relative | own | global X position
         */
        getRelativeX: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getX();
            }

            return this.__getRelativeX__(__rect);
        },

        /**
         * Optimized version of "getRelativeX" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which X position is calculated
         * @returns {Number} relative | own | global X position
         */
        __getRelativeX__: function(__rect) {
            if (__rect !== this) {
                //we'll get position relative to the left position of all parents
                return this.getX() + this.__parent__.__getRelativeLeft__(__rect);
            }
            //must return 0 since we are looking a position relative to __rect
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve Y position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Y position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Y position is returned
         *  @public
         * @param {Object} __rect one of parents of current instance relative to which Y position is calculated
         * @returns {Number} relative | own | global Y position
         */
        getRelativeY: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getY();
            }
            return this.__getRelativeY__(__rect);
        },

        /**
         * Optimized version of "getRelativeY" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Y position is calculated
         * @returns {Number} relative | own | global Y position
         */
        __getRelativeY__: function(__rect) {
            if (__rect !== this) {
                //we'll get position relative to the top position of all parents
                return this.getY() + this.__parent__.__getRelativeTop__(__rect);
            }
            //must return 0 since we are looking a position relative to __rect
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve Left position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Left position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Left position is returned
         *  @public
         * @param {Object} __rect one of parents of current instance relative to which Left position is calculated
         * @returns {Number} relative | own | global Left position
         */
        getRelativeLeft: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getLeft();
            }
            return this.__getRelativeLeft__(__rect);
        },

        /**
         * Optimized version of "getRelativeLeft" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Left position is calculated
         * @returns {Number} relative | own | global Left position
         */
        __getRelativeLeft__: function(__rect) {
            if (__rect !== this) {
                //we'll get position relative to the left position of all parents
                return this.getLeft() + this.__parent__.__getRelativeLeft__(__rect);
            }
            //must return 0 since we are looking a position relative to __rect
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve Top position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Top position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Top position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which Top position is calculated
         * @returns {Number} relative | own | global Top position
         */
        getRelativeTop: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getTop();
            }

            return this.__getRelativeTop__(__rect);
        },

        /**
         * Optimized version of "getRelativeTop" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Top position is calculated
         * @returns {Number} relative | own | global Top position
         */
        __getRelativeTop__: function(__rect) {
            if (__rect !== this) {
                //we'll get position relative to the left position of all parents
                return this.getTop() + this.__parent__.__getRelativeTop__(__rect);
            }
            //must return 0 since we are looking a position relative to __rect
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve Right position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Right position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Right position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which Right position is calculated
         * @returns {Number} relative | own | global Right position
         */
        getRelativeRight: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getRight();
            }
            return this.__getRelativeRight__(__rect);
        },

        /**
         * Optimized version of "getRelativeRight" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Right position is calculated
         * @returns {Number} relative | own | global Right position
         */
        __getRelativeRight__: function(__rect) {
            if (__rect !== this) {
                return this.getRight() + this.__parent__.__getRelativeLeft__(__rect);
            }
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve Bottom position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Bottom position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Bottom position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which Bottom position is calculated
         * @returns {Number} relative | own | global Bottom position
         */
        getRelativeBottom: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getBottom();
            }
            return this.__getRelativeBottom__(__rect);
        },

        /**
         * Optimized version of "getRelativeBottom" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Bottom position is calculated
         * @returns {Number} relative | own | global Bottom position
         */
        __getRelativeBottom__: function(__rect) {
            if (__rect !== this) {
                return this.getBottom() + this.__parent__.__getRelativeTop__(__rect);
            }
            return 0;
        },

        /**
         * Creates Bounds Object with x,y,width, height attributes with values relative to supplied
         * parent rectangle
         * @public
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} x,y,width,height relative to parent rectangle, if a rectangle not valid 0 own bounds are returned
         */
        getRelativeBounds: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getBounds();
            }
            return this.__getRelativeBounds__(__rect);
        },

        /**
         * Optimized version of "getRelativeBounds"
         * Creates Bounds Object with x,y,width, height attributes with values relative to supplied
         * parent rectangle
         * @private
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} x,y,width,height relative to parent rectangle
         */
        __getRelativeBounds__: function(__rect) {
            return {
                left: this.__getRelativeLeft__(__rect),
                top: this.__getRelativeTop__(__rect),
                right: this.__getRelativeRight__(__rect),
                bottom: this.__getRelativeBottom__(__rect)
            };
        },

        /**
         * Creates full version of Bounds Object adding right, bottom attributes with values relative to supplied
         * parent rectangle
         * @public
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} x,y,width,height,right,bottom relative to parent rectangle, if a rectangle not valid - own bounds are returned
         */
        getRelativeData: function(__rect) {
            //test for errors as a public API
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getData();
            }
            return this.__getRelativeData__(__rect);
        },

        /**
         * Optimized version of "getRelativeData"
         * Creates a full Bounds Object with end values adding right, bottom attributes with values relative to supplied
         * parent rectangle
         * @private
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} end values of x,y,width,height,right,bottom  relative to parent rectangle
         */
        __getRelativeData__: function(__rect) {
            return {
                left: this.__getRelativeLeft__(__rect),
                top: this.__getRelativeTop__(__rect),
                right: this.__getRelativeRight__(__rect),
                bottom: this.__getRelativeBottom__(__rect),
                x: this.__getRelativeX__(__rect),
                y: this.__getRelativeY__(__rect),
                width: this.getWidth(),
                height: this.getHeight(),
                originX: this.getOriginX(),
                originY: this.getOriginY()
            };
        }
    };

    return rectangleRelativeGetters;
});
