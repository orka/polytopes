/**
 * Rect extention that enables rect to test reative intersecion
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function($error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleRelativeIntersections = {



        //partially intersecting



        //vertical tests



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        isInRelativeVertBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }

            return this.__isInRelativeVertBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        __isInRelativeVertBounds__: function(__rectangle) {
            //if this bottom side is out of the top side of parent
            if (this.__getRelativeBottom__(__rectangle) <= 0 || this.__getRelativeTop__(__rectangle) >= __rectangle.getHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        isInRelativeHorBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isInRelativeHorBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if partially or fully in horizontal bounds of parent
         */
        __isInRelativeHorBounds__: function(__rectangle) {
            //if this right side is out of the left side of parent
            if (this.__getRelativeRight__(__rectangle) <= 0 || this.__getRelativeLeft__(__rectangle) >= __rectangle.getWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __bothAxis Flag to test in both axis - strict
         * @returns {Boolean} true if partially or fully in vertical bounds of parent
         */
        isInRelativeBounds: function(__rectangle, __bothAxis) {
            if (this.noLinkError(__rectangle) || this.badFlagError(__bothAxis)) {
                return false;
            }
            return this.__isInRelativeBounds__(__rectangle, __bothAxis);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __bothAxis Flag to enforce the strict test to pass on both axis
         * @returns {Boolean} true if partially or fully in bounds of parent
         */
        __isInRelativeBounds__: function(__rectangle, __bothAxis) {
            if (__bothAxis) {
                //strict mode
                return this.__isInRelativeHorBounds__(__rectangle) && this.__isInRelativeVertBounds__(__rectangle);
            }
            //loose mode
            return this.__isInRelativeHorBounds__(__rectangle) || this.__isInRelativeVertBounds__(__rectangle);
        },



        //--------- fully in bounds



        //vert



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        isFullyInRelativeVertBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isFullyInRelativeVertBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        __isFullyInRelativeVertBounds__: function(__rectangle) {
            //if this bottom side is out of the top side of parent
            if (this.__getRelativeTop__(__rectangle) < 0 || this.__getRelativeBottom__(__rectangle) > __rectangle.getHeight()) {
                return false;
            }
            return true;
        },



        //hor



        /**
         * Tests vertical intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        isFullyInRelativeHorBounds: function(__rectangle) {
            if (this.noLinkError(__rectangle)) {
                return false;
            }
            return this.__isFullyInRelativeHorBounds__(__rectangle);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @returns {Boolean} true if fully in horizontal bounds of parent
         */
        __isFullyInRelativeHorBounds__: function(__rectangle) {
            //if this right side is out of the left side of parent
            if (this.__getRelativeLeft__(__rectangle) < 0 || this.__getRelativeRight__(__rectangle) > __rectangle.getWidth()) {
                return false;
            }
            return true;
        },



        //both



        /**
         * Tests intersect to specified parent
         * @public
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __eitherAxis Flag to test in either axis - non strict
         * @returns {Boolean} true if fully in vertical bounds of parent
         */
        isFullyInRelativeBounds: function(__rectangle, __eitherAxis) {
            if (this.noLinkError(__rectangle) || this.badFlagError(__eitherAxis)) {
                return false;
            }
            return this.__isFullyInRelativeBounds__(__rectangle, __eitherAxis);
        },

        /**
         * Optimized private method
         * @private
         * @param {Object} __rectangle - must be one of the parent to this object
         * @param {Boolean} __eitherAxis Flag to loosen the test to pass on either axis
         * @returns {Boolean} true if fully in bounds of parent
         */
        __isFullyInRelativeBounds__: function(__rectangle, __eitherAxis) {
            if (__eitherAxis) {
                //loose mode
                return this.__isFullyInRelativeHorBounds__(__rectangle) || this.__isFullyInRelativeVertBounds__(__rectangle);
            }
            //strict mode - defaults
            return this.__isFullyInRelativeHorBounds__(__rectangle) && this.__isFullyInRelativeVertBounds__(__rectangle);
        }
    };

    return rectangleRelativeIntersections;
});
