define([
    'p!-polytopes/rectExt/errorFunctions',
    'p!-polytopes/rectExt/rectangleRelativeGetters',
    'p!-polytopes/rectExt/rectangleRelativeEndGetters',
    'p!-polytopes/rectExt/rectangleGlobalGetters',
    //intersection
    'p!-polytopes/rectExt/rectangleParentIntersections',
    'p!-polytopes/rectExt/rectangleParentEndIntersections',
    'p!-polytopes/rectExt/rectangleRelativeIntersections',
    'p!-polytopes/rectExt/rectangleRelativeEndIntersections',
    //position tests
    'p!-polytopes/rectExt/rectanglePositionTests',
    'p!-polytopes/rectExt/rectangleGlobalPositionTests',
    'p!-polytopes/rectExt/rectangleEndPositionTests',
    'p!-polytopes/rectExt/rectangleGlobalEndPositionTests',
    //bonus
    'p!-polytopes/rectExt/rectangleCenterGetters',
    'p!-polytopes/rectExt/rectangleScaleFunctions'
], function(
    $errorFunctions,
    $rectangleRelativeGetters,
    $rectangleRelativeEndGetters,
    $rectangleGlobalGetters,
    //intersection
    $rectangleParentIntersections,
    $rectangleParentEndIntersections,
    $rectangleRelativeIntersections,
    $rectangleRelativeEndIntersections,
    //position tests
    $rectanglePositionTests,
    $rectangleGlobalPositionTests,
    $rectangleEndPositionTests,
    $rectangleGlobalEndPositionTests,
    //bonus
    $rectangleCenterGetters,
    $rectangleScaleFunctions) {
    'use strict';
    return {
        errorFunctions: $errorFunctions,
        rectangleParentIntersections: $rectangleParentIntersections,
        rectangleRelativeGetters: $rectangleRelativeGetters,
        rectangleRelativeEndGetters: $rectangleRelativeEndGetters,
        //indersections
        rectangleGlobalGetters: $rectangleGlobalGetters,
        rectangleParentEndIntersections: $rectangleParentEndIntersections,
        rectangleRelativeIntersections: $rectangleRelativeIntersections,
        rectangleRelativeEndIntersections: $rectangleRelativeEndIntersections,
        //positions
        rectanglePositionTests: $rectanglePositionTests,
        rectangleGlobalPositionTests: $rectangleGlobalPositionTests,
        rectangleEndPositionTests: $rectangleEndPositionTests,
        rectangleGlobalEndPositionTests: $rectangleGlobalEndPositionTests,
        //bonus
        rectangleCenterGetters: $rectangleCenterGetters,
        rectangleScaleFunctions: $rectangleScaleFunctions
    };
});
