/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define([
    'p!-polytopes/point2d',
    'p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $point2d,
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleCenterGetters = {

        //own center



        getCenterX: function() {
            return this.getLeft() + this.getWidth() / 2;
        },

        getCenterY: function() {
            return this.getTop() + this.getHeight() / 2;
        },

        getCenterPoint: function() {
            return $point2d.create(this.getCenterX(), this.getCenterY());
        },



        //global center



        getGlobalCenterX: function() {
            return this.getGlobalLeft() + this.getWidth() / 2;
        },

        getGlobalCenterY: function() {
            return this.getGlobalTop() + this.getHeight() / 2;
        },

        getGlobalCenterPoint: function() {
            return $point2d.create(this.getGlobalCenterX(), this.getGlobalCenterY());
        },



        //own end center



        getEndCenterX: function() {
            return this.getEndLeft() + this.getEndWidth() / 2;
        },

        getEndCenterY: function() {
            return this.getEndTop() + this.getEndHeight() / 2;
        },

        getEndCenterPoint: function() {
            return $point2d.create(this.getEndCenterX(), this.getEndCenterY());
        },



        //global end center



        getGlobalEndCenterX: function() {
            return this.getGlobalEndLeft() + this.getEndWidth() / 2;
        },

        getGlobalEndCenterY: function() {
            return this.getGlobalEndTop() + this.getEndHeight() / 2;
        },

        getGlobalEndCenterPoint: function() {
            return $point2d.create(this.getGlobalEndCenterX(), this.getGlobalEndCenterY());
        }
    };

    return rectangleCenterGetters;
});
