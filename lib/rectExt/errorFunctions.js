/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleErrorFunctions = {

        //--------- ERROR TESTS

        invalidRectError: function(__rect) {
            if (!this.isValidRectInstance(__rect)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Invalid parent object type specified!', arguments));
                return true;
            }
            return false;
        },

        noParentError: function() {
            if (!this.__parent__) {
                this.emit('error', $error($error.TYPE.PARENT_NOT_FOUND, 'no parent found'));
                return true;
            }
            return false;
        },

        //Always evoked by a child
        noLinkError: function(__rect) {
            if (!this.hasLinkTo(__rect)) {
                this.emit('error', $error($error.TYPE.INVALID_RELATIVE, 'Provided rect is not related to Instance!', arguments));
                return true;
            }

            return false;
        },

        badFlagError: function(__bool) {
            if (!$assertful.optBool(__bool)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Flag is not a Boolean', __bool));
                return true;
            }
            return false;
        },

        badValueError: function(__value) {
            if (!$assertful.number(__value)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Argument is non a number!'), arguments);
                return true;
            }
            return false;
        }
    };

    return rectangleErrorFunctions;
});
