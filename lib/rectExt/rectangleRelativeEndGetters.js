/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleRelativeEndGetters = {

        /**
         * Iterates through parent rectangles to retrieve End of X position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global X position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own X position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which X position is calculated
         * @returns {Number} relative | own | global X position
         */
        getRelativeEndX: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndX();
            }
            return this.__getRelativeEndX__(__rect);
        },

        /**
         * Optimized version of "getRelativeX" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which X position is calculated
         * @returns {Number} relative | own | global end of X position
         */
        __getRelativeEndX__: function(__rect) {
            if (__rect !== this) {
                return this.getEndX() + this.__parent__.__getRelativeEndLeft__(__rect);
            }
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve End of Y position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global End of Y position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own End of Y position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which End of Y position is calculated
         * @returns {Number} relative | own | global End of Y position
         */
        getRelativeEndY: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndY();
            }
            return this.__getRelativeEndY__(__rect);
        },

        /**
         * Optimized version of "getRelativeY" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Y position is calculated
         * @returns {Number} relative | own | global Y position
         */
        __getRelativeEndY__: function(__rect) {
            if (__rect !== this) {
                return this.getEndY() + this.__parent__.__getRelativeEndTop__(__rect);
            }
            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve End of Left position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global Left position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own Left position is returned
         *  @public
         * @param {Object} __rect one of parents of current instance relative to which Left position is calculated
         * @returns {Number} relative | own | global Left position
         */
        getRelativeEndLeft: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndLeft();
            }
            return this.__getRelativeEndLeft__(__rect);
        },

        /**
         * Optimized version of "getRelativeLeft" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which X position is calculated
         * @returns {Number} relative | own | global end of X position
         */
        __getRelativeEndLeft__: function(__rect) {
            if (__rect !== this) {
                return this.getEndLeft() + this.__parent__.__getRelativeEndLeft__(__rect);
            }

            return 0;
        },

        /**
         * Iterates through parent rectangles to retrieve End of Top position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global End of Top position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own End of Top position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which End of Top position is calculated
         * @returns {Number} relative | own | global End of Top position
         */
        getRelativeEndTop: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndTop();
            }
            return this.__getRelativeEndTop__(__rect);
        },

        /**
         * Optimized version of "getRelativeTop" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which Top position is calculated
         * @returns {Number} relative | own | global Top position
         */
        __getRelativeEndTop__: function(__rect) {
            if (__rect !== this) {
                return this.getEndTop() + this.__parent__.__getRelativeEndTop__(__rect);
            }
            return 0; //must return 0 as a relative point of origin
        },

        /**
         * Iterates through parent rectangles to retrieve End of Right position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global End of Right position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own End of Right position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which End of Right position is calculated
         * @returns {Number} relative | own | global End of Right position
         */
        getRelativeEndRight: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndRight();
            }
            return this.__getRelativeEndRight__(__rect);
        },

        /**
         * Optimized version of "getRelativeEndRight" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which EndRight position is calculated
         * @returns {Number} relative | own | global EndRight position
         */
        __getRelativeEndRight__: function(__rect) {
            if (__rect !== this) {
                return this.getEndRight() + this.__parent__.__getRelativeEndLeft__(__rect);
            }
            return 0; //must return 0 as a relative point of origin
        },

        /**
         * Iterates through parent rectangles to retrieve End of Bottom position,
         * - up-until supplied rectangle is encountered.
         *  - If supplied rectangle is no a parent of instance: global End of Bottom position is calculated
         *  - If supplied arguments is not an instance of "Rectangle" - own End of Bottom position is returned
         * @public
         * @param {Object} __rect one of parents of current instance relative to which End of Bottom position is calculated
         * @returns {Number} relative | own | global End of Bottom position
         */
        getRelativeEndBottom: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndBottom();
            }
            return this.__getRelativeEndBottom__(__rect);
        },

        /**
         * Optimized version of "getRelativeEndBottom" that omits argument checks.
         * @private
         * @param {Object} __rect one of parents of current instance relative to which EndBottom position is calculated
         * @returns {Number} relative | own | global EndBottom position
         */
        __getRelativeEndBottom__: function(__rect) {
            if (__rect !== this) {
                return this.getEndBottom() + this.__parent__.__getRelativeEndTop__(__rect);
            }
            return 0;
        },

        /**
         * Creates Bounds Object end values x,y,width,height attributes with values relative to supplied
         * parent rectangle
         * @public
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} end values of x,y,width,height relative to parent rectangle, if a rectangle not valid - own end bounds are returned
         */
        getRelativeEndBounds: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.getEndBounds();
            }
            return this.__getRelativeEndBounds__(__rect);
        },

        /**
         * Optimized version of "getRelativeEndBounds"
         * Creates Bounds Object end values x,y,width,height attributes with values relative to supplied
         * parent rectangle
         * @private
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} end values of x,y,width,height relative to parent rectangle, if a rectangle not valid - own end bounds are returned
         */
        __getRelativeEndBounds__: function(__rect) {
            return {
                left: this.__getRelativeEndLeft__(__rect),
                top: this.__getRelativeEndTop__(__rect),
                right: this.__getRelativeEndRight__(__rect),
                bottom: this.__getRelativeEndBottom__(__rect)
            };
        },

        /**
         * Creates full version of Bounds Object adding right, bottom attributes with values relative to supplied
         * parent rectangle
         * @event {TYPE_ERROR} error event if a rectangle not an offspring of "Rectangle" class
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} x,y,width,height,right,bottom relative to parent rectangle, if a rectangle not valid - own bounds are returned
         */
        getRelativeEndData: function(__rect) {
            if (this.invalidRectError(__rect) || this.noLinkError(__rect)) {
                return this.geEndtData();
            }
            return this.__getRelativeEndData__(__rect);
        },

        /**
         * Optimized version of "getRelativeData"
         * Creates a full Bounds Object with end values adding right, bottom attributes with values relative to supplied
         * parent rectangle
         * @private
         * @param   {Object} __rect one of parent rectangles,
         * @returns {Object} end values of x,y,width,height,right,bottom  relative to parent rectangle
         */
        __getRelativeEndData__: function(__rect) {
            return {
                left: this.__getRelativeEndLeft__(__rect),
                top: this.__getRelativeEndTop__(__rect),
                right: this.__getRelativeEndRight__(__rect),
                bottom: this.__getRelativeEndBottom__(__rect),
                x: this.__getRelativeEndX__(__rect),
                y: this.__getRelativeEndY__(__rect),
                width: this.getEndWidth(),
                height: this.getEndHeight(),
                originX: this.getOriginEndX(),
                originY: this.getOriginEndY()
            };
        }
    };

    return rectangleRelativeEndGetters;
});
