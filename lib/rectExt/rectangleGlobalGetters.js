/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleGlobalGetters = {
        /**
         * Iterates through all parent rectangles to retrieve X position
         * @returns {Number} Global X position
         */
        getGlobalX: function() {
            return this.getX() + (this.__parent__ ? this.__parent__.getGlobalLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve Y position
         * @returns {Number} Global Y position
         */
        getGlobalY: function() {
            return this.getY() + (this.__parent__ ? this.__parent__.getGlobalTop() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve X position
         * @returns {Number} Global X position
         */
        getGlobalLeft: function() {
            return this.getLeft() + (this.__parent__ ? this.__parent__.getGlobalLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve Y position
         * @returns {Number} Global Y position
         */
        getGlobalTop: function() {
            return this.getTop() + (this.__parent__ ? this.__parent__.getGlobalTop() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve Right position
         * @returns {Number} Global Right position
         */
        getGlobalRight: function() {
            return this.getRight() + (this.__parent__ ? this.__parent__.getGlobalLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve Bottom position
         * @returns {Number} Global Bottom position
         */
        getGlobalBottom: function() {
            return this.getBottom() + (this.__parent__ ? this.__parent__.getGlobalTop() : 0);
        },

        /**
         * Constructs a global positions Bounds object
         * @returns {Object} Bounds object with {left,top, right,bottom}
         */
        getGlobalBounds: function() {
            return {
                left: this.getGlobalLeft(),
                top: this.getGlobalTop(),
                right: this.getGlobalRight(),
                bottom: this.getGlobalBottom()
            };
        },

        /**
         * Creates global position object including all known data sets of this rect
         * @returns {Object} Bounds object with {left,top, right, bottom, x, y, width, height, originX, originY}
         */
        getGlobalData: function() {
            return {
                left: this.getGlobalLeft(),
                top: this.getGlobalTop(),
                right: this.getGlobalRight(),
                bottom: this.getGlobalBottom(),
                x: this.getGlobalX(),
                y: this.getGlobalY(),
                width: this.getWidth(),
                height: this.getHeight(),
                originX: this.getOriginX(),
                originY: this.getOriginY()
            };
        },

        /**
         * Iterates through all parent rectangles to retrieve end X position
         * @returns {Number} Global End X position
         */
        getGlobalEndX: function() {
            return this.getEndX() + (this.__parent__ ? this.__parent__.getGlobalEndLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve end Y position
         * @returns {Number} Global End Y position
         */
        getGlobalEndY: function() {
            return this.getEndY() + (this.__parent__ ? this.__parent__.getGlobalEndTop() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve end Left position
         * @returns {Number} Global End Left position
         */
        getGlobalEndLeft: function() {
            return this.getEndLeft() + (this.__parent__ ? this.__parent__.getGlobalEndLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve end Top position
         * @returns {Number} Global End Top position
         */
        getGlobalEndTop: function() {
            return this.getEndTop() + (this.__parent__ ? this.__parent__.getGlobalEndTop() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve end Right position
         * @returns {Number} Global End Right position
         */
        getGlobalEndRight: function() {
            return this.getEndRight() + (this.__parent__ ? this.__parent__.getGlobalEndLeft() : 0);
        },

        /**
         * Iterates through all parent rectangles to retrieve end Bottom position
         * @returns {Number} Global End Bottom position
         */
        getGlobalEndBottom: function() {
            return this.getEndBottom() + (this.__parent__ ? this.__parent__.getGlobalEndTop() : 0);
        },


        /**
         * Constructs a global positions Bounds object
         * @returns {Object} Bounds object with {left,top, right,bottom}
         */
        getGlobalEndBounds: function() {
            return {
                left: this.getGlobalEndLeft(),
                top: this.getGlobalEndTop(),
                right: this.getGlobalEndRight(),
                bottom: this.getGlobalEndBottom()
            };
        },

        /**
         * Creates global position object including all known data sets of this rectangle
         * @returns {Object} all position data associated with rectangle
         */
        getGlobalEndData: function() {
            return {
                left: this.getGlobalEndLeft(),
                top: this.getGlobalEndTop(),
                right: this.getGlobalEndRight(),
                bottom: this.getGlobalEndBottom(),
                x: this.getGlobalEndX(),
                y: this.getGlobalEndY(),
                width: this.getEndWidth(),
                height: this.getEndHeight(),
                originX: this.getOriginEndX(),
                originY: this.getOriginEndY()
            };
        }
    };

    return rectangleGlobalGetters;
});
