/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define(['p!-polytopes/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var rectangleScaleFunctions = {

        scalePx: function(__pxValue, __bypassCurves) {
            if (this.badValueError(__pxValue)) {
                return this;
            }
            return this.__scalePx__(__pxValue, __bypassCurves);
        },

        __scalePx__: function(__pxValue, __bypassCurves) {
            this.translateCoord(-__pxValue, -__pxValue, __bypassCurves);
            this.addSize(__pxValue * 2, __pxValue * 2, __bypassCurves);
            return this;
        },

        scale: function(__prsValue, __bypassCurves) {
            if (this.badValueError(__prsValue)) {
                return this;
            }
            return this.__scale__(__prsValue, __bypassCurves);
        },

        __scale__: function(__prsValue, __bypassCurves) {
            this.translateX((this.width - this.width * __prsValue) / 2, __bypassCurves);
            this.translateY((this.height - this.height * __prsValue) / 2, __bypassCurves);
            this.addSize(this.width * __prsValue, this.height * __prsValue, __bypassCurves);
        }
    };

    return rectangleScaleFunctions;
});
