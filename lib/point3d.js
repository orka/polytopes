/**
 * Describes a three (2D) dimensional point with x, y and z attributes
 * Includes variety of methods to aid in manipulating positioning and in time curve mutation of z,y and z properties
 */
define(['p!-numerus',
    'p!-assertful',
    'p!-polytopes/error',
    'p!-logger',
    'p!-polytopes/point2d'
], function($numerus,
    $assertful,
    $error,
    $logger,
    $point2d) {
    'use strict';

    var Point3d = $point2d.subclass('Point3d', {

        constructor: function(__xNumerus, __yNumerus, __zNumerus) {
            //init super constructor
            Point3d.super.constructor.call(this, __xNumerus, __yNumerus);

            if (!$numerus.number.isOffspring(__zNumerus)) {
                $error($error.TYPE.TYPE_ERROR, 'Arguments must be instances of Numerus package').throw();
            }
            this.__zNumerus = __zNumerus;
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Point3d.super.deconstructor.call(this);
        },

        /**
         * a dynamic set of common listeners
         * install listeners the first time listeners affition is evoked
         */
        addListener: function (__event, __handler) {
            switch (__event) {
                case 'floorZ':
                    this.__zNumerus.addListener('floor', __handler);
                    break;
                case 'ceilZ':
                    this.__zNumerus.addListener('ceil', __handler);
                    break;
                case 'startZ':
                    this.__zNumerus.addListener('start', __handler);
                    break;
                case 'endZ':
                    this.__zNumerus.addListener('end', __handler);
                    break;
                case 'completeZ':
                    this.__zNumerus.addListener('complete', __handler);
                    break;
                default:
                    Point3d.super.addListener.apply(this, arguments);
            }
        },

        setZ: function(__value, __bypassCurves) {
            this.__zNumerus.set(__value, __bypassCurves);
        },

        setCoord: function(__x, __y, __z, __bypassCurves) {
            Point3d.super.setCoord.call(this, __x, __y, __bypassCurves);
            this.setZ(__z, __bypassCurves);
        },

        endZ: function() {
            this.__zNumerus.end();
        },

        endAll: function() {
            Point3d.super.endAll.call(this);
            this.endZ();
        },

        getZ: function() {
            return this.__zNumerus.get();
        },

        getCoord: function() {
            var __coord = Point3d.super.getCoord();
            __coord.z = this.getZ();
            return __coord;
        },

        maxZ: function(__bypassCurves) {
            this.__zNumerus.toMax(__bypassCurves);
        },

        maxAll: function(__bypassCurves) {
            Point3d.super.maxAll.call(this, __bypassCurves);
            this.maxZ(__bypassCurves);
        },

        minZ: function(__bypassCurves) {
            this.__zNumerus.toMin(__bypassCurves);
        },

        minAll: function(__bypassCurves) {
            Point3d.super.minAll.call(this, __bypassCurves);
            this.minZ(__bypassCurves);
        },

        resetZ: function(__bypassCurves) {
            this.__zNumerus.reset(__bypassCurves);
        },

        resetAll: function(__bypassCurves) {
            Point3d.super.resetAll.call(this, __bypassCurves);
            this.resetZ(__bypassCurves);
        },

        translateZ: function(__value, __bypassCurves) {
            this.setZ(this.getZ() + __value, __bypassCurves);
        },

        translateCoord: function(__x, __y, __z, __bypassCurves) {
            Point3d.super.translateCoord.call(this, __x, __y, __bypassCurves);
            this.translateZ(__z, __bypassCurves);
        },

        getEndZ: function() {
            return this.__zNumerus.getEnd();
        },

        getEndCoord: function() {
            var __coord = Point3d.super.getEndCoord();
            __coord.z = this.getEndZ();
            return __coord;
        },

        getStartZ: function() {
            return this.__zNumerus.getStart();
        },

        getStartCoord: function() {
            var __coord = Point3d.super.getStartCoord();
            __coord.z = this.getStartZ();
            return __coord;
        }
    });

    Point3d.create = function (__x, __y, __z) {
        return Point3d($numerus.number(__x), $numerus.number(__y), $numerus.number(__z));
    };

    Point3d.createFromBounds = function (__bounds) {
        return Point3d.create(__bounds.x, __bounds.y, __bounds.z);
    };

    Point3d.createFromArray = function (__array) {
        return Point3d.create(__array[0], __array[1], __array[2]);
    };

    return Point3d;
});
