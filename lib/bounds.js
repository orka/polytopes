/**
 * Describes a two (2D) dimensional rect with x and y, width and height attributes
 */
define([
    'p!-defineclass',
], function(
    $defineclass) {
    'use strict';

    var Bounds = $defineclass('Rectangle', {

        /**
         * Public API
         * @param   {Number} __x The x position of the bounds
         * @param   {Number} __y The y position of the bounds
         * @param   {Number} __width The width of the bounds
         * @param   {Number} __height The height of the bounds
         * @returns {Object} Bounds instance
         */
        constructor: function(__x, __y, __width, __height) {
            this.x = __x;
            this.y = __y;
            this.width = __width;
            this.height = __height;
        },

        deconstructor: function() {
            this.x = null;
            this.y = null;
            this.width = null;
            this.height = null;
        },

        getY: function() {
            return this.y;
        },

        getX: function() {
            return this.x;
        },

        getWidth: function() {
            return this.width;
        },

        getHeight: function() {
            return this.height;
        },

        getRight: function() {
            return this.x + this.width;
        },

        getBottom: function() {
            return this.y + this.height;
        },

        getMiddleY: function() {
            return this.y + this.height / 2;
        },

        getCenterX: function() {
            return this.x + this.width / 2;
        },

        clone: function() {
            return Bounds(this.x, this.y, this.width, this.height);
        },



        //-----------------------------------------------------     MODYFYIERS




        setX: function(__x) {
            this.x = __x;
            return this;
        },

        setY: function(__y) {
            this.y = __y;
            return this;
        },

        setHeight: function(__height) {
            this.height = __height;
            return this;
        },

        setWidth: function(__width) {
            this.width = __width;
            return this;
        },

        translateX: function(__x) {
            this.x += __x;
            return this;
        },


        translateY: function(__y) {
            this.y += __y;
            return this;
        },

        addWidth: function(__width) {
            this.width += __width;
            return this;
        },

        addHeight: function(__height) {
            this.height += __height;
            return this;
        },

        translate: function(__x, __y) {
            this.x += __x;
            this.y += __y;
            return this;
        },

        translateBounds: function(__bounds) {
            this.x += __bounds.x;
            this.y += __bounds.y;
            this.width += __bounds.width;
            this.height += __bounds.height;
            return this;
        },

        scalePx: function(__px) {
            this.x -= __px;
            this.y -= __px;
            this.width += __px * 2;
            this.height += __px * 2;
            return this;
        },

        scale: function(__prs) {
            this.scaleX(__prs);
            this.scaleY(__prs);
            this.scaleWidth(__prs);
            this.scaleHeight(__prs);
            return this;
        },

        scaleX: function(__prs) {
            this.x += (this.width - (this.width * __prs)) / 2;
            return this;
        },

        scaleY: function(__prs) {
            this.y += (this.height - (this.height * __prs)) / 2;
            return this;
        },

        scaleWidth: function(__prs) {
            this.width *= __prs;
            return this;
        },

        scaleHeight: function(__prs) {
            this.height *= __prs;
            return this;
        }
    });


    return Bounds;
});
