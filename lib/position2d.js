/**
 * Describes a two (2D) dimensional shape with posiion and origine point2ds only
 * All 2 dymentional object will extend from here
 * Includes variety of methods to aid in manipulating positioning and in time curve mutation of both x and y property
 */
define(['p!-eventful',
    'p!-numerus',
    'p!-polytopes/point2d',
    'p!-assertful',
    'p!-polytopes/error',
    'p!-logger'
], function($eventful,
    $numerus,
    $point2d,
    $assertful,
    $error,
    $logger) {
    'use strict';

    var Position2d = $point2d.subclass('Position2d', {

        /**
         * Shape object constructor
         * defines 2 dimentional shape base class with psition and origine point2d. Note: shape object is an abstract and contains not size or shape definition!
         * @param {Object} __positionPoint2d a position point2d - optional - default 0/0 (Point2d2d:lib/point2d2d.js)
         * @param {Object} __originPoint2d shape origin point2d - optional - default 0/0 {Point2d2d:lib/point2d2d.js}
         */
        constructor: function(__xNumerus, __yNumerus, __xOriginNumerus, __yOriginNumerus) {

            Position2d.super.constructor.call(this, __xNumerus, __yNumerus);

            if ((__xOriginNumerus && !$numerus.number.isOffspring(__xOriginNumerus)) ||
                (__yOriginNumerus && !$numerus.number.isOffspring(__yOriginNumerus))) {
                $error($error.TYPE.TYPE_ERROR, 'Arguments invalid - use instances of numerus only!').throw();
            } else if (__xOriginNumerus && __yOriginNumerus) {
                this.__originXNumerus = __xOriginNumerus;
                this.__originYNumerus = __yOriginNumerus;
            } else {
                this.__originXNumerus = $numerus.number(0);
                this.__originYNumerus = $numerus.number(0);
            }


            this.originX = this.__originXNumerus.__value__;
            this.originY = this.__originYNumerus.__value__;

            this.__originXNumerus.addListener('change', this.__setCacheOriginX__.bind(this));
            this.__originYNumerus.addListener('change', this.__setCacheOriginY__.bind(this));
        },

        /**
         * Dereferences all objects associated with this class
         */
        deconstructor: function() {
            Position2d.super.deconstructor.call(this);
            //deconstruct point2ds
            this.__originXNumerus.deconstructor();
            this.__originYNumerus.deconstructor();
            this.__originXNumerus = null;
            this.__originYNumerus = null;
            this.originX = null;
            this.originY = null;
        },

        /**
         * Clone current point with current or original positions
         * @param {Boolean} __currentPositions if true current positions are cloned
         * @returns {Object} New Instance of Point2D
         */
        clone: function(__currentPositions) {
            return Position2d(this.__xNumerus.clone(__currentPositions), this.__yNumerus.clone(__currentPositions), this.__originXNumerus.clone(__currentPositions), this.__originYNumerus.clone(__currentPositions));
        },

        updatePosition: function () {
            this.updateCoord();
            this.__originXNumerus.update();
            this.__originYNumerus.update();
        },

        __setCacheOriginX__: function () {
            this.originX = this.__originXNumerus.__value__;
        },

        __setCacheOriginY__: function () {
            this.originY = this.__originYNumerus.__value__;
        },

        //---------------------------------------   SETTERS   ----------------------------------



        setOriginX: function(__value, __bypassCurves) {
            this.__originXNumerus.set(__value, __bypassCurves);
            return this;
        },

        setOriginY: function(__value, __bypassCurves) {
            this.__originYNumerus.set(__value, __bypassCurves);
            return this;
        },

        setOrigin: function(__x, __y, __bypassCurves) {
            this.setOriginX(__x, __bypassCurves);
            this.setOriginY(__y, __bypassCurves);
            return this;
        },


        maxOriginX: function(__bypassCurves) {
            this.__originXNumerus.toMax(__bypassCurves);
            return this;
        },

        maxOriginY: function(__bypassCurves) {
            this.__originYNumerus.toMax(__bypassCurves);
            return this;
        },

        maxOrigin: function(__bypassCurves) {
            this.maxOriginX(__bypassCurves);
            this.maxOriginY(__bypassCurves);
            return this;
        },

        minOriginX: function(__bypassCurves) {
            this.__originXNumerus.toMin(__bypassCurves);
            return this;
        },

        minOriginY: function(__bypassCurves) {
            this.__originYNumerus.toMin(__bypassCurves);
            return this;
        },

        minOrigin: function(__bypassCurves) {
            this.minOriginX(__bypassCurves);
            this.minOriginY(__bypassCurves);
            return this;
        },

        resetOriginX: function(__bypassCurves) {
            this.__originXNumerus.reset(__bypassCurves);
            return this;
        },

        resetOriginY: function(__bypassCurves) {
            this.__originYNumerus.reset(__bypassCurves);
            return this;
        },

        resetOrigin: function(__bypassCurves) {
            this.resetOriginX(__bypassCurves);
            this.resetOriginY(__bypassCurves);
            return this;
        },

        resetPosition: function(__bypassCurves) {
            this.resetOrigin(__bypassCurves);
            this.resetCoord(__bypassCurves);
            return this;
        },

        translateOriginX: function(__value, __bypassCurves) {
            this.setOriginX(this.getOriginX() + __value, __bypassCurves);
            return this;
        },

        translateOriginY: function(__value, __bypassCurves) {
            this.setOriginY(this.getOriginY() + __value, __bypassCurves);
            return this;
        },

        translateOrigin: function(__x, __y, __bypassCurves) {
            this.translateOriginX(__x, __bypassCurves);
            this.translateOriginY(__y, __bypassCurves);
            return this;
        },

        /**
         * Sets Left position by setting left edge to the specified position
         * @param {Number} __value        Left position
         * @param {Boolean} __bypassCurves a bypass flag to skip/curves
         * @returns {Object} self
         */
        setLeft: function(__value, __bypassCurves) {
            this.setX(__value + this.getOriginX(), __bypassCurves);
            return this;
        },

        /**
         * Sets Top position by setting left edge to the specified position
         * @param {Number} __value        Top position
         * @param {Boolean} __bypassCurves a bypass flag to skip/curves
         * @returns {Object} self
         */
        setTop: function(__value, __bypassCurves) {
            this.setY(__value + this.getOriginY(), __bypassCurves);
            return this;
        },

        /**
         * Sets Top amd Left position by setting left and top edge to the specified position
         * @param {Number} __x        Left position
         * @param {Number} __y        Top position
         * @param {Boolean} __bypassCurves a bypass flag to skip/curves
         * @returns {Object} self
         */
        setPosition: function(__x, __y, __bypassCurves) {
            this.setLeft(__x, __bypassCurves);
            this.setTop(__y, __bypassCurves);
            return this;
        },

        translateLeft: function(__value, __bypassCurves) {
            this.setLeft(this.getLeft() + __value, __bypassCurves);
            return this;
        },

        translateTop: function(__value, __bypassCurves) {
            this.setTop(this.getTop() + __value, __bypassCurves);
            return this;
        },

        translatePosition: function(__x, __y, __bypassCurves) {
            this.translateLeft(__x, __bypassCurves);
            this.translateTop(__y, __bypassCurves);
            return this;
        },



        //---------------------------------------   END Curve   ----------------------------------



        endOriginX: function() {
            this.__originXNumerus.end();
            return this;
        },

        endOriginY: function() {
            this.__originYNumerus.end();
            return this;
        },

        endOrigin: function() {
            this.endOriginY();
            this.endOriginX();
            return this;
        },



        //---------------------------------------   GETTERS   ----------------------------------



        getOriginX: function() {
            return this.originX;
        },

        getOriginY: function() {
            return this.originY;
        },

        getOrigin: function() {
            return {
                x: this.getOriginX(),
                y: this.getOriginY()
            };
        },

        getOriginEndX: function() {
            return this.__originXNumerus.getEnd();
        },

        getOriginEndY: function() {
            return this.__originYNumerus.getEnd();
        },

        getOriginEnd: function() {
            return {
                x: this.getOriginEndX(),
                y: this.getOriginEndY()
            };
        },

        getOriginStartX: function() {
            return this.__originXNumerus.getStart();
        },

        getOriginStartY: function() {
            return this.__originYNumerus.getStart();
        },

        getOriginStart: function() {
            return {
                x: this.getOriginStartX(),
                y: this.getOriginStartY()
            };
        },

        getLeft: function() {
            return this.getX() - this.getOriginX();
        },

        getTop: function() {
            return this.getY() - this.getOriginY();
        },

        getPosition: function() {
            return {
                left: this.getLeft(),
                top: this.getTop()
            };
        },

        getEndLeft: function() {
            return this.getEndX() - this.getOriginEndX();
        },

        getEndTop: function() {
            return this.getEndY() - this.getOriginEndY();
        },

        getEndPosition: function() {
            return {
                left: this.getEndLeft(),
                top: this.getEndTop()
            };
        },

        getStartLeft: function() {
            return this.getStartX() - this.getOriginStartX();
        },

        getStartTop: function() {
            return this.getStartY() - this.getOriginStartY();
        },

        getStartPosition: function() {
            return {
                left: this.getStartLeft(),
                top: this.getStartTop()
            };
        }
    });


    Position2d.fromCoord = function(__x, __y, __oX, __oY) {
        return Position2d($numerus.number(__x), $numerus.number(__y), $numerus.number(__oX), $numerus.number(__oY));
    };

    Position2d.fromNumerus = function(__x, __y, __oX, __oY) {
        if (__oX && __oY) {
            return Position2d(__x.clone(), __y.clone(), __oX.clone(), __oY.clone());
        }
        return Position2d(__x.clone(), __y.clone());
    };

    Position2d.fromPoints = function(__coordPoint, __originPoint) {
        if (__originPoint) {
            return Position2d.fromNumerus(__coordPoint.x, __coordPoint.y, __originPoint.x, __originPoint.y);
        }
        return Position2d.fromCoord(__coordPoint.x, __coordPoint.y);
    };

    Position2d.fromBounds = function(__bounds) {
        return Position2d.fromCoord(__bounds.x, __bounds.y, __bounds.originX, __bounds.originY);
    };

    Position2d.fromArray = function(__array) {
        return Position2d.fromCoord(__array[0], __array[1], __array[2], __array[3]);
    };

    return Position2d;
});
