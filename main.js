define([
    'p!-polytopes/point2d',
    'p!-polytopes/size2d',
    'p!-polytopes/positions2d',
    'p!-polytopes/rectangle',
    'p!-polytopes/bounds',
    'p!-polytopes/error'
], function(
    $point2d,
    $size2d,
    $position2d,
    $rectangle,
    $bounds,
    $error) {
    'use strict';
    return {
        point2d: $point2d,
        size2d: $size2d,
        position2d: $position2d,
        rectangle: $rectangle,
        rect: $rectangle,
        bounds: $bounds,
        error: $error
    };
});
