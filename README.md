## POLYTOPES
Polytopes Library (a geometrical mathematical construct)

<<The term polytope is nowadays a broad term that covers a wide class of objects, and different definitions are attested in mathematical literature. Many of these definitions are not equivalent, resulting in different sets of objects being called polytopes. They represent different approaches to generalizing the convex polytopes to include other objects with similar properties.>>

* Vertex: 0-Polytope - aka Point
* Edge: 1-Polytope - aka Line
* Face: 2-Polytope - aka polygon
* Cell: 3-Polytope - aka a polyhedral element