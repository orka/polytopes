/*
global describe,
global it,
global expect,
 */
define(['p!-numerus', 'lib/rectangle'], function($numerus, $rectangle) {
    'use strict';

    var num = $numerus.number(0);

    describe('When instantiating 2D Point object', function() {

        it('$rectangle Class should exist', function() {
            expect($rectangle).toBeDefined();
        });

        describe('When using constructor method $rectangle(Numerus, Numerus)', function() {
            function create() {
                return $rectangle.bind(null, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if numbers supplied', function() {
                expect(create(1, 2, 3, 4)).toThrow();
                expect(create(1.1, 2.2, 1.2, 0.1)).toThrow();
                expect(create(Infinity, Infinity, Infinity, Infinity)).toThrow();
            });

            it('Should throw if numbers supplied for optional values', function() {
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), 1, 2)).toThrow();
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), 1.2, 0.1)).toThrow();
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), Infinity, Infinity)).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw if Object|Array|String supplied for optional values', function() {
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), {}, {})).toThrow();
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), [], [])).toThrow();
                expect(create(num.clone(), num.clone(), num.clone(), num.clone(), 'qw', 'er')).toThrow();
            });

            it('Should throw when only one Numerus objects supplied', function() {
                expect(create(num.clone(), 2, 3, 4)).toThrow();
                expect(create(1, 2, 3, 4, num.clone())).toThrow();
            });

            it('Should not throw when all Numerus objects supplied', function() {
                expect(create(num.clone(), num.clone(), num.clone(), num.clone())).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(num.clone(), num.clone(), num.clone(), num.clone())();
                expect(_instance.getX()).toEqual(num.get());
                expect(_instance.getY()).toEqual(num.get());
            });
        });


        describe('When using constructor method $rectangle.fromCoord(#,#)', function() {

            function create() {
                return $rectangle.fromCoord.bind(null, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {}, {}, {})).toThrow();
                expect(create([], [], [], [])).toThrow();
                expect(create('qw', 'er', 'qw', 'er')).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create(num.clone(), 1, 1, 2)).toThrow();
                expect(create(1, 1, 2, num.clone())).toThrow();
            });

            it('Should throw when both Numerus objects supplied', function() {
                expect(create(num.clone(), num.clone())).toThrow();
            });

            it('Should not throw if numbers supplied', function() {
                expect(create(1, 2, 1, 2)).not.toThrow();
                expect(create(1.1, 2.2, 1.1, 2.2)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(1, 2, 1, 2)();
                expect(_instance.getX()).toEqual(1);
                expect(_instance.getY()).toEqual(2);
            });
        });

        describe('When using constructor method $rectangle.fromBounds({})', function() {
            var _bounds = {
                x: 0,
                y: 0,
                width: 0,
                height: 0
            };

            function create(__bounds) {
                return $rectangle.fromBounds.bind(null, __bounds);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create([])).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create(num.clone())).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_bounds)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_bounds)();
                expect(_instance.getX()).toEqual(_bounds.x);
                expect(_instance.getY()).toEqual(_bounds.y);
            });

            it('Should have Width, Height values equal to arguments supplied', function() {
                var _instance = create(_bounds)();
                expect(_instance.getWidth()).toEqual(_bounds.width);
                expect(_instance.getHeight()).toEqual(_bounds.height);
            });
        });



        describe('When using constructor method $rectangle.fromArray([])', function() {
            var _array = [1, 2, 3, 4];

            function create(__array) {
                return $rectangle.fromArray.bind(null, __array);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create({})).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create(num.clone())).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should throw array length is less than 3', function() {
                expect(create([0, 1, 2])).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_array)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_array)();
                expect(_instance.getX()).toEqual(_array[0]);
                expect(_instance.getY()).toEqual(_array[1]);
            });

            it('Should have Width, Height values equal to arguments supplied', function() {
                var _instance = create(_array)();
                expect(_instance.getWidth()).toEqual(_array[2]);
                expect(_instance.getHeight()).toEqual(_array[3]);
            });
        });



        describe('When using deconstructor() method', function() {
            var instance;

            beforeEach(function() {
                instance = $rectangle(num.clone(), num.clone(), num.clone(), num.clone());
            });

            it('Should be defined', function() {
                expect(instance.deconstructor).toBeDefined();
            });

            it('Should derefference .x, .y', function() {
                instance.deconstructor();
                expect(instance.x).toEqual(null);
                expect(instance.y).toEqual(null);
            });

            it('Should derefference .width, .height', function() {
                instance.deconstructor();
                expect(instance.width).toEqual(null);
                expect(instance.height).toEqual(null);
            });
        });


        describe('When using clone() method', function() {
            var instance;

            beforeEach(function() {
                instance = $rectangle(num.clone(), num.clone(), num.clone(), num.clone());
            });

            it('Should be defined', function() {
                expect(instance.clone).toBeDefined();
            });

            it('Should create new instance with original properies of Point', function() {
                var _new = instance.clone();
                expect($rectangle.isOffspring(_new)).toEqual(true);
            });
        });
    });
});
