/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/rectangle'], function($numerus, $curves, $rectangle) {
    'use strict';

    var measurePerf = false;

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    //
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 222;
    var OriginY = 444;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);

    //
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var Width = 222;
    var Height = 444;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);

    var nestedDepth = 3;
    //
    var child;
    var genesis;

    //precompute results
    var data = {
        left: X - OriginX,
        top: Y - OriginY,
        right: X - OriginX + Width,
        bottom: Y - OriginY + Height,
        x: X,
        y: Y,
        width: Width,
        height: Height,
        originX: OriginX,
        originY: OriginY
    };
    var dataStr = JSON.stringify(data);

    var newData = {
        left: newX - newOriginX,
        top: newY - newOriginY,
        right: newX - newOriginX + newWidth,
        bottom: newY - newOriginY + newHeight,
        x: newX,
        y: newY,
        width: newWidth,
        height: newHeight,
        originX: newOriginX,
        originY: newOriginY
    };
    var newDataStr = JSON.stringify(newData);

    var nestedData = {
        left: data.left * nestedDepth,
        top: data.top * nestedDepth,
        right: data.left * nestedDepth + data.width,
        bottom: data.top * nestedDepth + data.height,
        x: data.left * nestedDepth + data.originX,
        y: data.top * nestedDepth + data.originY,
        width: data.width,
        height: data.height,
        originX: data.originX,
        originY: data.originY
    };
    var nestedDataStr = JSON.stringify(nestedData);

    var nestedNewData = {
        left: newData.left * nestedDepth,
        top: newData.top * nestedDepth,
        right: newData.left * nestedDepth + newData.width,
        bottom: newData.top * nestedDepth + newData.height,
        x: newData.left * nestedDepth + newData.originX,
        y: newData.top * nestedDepth + newData.originY,
        width: newData.width,
        height: newData.height,
        originX: newData.originX,
        originY: newData.originY
    };
    var nestedNewDataStr = JSON.stringify(nestedNewData);

    var bounds = {
        left: data.left,
        top: data.top,
        right: data.right,
        bottom: data.bottom
    };
    var boundsStr = JSON.stringify(bounds);

    var nestedBounds = {
        left: nestedData.left,
        top: nestedData.top,
        right: nestedData.right,
        bottom: nestedData.bottom
    };
    var nestedBoundsStr = JSON.stringify(nestedBounds);

    var newBounds = {
        left: newData.left,
        top: newData.top,
        right: newData.right,
        bottom: newData.bottom
    };
    var newBoundsStr = JSON.stringify(newBounds);

    var nestedEndBounds = {
        left: nestedNewData.left,
        top: nestedNewData.top,
        right: nestedNewData.right,
        bottom: nestedNewData.bottom
    };
    var nestedEndBoundsStr = JSON.stringify(nestedEndBounds);


    function setNewValues(__child, __bypass) {
        return __child.setSize(newWidth, newHeight, __bypass).setCoord(newX, newY, __bypass).setOrigin(newOriginX, newOriginY, __bypass);
    }

    function make() {
        return $rectangle(numX.clone(), numY.clone(), numWidth.clone(), numHeight.clone(), numOriginX.clone(), numOriginY.clone());
    }

    function runOptPerf(__method) {
        if (measurePerf) {
            it('Optional --- Should have optimized version of method that is actually faster', function() {
                var _mark1 = performance.now();
                child[__method](genesis);
                var _mark2 = performance.now();
                child['__' + __method + '__'](genesis);
                var _mark3 = performance.now();
                expect((_mark2 - _mark1) - (_mark3 - _mark2)).toBeGreaterThan(0);
            });
        }
    }

    //------------------------------ ORPHANT


    var orphantSetup = {
        make: function() {
            child = make();
            genesis = null;
        },
        new: function() {
            orphantSetup.make();
            setNewValues(child);
        },
        newBypass: function() {
            orphantSetup.make();
            setNewValues(child, true);
        },
        newEnded: function(__done) {
            orphantSetup.make();
            setNewValues(child);
            setTimeout(__done, curveDuration);
        }
    };


    //------------------------------ NESTED


    var nestedSetup = {
        make: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
            }

            genesis = _child;
        },
        new: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newBypass: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child, true);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newEnded: function(__done) {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
            setTimeout(__done, curveDuration);
        }
    };




    describe('When using $rectangle end getters APIs', function() {
        //getEndX
        describe('When using .getEndX()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndX).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndX.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndX.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getEndX()).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getEndX()).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getEndX()).toEqual(newData.x);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getEndX()).toEqual(newData.x);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getEndX()).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getEndX()).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getEndX()).toEqual(newData.x);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getEndX()).toEqual(newData.x);
                    });
                });
            });
        });
        //getEndY
        describe('When using .getEndY()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndY).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndY.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndY.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getEndY()).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getEndY()).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getEndY()).toEqual(newData.y);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getEndY()).toEqual(newData.y);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getEndY()).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getEndY()).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getEndY()).toEqual(newData.y);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getEndY()).toEqual(newData.y);
                    });
                });
            });
        });
        //getEndLeft
        describe('When using .getEndLeft()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndLeft).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndLeft.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndLeft.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getEndLeft()).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getEndLeft()).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getEndLeft()).toEqual(newData.left);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getEndLeft()).toEqual(newData.left);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getEndLeft()).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getEndLeft()).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getEndLeft()).toEqual(newData.left);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getEndLeft()).toEqual(newData.left);
                    });
                });
            });
        });
        //getEndTop
        describe('When using .getEndTop()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndTop).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndTop.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndTop.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getEndTop()).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getEndTop()).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getEndTop()).toEqual(newData.top);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getEndTop()).toEqual(newData.top);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getEndTop()).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getEndTop()).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getEndTop()).toEqual(newData.top);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getEndTop()).toEqual(newData.top);
                    });
                });
            });
        });
        //getEndRight
        describe('When using .getEndRight()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndRight).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndRight.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndRight.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getEndRight()).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getEndRight()).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getEndRight()).toEqual(newData.right);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getEndRight()).toEqual(newData.right);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getEndRight()).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getEndRight()).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getEndRight()).toEqual(newData.right);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getEndRight()).toEqual(newData.right);
                    });
                });
            });
        });
        //getEndBottom
        describe('When using .getEndBottom()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndBottom).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndBottom.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndBottom.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getEndBottom()).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getEndBottom()).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getEndBottom()).toEqual(newData.bottom);
                    });
                });
            });
        });
        //getEndBounds
        describe('When using .getEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndBounds).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndBounds.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndBounds.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getEndBounds()).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getEndBounds()).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getEndBounds()).toEqual(newBounds);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getEndBounds()).toEqual(newBounds);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getEndBounds()).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getEndBounds()).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getEndBounds()).toEqual(newBounds);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getEndBounds()).toEqual(newBounds);
                    });
                });
            });
        });
        //getEndData
        describe('When using .getEndData()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getEndData).toBeDefined();
            });

            it('Should not throw', function() {
                expect(child.getEndData.bind(child)).not.toThrow();
            });

            it('Should not throw if arguments assigned', function() {
                expect(child.getEndData.bind(child, child)).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getEndData()).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newDataStr, function() {
                        expect(child.getEndData()).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getEndData()).toEqual(newData);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newDataStr, function() {
                        expect(child.getEndData()).toEqual(newData);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getEndData()).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + newDataStr, function() {
                        expect(child.getEndData()).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getEndData()).toEqual(newData);
                        __done();
                    });
                });

                describe('When new values previously set before the end of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + newDataStr, function() {
                        expect(child.getEndData()).toEqual(newData);
                    });
                });
            });
        });
    });
});
