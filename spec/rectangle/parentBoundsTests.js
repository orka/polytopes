/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/rectangle'], function($numerus, $curves, $rectangle) {
    'use strict';

    var measurePerf = false;

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    //
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 222;
    var OriginY = 444;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 1000;
    var minY = -1000;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);

    //
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var Width = 423;
    var Height = 123;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);

    var nestedDepth = 3;
    //
    var child;
    var genesis;

    //precompute results
    var data = {
        left: X - OriginX,
        top: Y - OriginY,
        right: X - OriginX + Width,
        bottom: Y - OriginY + Height,
        x: X,
        y: Y,
        width: Width,
        height: Height,
        originX: OriginX,
        originY: OriginY
    };
    var dataStr = JSON.stringify(data);

    var newData = {
        left: newX - newOriginX,
        top: newY - newOriginY,
        right: newX - newOriginX + newWidth,
        bottom: newY - newOriginY + newHeight,
        x: newX,
        y: newY,
        width: newWidth,
        height: newHeight,
        originX: newOriginX,
        originY: newOriginY
    };
    var newDataStr = JSON.stringify(newData);

    var nestedData = {
        left: data.left * nestedDepth,
        top: data.top * nestedDepth,
        right: data.left * nestedDepth + data.width,
        bottom: data.top * nestedDepth + data.height,
        x: data.left * nestedDepth + data.originX,
        y: data.top * nestedDepth + data.originY,
        width: data.width,
        height: data.height,
        originX: data.originX,
        originY: data.originY
    };
    var nestedDataStr = JSON.stringify(nestedData);

    var nestedNewData = {
        left: newData.left * nestedDepth,
        top: newData.top * nestedDepth,
        right: newData.left * nestedDepth + newData.width,
        bottom: newData.top * nestedDepth + newData.height,
        x: newData.left * nestedDepth + newData.originX,
        y: newData.top * nestedDepth + newData.originY,
        width: newData.width,
        height: newData.height,
        originX: newData.originX,
        originY: newData.originY
    };
    var nestedNewDataStr = JSON.stringify(nestedNewData);

    var bounds = {
        left: data.left,
        top: data.top,
        right: data.right,
        bottom: data.bottom
    };
    var boundsStr = JSON.stringify(bounds);

    var nestedBounds = {
        left: nestedData.left,
        top: nestedData.top,
        right: nestedData.right,
        bottom: nestedData.bottom
    };
    var nestedBoundsStr = JSON.stringify(nestedBounds);

    var newBounds = {
        left: newData.left,
        top: newData.top,
        right: newData.right,
        bottom: newData.bottom
    };
    var newBoundsStr = JSON.stringify(newBounds);

    var nestedEndBounds = {
        left: nestedNewData.left,
        top: nestedNewData.top,
        right: nestedNewData.right,
        bottom: nestedNewData.bottom
    };
    var nestedEndBoundsStr = JSON.stringify(nestedEndBounds);


    function setNewValues(__child, __bypass) {
        return __child.setSize(newWidth, newHeight, __bypass).setCoord(newX, newY, __bypass).setOrigin(newOriginX, newOriginY, __bypass);
    }

    function make() {
        return $rectangle(numX.clone(), numY.clone(), numWidth.clone(), numHeight.clone(), numOriginX.clone(), numOriginY.clone());
    }

    function runOptPerf(__method) {
        if (measurePerf) {
            it('Optional --- Should have optimized version of method that is actually faster', function() {
                var _mark1 = performance.now();
                child[__method](genesis);
                var _mark2 = performance.now();
                child['__' + __method + '__'](genesis);
                var _mark3 = performance.now();
                expect((_mark2 - _mark1) - (_mark3 - _mark2)).toBeGreaterThan(0);
            });
        }
    }

    //------------------------------ ORPHANT


    var orphantSetup = {
        make: function() {
            child = make();
            genesis = null;
        },
        new: function() {
            orphantSetup.make();
            setNewValues(child);
        },
        newBypass: function() {
            orphantSetup.make();
            setNewValues(child, true);
        },
        newEnded: function(__done) {
            orphantSetup.make();
            setNewValues(child);
            setTimeout(__done, curveDuration);
        }
    };


    //------------------------------ NESTED


    var nestedSetup = {
        make: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
            }

            genesis = _child;
        },
        new: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newBypass: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child, true);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newEnded: function(__done) {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
            setTimeout(__done, curveDuration);
        }
    };

    describe('When using $rectangle parent intersection APIs', function() {
        //isInParentVertBounds
        describe('When using .isInParentVertBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentVertBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw if no parent is set', function() {
                    expect(child.isInParentVertBounds).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully above parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isInParentVertBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), true);
                    expect(child.isInParentVertBounds()).toEqual(false);
                });

                it('Should return "true" if parially in upper parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isInParentVertBounds()).toEqual(true);
                });

                it('Should return "true" if parially in lower parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, true);
                    expect(child.isInParentVertBounds()).toEqual(true);
                });

                it('Should return "true" if fully in parent bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isInParentVertBounds()).toEqual(true);
                });
            });
        });

        //isInParentHorBounds
        describe('When using .isInParentHorBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentHorBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw if no parent is set', function() {
                    expect(child.isInParentHorBounds).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isInParentHorBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), true);
                    expect(child.isInParentHorBounds()).toEqual(false);
                });

                it('Should return "true" if parially in left parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isInParentHorBounds()).toEqual(true);
                });

                it('Should return "true" if parially in right parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, true);
                    expect(child.isInParentHorBounds()).toEqual(true);
                });

                it('Should return "true" if fully in parent vertical bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isInParentHorBounds()).toEqual(true);
                });
            });
        });

        //isInParentBounds
        describe('When using .isInParentBounds(true) - strict mode (both axis)', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw if no parent is set', function() {
                    expect(child.isInParentBounds).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully above the parent bounds, but interestcs it on X axys', function() {
                    //suply bypassflag
                    child.setBottom(0, true).setLeft(0, true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "false" if fully below the parent bounds, but interestcs it on X axys', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), true).setLeft(0, false);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "false" if fully off the left the parent bounds, but interestcs it on Y axys', function() {
                    //suply bypassflag
                    child.setRight(0, true).setTop(0, true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "false" if fully off the right the parent bounds, but interestcs it on Y axys', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), true).setTop(0, true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });



                // BOTH



                it('Should return "fasle" if fully above and to the left of the parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true).setRight(0, true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "false" if fully above and to the right of the parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true).setRight(genesis.getWidth(), true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "fasle" if fully l and to the left of the parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true).setRight(0, true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "false" if fully above and to the right of the parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true).setRight(genesis.getWidth(), true);
                    expect(child.isInParentBounds(true)).toEqual(false);
                });

                it('Should return "true" if fully in parent bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isInParentBounds()).toEqual(true);
                });
            });
        });
        return;
        //isFullyInParentVertBounds
        describe('When using .isFullyInParentVertBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentVertBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentVertBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully above parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isFullyInParentVertBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), true);
                    expect(child.isFullyInParentVertBounds()).toEqual(false);
                });

                it('Should return "false" if parially in upper parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isFullyInParentVertBounds()).toEqual(false);
                });

                it('Should return "false" if parially in lower parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, true);
                    expect(child.isFullyInParentVertBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isFullyInParentVertBounds()).toEqual(true);
                });
            });
        });
        // /isFullyInParentHorBounds
        describe('When using .isFullyInParentHorBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentHorBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentHorBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isFullyInParentHorBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), true);
                    expect(child.isFullyInParentHorBounds()).toEqual(false);
                });

                it('Should return "false" if parially in left parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isFullyInParentHorBounds()).toEqual(false);
                });

                it('Should return "false" if parially in right parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, true);
                    expect(child.isFullyInParentHorBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent vertical bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isFullyInParentHorBounds()).toEqual(true);
                });
            });
        });

        //isFullyInParentBounds
        describe('When using .isFullyInParentBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if fully above parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right of parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if parially in upper parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if parially in Left parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if parially in lower parent bounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "false" if parially in Right parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, true);
                    expect(child.isFullyInParentBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent bounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], true)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], true);

                    expect(child.isFullyInParentBounds()).toEqual(true);
                });
            });
        });
    });
    return;
    describe('When using $rectangle parent intersection APIs - end bounds', function() {
        //isInParentVertEndBounds
        describe('When using .isInParentVertEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentVertEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isInParentVertEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully above parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), false);
                    expect(child.isInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "true" if parially in upper parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isInParentVertEndBounds()).toEqual(true);
                });

                it('Should return "true" if parially in lower parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, false);
                    expect(child.isInParentVertEndBounds()).toEqual(true);
                });

                it('Should return "true" if fully in parent Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isInParentVertEndBounds()).toEqual(true);
                });
            });
        });

        //isInParentHorEndBounds
        describe('When using .isInParentHorEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentHorEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isInParentHorEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), false);
                    expect(child.isInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "true" if parially in left parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isInParentHorEndBounds()).toEqual(true);
                });

                it('Should return "true" if parially in right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, false);
                    expect(child.isInParentHorEndBounds()).toEqual(true);
                });

                it('Should return "true" if fully in parent vertical Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isInParentHorEndBounds()).toEqual(true);
                });
            });
        });

        //isInParentEndBounds
        describe('When using .isInParentEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInParentEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isInParentEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully above parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), false);
                    expect(child.isInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right of parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), false);
                    expect(child.isInParentEndBounds()).toEqual(false);
                });

                it('Should return "true" if parially in upper parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isInParentEndBounds()).toEqual(true);
                });

                it('Should return "true" if parially in Left parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isInParentEndBounds()).toEqual(true);
                });

                it('Should return "true" if parially in lower parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, false);
                    expect(child.isInParentEndBounds()).toEqual(true);
                });

                it('Should return "true" if parially in Right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, false);
                    expect(child.isInParentEndBounds()).toEqual(true);
                });

                it('Should return "true" if fully in parent Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isInParentEndBounds()).toEqual(true);
                });
            });
        });

        //isFullyInParentVertEndBounds
        describe('When using .isFullyInParentVertEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentVertEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentVertEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully above parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isFullyInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), false);
                    expect(child.isFullyInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in upper parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isFullyInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in lower parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getEndHeight() - 1, false);
                    expect(child.isFullyInParentVertEndBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isFullyInParentVertEndBounds()).toEqual(true);
                });
            });
        });
        // /isFullyInParentHorEndBounds
        describe('When using .isFullyInParentHorEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentHorEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentHorEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isFullyInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), false);
                    expect(child.isFullyInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in left parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isFullyInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, false);
                    expect(child.isFullyInParentHorEndBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent vertical Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isFullyInParentHorEndBounds()).toEqual(true);
                });
            });
        });

        //isFullyInParentEndBounds
        describe('When using .isFullyInParentEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInParentEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return "false" if no parent is set', function() {
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return "false" if fully off the left of parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully above parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully below parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight(), false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if fully off the right of parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth(), false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in upper parent Endbounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in Left parent Endbounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in lower parent Endbounds', function() {
                    //suply bypassflag
                    child.setTop(child.getParent().getHeight() - 1, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "false" if parially in Right parent Endbounds', function() {
                    //suply bypassflag
                    child.setLeft(child.getParent().getWidth() - 1, false);
                    expect(child.isFullyInParentEndBounds()).toEqual(false);
                });

                it('Should return "true" if fully in parent Endbounds', function() {
                    //suply bypassflag
                    child
                        .setArrayBounds([10, 10, 80, 80], false)
                        .getParent()
                        .setArrayBounds([0, 0, 100, 100], false);

                    expect(child.isFullyInParentEndBounds()).toEqual(true);
                });
            });
        });
    });

});
