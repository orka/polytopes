/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/rectangle'], function($numerus, $curves, $rectangle) {
    'use strict';

    var measurePerf = false;

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    //
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 222;
    var OriginY = 444;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 1000;
    var minY = -1000;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);

    //
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var Width = 423;
    var Height = 123;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);

    var nestedDepth = 3;
    //
    var child;
    var genesis;

    //precompute results
    var data = {
        left: X - OriginX,
        top: Y - OriginY,
        right: X - OriginX + Width,
        bottom: Y - OriginY + Height,
        x: X,
        y: Y,
        width: Width,
        height: Height,
        originX: OriginX,
        originY: OriginY
    };
    var dataStr = JSON.stringify(data);

    var newData = {
        left: newX - newOriginX,
        top: newY - newOriginY,
        right: newX - newOriginX + newWidth,
        bottom: newY - newOriginY + newHeight,
        x: newX,
        y: newY,
        width: newWidth,
        height: newHeight,
        originX: newOriginX,
        originY: newOriginY
    };
    var newDataStr = JSON.stringify(newData);

    var nestedData = {
        left: data.left * nestedDepth,
        top: data.top * nestedDepth,
        right: data.left * nestedDepth + data.width,
        bottom: data.top * nestedDepth + data.height,
        x: data.left * nestedDepth + data.originX,
        y: data.top * nestedDepth + data.originY,
        width: data.width,
        height: data.height,
        originX: data.originX,
        originY: data.originY
    };
    var nestedDataStr = JSON.stringify(nestedData);

    var nestedNewData = {
        left: newData.left * nestedDepth,
        top: newData.top * nestedDepth,
        right: newData.left * nestedDepth + newData.width,
        bottom: newData.top * nestedDepth + newData.height,
        x: newData.left * nestedDepth + newData.originX,
        y: newData.top * nestedDepth + newData.originY,
        width: newData.width,
        height: newData.height,
        originX: newData.originX,
        originY: newData.originY
    };
    var nestedNewDataStr = JSON.stringify(nestedNewData);

    var bounds = {
        left: data.left,
        top: data.top,
        right: data.right,
        bottom: data.bottom
    };
    var boundsStr = JSON.stringify(bounds);

    var nestedBounds = {
        left: nestedData.left,
        top: nestedData.top,
        right: nestedData.right,
        bottom: nestedData.bottom
    };
    var nestedBoundsStr = JSON.stringify(nestedBounds);

    var newBounds = {
        left: newData.left,
        top: newData.top,
        right: newData.right,
        bottom: newData.bottom
    };
    var newBoundsStr = JSON.stringify(newBounds);

    var nestedEndBounds = {
        left: nestedNewData.left,
        top: nestedNewData.top,
        right: nestedNewData.right,
        bottom: nestedNewData.bottom
    };
    var nestedEndBoundsStr = JSON.stringify(nestedEndBounds);


    function setNewValues(__child, __bypass) {
        return __child.setSize(newWidth, newHeight, __bypass).setCoord(newX, newY, __bypass).setOrigin(newOriginX, newOriginY, __bypass);
    }

    function make() {
        return $rectangle(numX.clone(), numY.clone(), numWidth.clone(), numHeight.clone(), numOriginX.clone(), numOriginY.clone());
    }

    function runOptPerf(__method) {
        if (measurePerf) {
            it('Optional --- Should have optimized version of method that is actually faster', function() {
                var _mark1 = performance.now();
                child[__method](genesis);
                var _mark2 = performance.now();
                child['__' + __method + '__'](genesis);
                var _mark3 = performance.now();
                expect((_mark2 - _mark1) - (_mark3 - _mark2)).toBeGreaterThan(0);
            });
        }
    }

    //------------------------------ ORPHANT


    var orphantSetup = {
        make: function() {
            child = make();
            genesis = null;
        },
        new: function() {
            orphantSetup.make();
            setNewValues(child);
        },
        newBypass: function() {
            orphantSetup.make();
            setNewValues(child, true);
        },
        newEnded: function(__done) {
            orphantSetup.make();
            setNewValues(child);
            setTimeout(__done, curveDuration);
        }
    };


    //------------------------------ NESTED


    var nestedSetup = {
        make: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
            }

            genesis = _child;
        },
        new: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newBypass: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child, true);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newEnded: function(__done) {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
            setTimeout(__done, curveDuration);
        }
    };


    describe('When using $rectangle relative intersection APIs', function() {
        //isInRelativeVertBounds
        describe('When using .isInRelativeVertBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInRelativeVertBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeVertBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeVertBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeVertBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully above target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), true);
                    expect(child.isInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isInRelativeVertBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially in lower target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, true);
                    expect(child.isInRelativeVertBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isInRelativeVertBounds(genesis)).toEqual(true);
                });
            });
        });

        //isInRelativeHorBounds
        describe('When using .isInRelativeHorBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInRelativeHorBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeHorBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeHorBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeHorBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully to the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), true);
                    expect(child.isInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isInRelativeHorBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within right target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, true);
                    expect(child.isInRelativeHorBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isInRelativeHorBounds(genesis)).toEqual(true);
                });
            });
        });

        //isInRelativeBounds
        describe('When using .isInRelativeBounds()', function() {
            beforeEach(function() {
                var _instance = child;
                nestedSetup.make();
                //reset all positions to 0
                while (_instance.getParent()) {
                    _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                }
            });

            it('Should be defined', function() {
                expect(child.isInRelativeBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully obove of target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within bottom of target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, true);
                    expect(child.isInRelativeBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isInRelativeBounds(genesis)).toEqual(true);
                });
            });
        });



        //fully/relative



        //isFullyInRelativeVertBounds
        describe('When using .isFullyInRelativeVertBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInRelativeVertBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeVertBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeVertBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeVertBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully above target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isFullyInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), true);
                    expect(child.isFullyInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isFullyInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in lower target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, true);
                    expect(child.isFullyInRelativeVertBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isFullyInRelativeVertBounds(genesis)).toEqual(true);
                });
            });
        });

        //isFullyInRelativeHorBounds
        describe('When using .isFullyInRelativeHorBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInRelativeHorBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeHorBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeHorBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeHorBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully to the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isFullyInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), true);
                    expect(child.isFullyInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isFullyInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within right target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, true);
                    expect(child.isFullyInRelativeHorBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isFullyInRelativeHorBounds(genesis)).toEqual(true);
                });
            });
        });

        //isFullyInRelativeBounds
        describe('When using .isFullyInRelativeBounds()', function() {
            beforeEach(function() {
                var _instance = child;
                nestedSetup.make();
                //reset all positions to 0
                while (_instance.getParent()) {
                    _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                }
            });

            it('Should be defined', function() {
                expect(child.isFullyInRelativeBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], true);
                    }
                });

                it('Should return "false" if fully the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully obove of target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within bottom of target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, true);
                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], true);

                    expect(child.isFullyInRelativeBounds(genesis)).toEqual(true);
                });
            });
        });
    });



    //END BOUNDS



    describe('When using $rectangle relative intersection APIs - end bounds', function() {
        //isInRelativeVertEndBounds
        describe('When using .isInRelativeVertEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInRelativeVertEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeVertEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeVertEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeVertEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], false);
                    }
                });

                it('Should return "false" if fully above target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), false);
                    expect(child.isInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isInRelativeVertEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially in lower target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, false);
                    expect(child.isInRelativeVertEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);

                    expect(child.isInRelativeVertEndBounds(genesis)).toEqual(true);
                });
            });
        });

        //isInRelativeHorEndBounds
        describe('When using .isInRelativeHorEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInRelativeHorEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeHorEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeHorEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeHorEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([0, 0, 100, 100], false);
                    }
                });

                it('Should return "false" if fully to the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), false);
                    expect(child.isInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isInRelativeHorEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within right target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, false);
                    expect(child.isInRelativeHorEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);

                    expect(child.isInRelativeHorEndBounds(genesis)).toEqual(true);
                });
            });
        });

        //isInRelativeEndBounds
        describe('When using .isInRelativeEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isInRelativeEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isInRelativeEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isInRelativeEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isInRelativeEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    child.setArrayBounds([0, 0, 100, 100], false);
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([100, 1000, 100, 100], false);
                    }
                });

                it('Should return "false" if fully the left of target parent bounds - strict', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isInRelativeEndBounds(genesis, true)).toEqual(false);
                });

                it('Should return "false" if fully the left and top of target parent bounds - non strict', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isInRelativeEndBounds(genesis, true)).toEqual(false);
                });

                it('Should return "false" if fully obove of target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isInRelativeEndBounds(genesis, true)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), false);
                    expect(child.isInRelativeEndBounds(genesis, true)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), false);
                    expect(child.isInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isInRelativeEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isInRelativeEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within bottom of target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, false);
                    expect(child.isInRelativeEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if parially within right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, false);
                    expect(child.isInRelativeEndBounds(genesis)).toEqual(true);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);

                    expect(child.isInRelativeEndBounds(genesis)).toEqual(true);
                });
            });
        });



        //fully/relative



        //isFullyInRelativeVertEndBounds
        describe('When using .isFullyInRelativeVertEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInRelativeVertEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeVertEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeVertEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeVertEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    child.setArrayBounds([0, 0, 100, 100], false);
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([100, 1000, 100, 100], false);
                    }
                });

                it('Should return "false" if fully above target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isFullyInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), false);
                    expect(child.isFullyInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isFullyInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in lower target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, false);
                    expect(child.isFullyInRelativeVertEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);

                    expect(child.isFullyInRelativeVertEndBounds(genesis)).toEqual(true);
                });
            });
        });

        //isFullyInRelativeHorEndBounds
        describe('When using .isFullyInRelativeHorEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInRelativeHorEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeHorEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeHorEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeHorEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();
                    child.setArrayBounds([0, 0, 100, 100], false);
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([1000, 1000, 100, 100], false);
                    }
                });

                it('Should return "false" if fully to the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isFullyInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), false);
                    expect(child.isFullyInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isFullyInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within right target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, false);
                    expect(child.isFullyInRelativeHorEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);

                    expect(child.isFullyInRelativeHorEndBounds(genesis)).toEqual(true);
                });
            });
        });

        //isFullyInRelativeEndBounds
        describe('When using .isFullyInRelativeEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.isFullyInRelativeEndBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should throw error if no arguments provided', function() {
                    expect(child.isFullyInRelativeEndBounds.bind(child)).toThrow();
                });

                it('Should throw error if arguments provided is not a parent of instance', function() {
                    expect(child.isFullyInRelativeEndBounds.bind(child, genesis)).toThrow();
                    expect(child.isFullyInRelativeEndBounds.bind(child, child)).toThrow();
                });
            });

            describe('While nested', function() {
                beforeEach(function() {
                    var _instance = child;
                    nestedSetup.make();

                    child.setArrayBounds([0, 0, 100, 100], false);
                    //reset all positions to 0
                    while (_instance.getParent()) {
                        _instance = _instance.getParent().setArrayBounds([1000, 1000, 100, 100], false);
                    }
                });

                it('Should return "false" if fully the left of target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(0, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully obove of target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(0, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully to the right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth(), false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if fully below target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight(), false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially in upper target parent bounds', function() {
                    //suply bypassflag
                    child.setBottom(1, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within left target parent bounds', function() {
                    //suply bypassflag
                    child.setRight(1, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within bottom of target parent bounds', function() {
                    //suply bypassflag
                    child.setTop(genesis.getHeight() - 1, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "false" if parially within right of target parent bounds', function() {
                    //suply bypassflag
                    child.setLeft(genesis.getWidth() - 1, false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(false);
                });

                it('Should return "true" if fully in target parent bounds', function() {
                    //suply bypassflag
                    genesis.setArrayBounds([1000, 11000, 100, 100], false);
                    expect(child.isFullyInRelativeEndBounds(genesis)).toEqual(true);
                });
            });
        });
    });
});
