/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/rectangle'], function($numerus, $curves, $rectangle) {
    'use strict';

    var measurePerf = false;

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    //
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 222;
    var OriginY = 444;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);

    //
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var Width = 222;
    var Height = 444;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);

    var nestedDepth = 3;
    //
    var child;
    var genesis;

    //precompute results
    var data = {
        left: X - OriginX,
        top: Y - OriginY,
        right: X - OriginX + Width,
        bottom: Y - OriginY + Height,
        x: X,
        y: Y,
        width: Width,
        height: Height,
        originX: OriginX,
        originY: OriginY
    };
    var dataStr = JSON.stringify(data);

    var newData = {
        left: newX - newOriginX,
        top: newY - newOriginY,
        right: newX - newOriginX + newWidth,
        bottom: newY - newOriginY + newHeight,
        x: newX,
        y: newY,
        width: newWidth,
        height: newHeight,
        originX: newOriginX,
        originY: newOriginY
    };
    var newDataStr = JSON.stringify(newData);

    var nestedData = {
        left: data.left * nestedDepth,
        top: data.top * nestedDepth,
        right: data.left * nestedDepth + data.width,
        bottom: data.top * nestedDepth + data.height,
        x: data.left * nestedDepth + data.originX,
        y: data.top * nestedDepth + data.originY,
        width: data.width,
        height: data.height,
        originX: data.originX,
        originY: data.originY
    };
    var nestedDataStr = JSON.stringify(nestedData);

    var nestedNewData = {
        left: newData.left * nestedDepth,
        top: newData.top * nestedDepth,
        right: newData.left * nestedDepth + newData.width,
        bottom: newData.top * nestedDepth + newData.height,
        x: newData.left * nestedDepth + newData.originX,
        y: newData.top * nestedDepth + newData.originY,
        width: newData.width,
        height: newData.height,
        originX: newData.originX,
        originY: newData.originY
    };
    var nestedNewDataStr = JSON.stringify(nestedNewData);

    var bounds = {
        left: data.left,
        top: data.top,
        right: data.right,
        bottom: data.bottom
    };
    var boundsStr = JSON.stringify(bounds);

    var nestedBounds = {
        left: nestedData.left,
        top: nestedData.top,
        right: nestedData.right,
        bottom: nestedData.bottom
    };
    var nestedBoundsStr = JSON.stringify(nestedBounds);

    var newBounds = {
        left: newData.left,
        top: newData.top,
        right: newData.right,
        bottom: newData.bottom
    };
    var newBoundsStr = JSON.stringify(newBounds);

    var nestedEndBounds = {
        left: nestedNewData.left,
        top: nestedNewData.top,
        right: nestedNewData.right,
        bottom: nestedNewData.bottom
    };
    var nestedEndBoundsStr = JSON.stringify(nestedEndBounds);


    function setNewValues(__child, __bypass) {
        return __child.setSize(newWidth, newHeight, __bypass).setCoord(newX, newY, __bypass).setOrigin(newOriginX, newOriginY, __bypass);
    }

    function make() {
        return $rectangle(numX.clone(), numY.clone(), numWidth.clone(), numHeight.clone(), numOriginX.clone(), numOriginY.clone());
    }

    function runOptPerf(__method) {
        if (measurePerf) {
            it('Optional --- Should have optimized version of method that is actually faster', function() {
                var _mark1 = performance.now();
                child[__method](genesis);
                var _mark2 = performance.now();
                child['__' + __method + '__'](genesis);
                var _mark3 = performance.now();
                expect((_mark2 - _mark1) - (_mark3 - _mark2)).toBeGreaterThan(0);
            });
        }
    }

    //------------------------------ ORPHANT


    var orphantSetup = {
        make: function() {
            child = make();
            genesis = null;
        },
        new: function() {
            orphantSetup.make();
            setNewValues(child);
        },
        newBypass: function() {
            orphantSetup.make();
            setNewValues(child, true);
        },
        newEnded: function(__done) {
            orphantSetup.make();
            setNewValues(child);
            setTimeout(__done, curveDuration);
        }
    };


    //------------------------------ NESTED


    var nestedSetup = {
        make: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
            }

            genesis = _child;
        },
        new: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newBypass: function() {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child, true);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
        },
        newEnded: function(__done) {
            var _i, _child;
            orphantSetup.make();
            _child = child;
            setNewValues(_child);
            for (_i = 0; _i < nestedDepth; _i += 1) {
                _child.setParent(_child.clone());
                _child = _child.getParent();
                setNewValues(_child);
            }

            genesis = _child;
            setTimeout(__done, curveDuration);
        }
    };



    describe('When using $rectangle relative getters APIs', function() {
        //getRelativeX
        describe('When using .getRelativeX()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeX).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndY.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndY.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndY.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getRelativeX(child.clone())).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getRelativeX(child.clone())).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getRelativeX(child.clone())).toEqual(newData.x);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.x, function() {
                    expect(child.getRelativeX(genesis)).toEqual(nestedData.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getRelativeX(genesis)).toEqual(nestedNewData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.x, function(__done) {
                        child.update();
                        expect(child.getRelativeX(genesis)).toEqual(nestedNewData.x);
                        __done();
                    });
                });
            });
        });
        //getRelativeY
        describe('When using .getRelativeY()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeY).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndY.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndY.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndY.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getRelativeY(child.clone())).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getRelativeY(child.clone())).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getRelativeY(child.clone())).toEqual(newData.y);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.y, function() {
                    expect(child.getRelativeY(genesis)).toEqual(nestedData.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getRelativeY(genesis)).toEqual(nestedNewData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.y, function(__done) {
                        child.update();
                        expect(child.getRelativeY(genesis)).toEqual(nestedNewData.y);
                        __done();
                    });
                });
            });
        });
        //getRelativeLeft
        describe('When using .getRelativeLeft()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeLeft).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeLeft.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeLeft.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeLeft.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getRelativeLeft(child.clone())).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getRelativeLeft(child.clone())).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getRelativeLeft(child.clone())).toEqual(newData.left);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.left, function() {
                    expect(child.getRelativeLeft(genesis)).toEqual(nestedData.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getRelativeLeft(genesis)).toEqual(nestedNewData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.left, function(__done) {
                        child.update();
                        expect(child.getRelativeLeft(genesis)).toEqual(nestedNewData.left);
                        __done();
                    });
                });
            });
        });
        //getRelativeTop
        describe('When using .getRelativeTop()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeTop).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeTop.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeTop.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeTop.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getRelativeTop(child.clone())).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getRelativeTop(child.clone())).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getRelativeTop(child.clone())).toEqual(newData.top);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.top, function() {
                    expect(child.getRelativeTop(genesis)).toEqual(nestedData.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getRelativeTop(genesis)).toEqual(nestedNewData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.top, function(__done) {
                        child.update();
                        expect(child.getRelativeTop(genesis)).toEqual(nestedNewData.top);
                        __done();
                    });
                });
            });
        });
        //getRelativeRight
        describe('When using .getRelativeRight()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeRight).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeRight.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeRight.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeRight.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getRelativeRight(child.clone())).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getRelativeRight(child.clone())).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getRelativeRight(child.clone())).toEqual(newData.right);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.right, function() {
                    expect(child.getRelativeRight(genesis)).toEqual(nestedData.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getRelativeRight(genesis)).toEqual(nestedNewData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.right, function(__done) {
                        child.update();
                        expect(child.getRelativeRight(genesis)).toEqual(nestedNewData.right);
                        __done();
                    });
                });
            });
        });
        //getRelativeBottom
        describe('When using .getRelativeBottom()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeBottom).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeBottom.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeBottom.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeBottom.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getRelativeBottom(child.clone())).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getRelativeBottom(child.clone())).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getRelativeBottom(child.clone())).toEqual(newData.bottom);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.bottom, function() {
                    expect(child.getRelativeBottom(genesis)).toEqual(nestedData.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getRelativeBottom(genesis)).toEqual(nestedNewData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.bottom, function(__done) {
                        child.update();
                        expect(child.getRelativeBottom(genesis)).toEqual(nestedNewData.bottom);
                        __done();
                    });
                });
            });
        });
        //getRelativeBounds
        describe('When using .getRelativeBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeBounds).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeBounds.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeBounds.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeBounds.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getRelativeBounds(child.clone())).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getRelativeBounds(child.clone())).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getRelativeBounds(child.clone())).toEqual(newBounds);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedBoundsStr, function() {
                    expect(child.getRelativeBounds(genesis)).toEqual(nestedBounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getRelativeBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedEndBoundsStr, function(__done) {
                        child.update();
                        expect(child.getRelativeBounds(genesis)).toEqual(nestedEndBounds);
                        __done();
                    });
                });
            });
        });
        //getRelativeData
        describe('When using .getRelativeData()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeData).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeData.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeData.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeData.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getRelativeData(child.clone())).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data :' + newDataStr, function() {
                        expect(child.getRelativeData(child.clone())).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getRelativeData(child.clone())).toEqual(newData);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getRelativeData(genesis)).toEqual(nestedData);
                });

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getRelativeData(genesis)).toEqual(nestedData);
                });

                it('Should return global position if parent is not part of lineage :' + nestedDataStr, function() {
                    expect(child.getRelativeData(child.clone())).toEqual(nestedData);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getRelativeData(genesis)).toEqual(nestedNewData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewDataStr, function(__done) {
                        child.update();
                        expect(child.getRelativeData(genesis)).toEqual(nestedNewData);
                        __done();
                    });
                });
            });
        });
    });

    describe('When using $rectangle relative end getters APIs', function() {
        //getRelativeEndX
        describe('When using .getRelativeEndX()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndX).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndY.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndY.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndY.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getRelativeEndX(child.clone())).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getRelativeEndX(child.clone())).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getRelativeEndX(child.clone())).toEqual(newData.x);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getRelativeEndX(child.clone())).toEqual(newData.x);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.x, function() {
                    expect(child.getRelativeEndX(genesis)).toEqual(nestedData.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getRelativeEndX(genesis)).toEqual(nestedNewData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.x, function(__done) {
                        child.update();
                        expect(child.getRelativeEndX(genesis)).toEqual(nestedNewData.x);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getRelativeEndX(child.clone())).toEqual(nestedNewData.x);
                    });
                });
            });
        });
        //getRelativeEndY
        describe('When using .getRelativeEndY()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndY).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndY.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndY.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndY.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getRelativeEndY(child.clone())).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getRelativeEndY(child.clone())).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getRelativeEndY(child.clone())).toEqual(newData.y);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getRelativeEndY(child.clone())).toEqual(newData.y);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.y, function() {
                    expect(child.getRelativeEndY(genesis)).toEqual(nestedData.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getRelativeEndY(genesis)).toEqual(nestedNewData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.y, function(__done) {
                        child.update();
                        expect(child.getRelativeEndY(genesis)).toEqual(nestedNewData.y);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getRelativeEndY(child.clone())).toEqual(nestedNewData.y);
                    });
                });
            });
        });
        //getRelativeEndLeft
        describe('When using .getRelativeEndLeft()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndLeft).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndLeft.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndLeft.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndLeft.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getRelativeEndLeft(child.clone())).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getRelativeEndLeft(child.clone())).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getRelativeEndLeft(child.clone())).toEqual(newData.left);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getRelativeEndLeft(child.clone())).toEqual(newData.left);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.left, function() {
                    expect(child.getRelativeEndLeft(genesis)).toEqual(nestedData.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getRelativeEndLeft(genesis)).toEqual(nestedNewData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.left, function(__done) {
                        child.update();
                        expect(child.getRelativeEndLeft(genesis)).toEqual(nestedNewData.left);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getRelativeEndLeft(child.clone())).toEqual(nestedNewData.left);
                    });
                });
            });
        });
        //getRelativeEndTop
        describe('When using .getRelativeEndTop()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndTop).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndTop.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndTop.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndTop.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getRelativeEndTop(child.clone())).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getRelativeEndTop(child.clone())).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getRelativeEndTop(child.clone())).toEqual(newData.top);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getRelativeEndTop(child.clone())).toEqual(newData.top);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.top, function() {
                    expect(child.getRelativeEndTop(genesis)).toEqual(nestedData.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getRelativeEndTop(genesis)).toEqual(nestedNewData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.top, function(__done) {
                        child.update();
                        expect(child.getRelativeEndTop(genesis)).toEqual(nestedNewData.top);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getRelativeEndTop(child.clone())).toEqual(nestedNewData.top);
                    });
                });
            });
        });
        //getRelativeEndRight
        describe('When using .getRelativeEndRight()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndRight).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndRight.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndRight.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndRight.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getRelativeEndRight(child.clone())).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getRelativeEndRight(child.clone())).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getRelativeEndRight(child.clone())).toEqual(newData.right);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getRelativeEndRight(child.clone())).toEqual(newData.right);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.right, function() {
                    expect(child.getRelativeEndRight(genesis)).toEqual(nestedData.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getRelativeEndRight(genesis)).toEqual(nestedNewData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.right, function(__done) {
                        child.update();
                        expect(child.getRelativeEndRight(genesis)).toEqual(nestedNewData.right);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getRelativeEndRight(child.clone())).toEqual(nestedNewData.right);
                    });
                });
            });
        });
        //getRelativeEndBottom
        describe('When using .getRelativeEndBottom()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndBottom).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndBottom.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndBottom.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndBottom.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getRelativeEndBottom(child.clone())).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getRelativeEndBottom(child.clone())).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getRelativeEndBottom(child.clone())).toEqual(newData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getRelativeEndBottom(child.clone())).toEqual(newData.bottom);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.bottom, function() {
                    expect(child.getRelativeEndBottom(genesis)).toEqual(nestedData.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getRelativeEndBottom(genesis)).toEqual(nestedNewData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.bottom, function(__done) {
                        child.update();
                        expect(child.getRelativeEndBottom(genesis)).toEqual(nestedNewData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getRelativeEndBottom(child.clone())).toEqual(nestedNewData.bottom);
                    });
                });
            });
        });
        //getRelativeEndBounds
        describe('When using .getRelativeEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndBounds).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndBounds.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndBounds.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndBounds.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getRelativeEndBounds(child.clone())).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getRelativeEndBounds(child.clone())).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getRelativeEndBounds(child.clone())).toEqual(newBounds);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getRelativeEndBounds(child.clone())).toEqual(newBounds);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedBoundsStr, function() {
                    expect(child.getRelativeEndBounds(genesis)).toEqual(nestedBounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getRelativeEndBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedEndBoundsStr, function(__done) {
                        child.update();
                        expect(child.getRelativeEndBounds(genesis)).toEqual(nestedEndBounds);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getRelativeEndBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });
            });
        });
        //getRelativeEndData
        describe('When using .getRelativeEndData()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getRelativeEndData).toBeDefined();
            });

            it('Should throw if no argument is passed argument', function() {
                expect(child.getRelativeEndData.bind(child)).toThrow();
            });

            it('Should throw if argument provided is a self', function() {
                expect(child.getRelativeEndData.bind(child, child)).toThrow();
            });

            it('Should not throw if argument is valid', function() {
                expect(child.getRelativeEndData.bind(child, child.clone())).not.toThrow();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getRelativeEndData(child.clone())).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data :' + newDataStr, function() {
                        expect(child.getRelativeEndData(child.clone())).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getRelativeEndData(child.clone())).toEqual(newData);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getRelativeEndData(child.clone())).toEqual(newData);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getRelativeEndData(genesis)).toEqual(nestedData);
                });

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getRelativeEndData(genesis)).toEqual(nestedData);
                });

                it('Should return global position if parent is not part of lineage :' + nestedDataStr, function() {
                    expect(child.getRelativeEndData(child.clone())).toEqual(nestedData);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getRelativeEndData(genesis)).toEqual(nestedNewData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewDataStr, function(__done) {
                        child.update();
                        expect(child.getRelativeEndData(genesis)).toEqual(nestedNewData);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getRelativeEndData(genesis)).toEqual(nestedNewData);
                    });
                });
            });
        });
    });

    describe('When using $rectangle relative getters APIs', function() {
        //getRelativeX
        describe('When using .getGlobalX()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalX).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getGlobalX(child.clone())).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getGlobalX(child.clone())).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getGlobalX(child.clone())).toEqual(newData.x);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.x, function() {
                    expect(child.getGlobalX(genesis)).toEqual(nestedData.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getGlobalX(genesis)).toEqual(nestedNewData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.x, function(__done) {
                        child.update();
                        expect(child.getGlobalX(genesis)).toEqual(nestedNewData.x);
                        __done();
                    });
                });
            });
        });

        //getGlobalY
        describe('When using .getGlobalY()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalY).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getGlobalY(child.clone())).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getGlobalY(child.clone())).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getGlobalY(child.clone())).toEqual(newData.y);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.y, function() {
                    expect(child.getGlobalY(genesis)).toEqual(nestedData.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getGlobalY(genesis)).toEqual(nestedNewData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.y, function(__done) {
                        child.update();
                        expect(child.getGlobalY(genesis)).toEqual(nestedNewData.y);
                        __done();
                    });
                });
            });
        });

        //getGlobalLeft
        describe('When using .getGlobalLeft()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalLeft).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getGlobalLeft(child.clone())).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getGlobalLeft(child.clone())).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getGlobalLeft(child.clone())).toEqual(newData.left);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.left, function() {
                    expect(child.getGlobalLeft(genesis)).toEqual(nestedData.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getGlobalLeft(genesis)).toEqual(nestedNewData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.left, function(__done) {
                        child.update();
                        expect(child.getGlobalLeft(genesis)).toEqual(nestedNewData.left);
                        __done();
                    });
                });
            });
        });
        //getGlobalTop
        describe('When using .getGlobalTop()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalTop).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getGlobalTop(child.clone())).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getGlobalTop(child.clone())).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getGlobalTop(child.clone())).toEqual(newData.top);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.top, function() {
                    expect(child.getGlobalTop(genesis)).toEqual(nestedData.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getGlobalTop(genesis)).toEqual(nestedNewData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.top, function(__done) {
                        child.update();
                        expect(child.getGlobalTop(genesis)).toEqual(nestedNewData.top);
                        __done();
                    });
                });
            });
        });
        //getGlobalRight
        describe('When using .getGlobalRight()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalRight).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getGlobalRight(child.clone())).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getGlobalRight(child.clone())).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getGlobalRight(child.clone())).toEqual(newData.right);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.right, function() {
                    expect(child.getGlobalRight(genesis)).toEqual(nestedData.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getGlobalRight(genesis)).toEqual(nestedNewData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.right, function(__done) {
                        child.update();
                        expect(child.getGlobalRight(genesis)).toEqual(nestedNewData.right);
                        __done();
                    });
                });
            });
        });
        //getGlobalBottom
        describe('When using .getGlobalBottom()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalBottom).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getGlobalBottom(child.clone())).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getGlobalBottom(child.clone())).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getGlobalBottom(child.clone())).toEqual(newData.bottom);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.bottom, function() {
                    expect(child.getGlobalBottom(genesis)).toEqual(nestedData.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getGlobalBottom(genesis)).toEqual(nestedNewData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.bottom, function(__done) {
                        child.update();
                        expect(child.getGlobalBottom(genesis)).toEqual(nestedNewData.bottom);
                        __done();
                    });
                });
            });
        });
        //getGlobalBounds
        describe('When using .getGlobalBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalBounds).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getGlobalBounds(child.clone())).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getGlobalBounds(child.clone())).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getGlobalBounds(child.clone())).toEqual(newBounds);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedBoundsStr, function() {
                    expect(child.getGlobalBounds(genesis)).toEqual(nestedBounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getGlobalBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedEndBoundsStr, function(__done) {
                        child.update();
                        expect(child.getGlobalBounds(genesis)).toEqual(nestedEndBounds);
                        __done();
                    });
                });
            });
        });
        //getGlobalData
        describe('When using .getGlobalData()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalData).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getGlobalData(child.clone())).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data :' + newDataStr, function() {
                        expect(child.getGlobalData(child.clone())).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getGlobalData(child.clone())).toEqual(newData);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getGlobalData(genesis)).toEqual(nestedData);
                });

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getGlobalData(genesis)).toEqual(nestedData);
                });

                it('Should return global position if parent is not part of lineage :' + nestedDataStr, function() {
                    expect(child.getGlobalData(child.clone())).toEqual(nestedData);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getGlobalData(genesis)).toEqual(nestedNewData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewDataStr, function(__done) {
                        child.update();
                        expect(child.getGlobalData(genesis)).toEqual(nestedNewData);
                        __done();
                    });
                });
            });
        });
    });

    describe('When using $rectangle relative end getters APIs', function() {
        //getGlobalEndX
        describe('When using .getGlobalEndX()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndX).toBeDefined();
            });


            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.x, function() {
                    expect(child.getGlobalEndX(child.clone())).toEqual(data.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getGlobalEndX(child.clone())).toEqual(newData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.x, function(__done) {
                        child.update();
                        expect(child.getGlobalEndX(child.clone())).toEqual(newData.x);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.x, function() {
                        expect(child.getGlobalEndX(child.clone())).toEqual(newData.x);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.x, function() {
                    expect(child.getGlobalEndX(genesis)).toEqual(nestedData.x);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getGlobalEndX(genesis)).toEqual(nestedNewData.x);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.x, function(__done) {
                        child.update();
                        expect(child.getGlobalEndX(genesis)).toEqual(nestedNewData.x);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.x, function() {
                        expect(child.getGlobalEndX(child.clone())).toEqual(nestedNewData.x);
                    });
                });
            });
        });
        //getGlobalEndY
        describe('When using .getGlobalEndY()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndY).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.y, function() {
                    expect(child.getGlobalEndY(child.clone())).toEqual(data.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getGlobalEndY(child.clone())).toEqual(newData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.y, function(__done) {
                        child.update();
                        expect(child.getGlobalEndY(child.clone())).toEqual(newData.y);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.y, function() {
                        expect(child.getGlobalEndY(child.clone())).toEqual(newData.y);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.y, function() {
                    expect(child.getGlobalEndY(genesis)).toEqual(nestedData.y);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getGlobalEndY(genesis)).toEqual(nestedNewData.y);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.y, function(__done) {
                        child.update();
                        expect(child.getGlobalEndY(genesis)).toEqual(nestedNewData.y);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.y, function() {
                        expect(child.getGlobalEndY(child.clone())).toEqual(nestedNewData.y);
                    });
                });
            });
        });
        //getGlobalEndLeft
        describe('When using .getGlobalEndLeft()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndLeft).toBeDefined();
            });

            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.left, function() {
                    expect(child.getGlobalEndLeft(child.clone())).toEqual(data.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getGlobalEndLeft(child.clone())).toEqual(newData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.left, function(__done) {
                        child.update();
                        expect(child.getGlobalEndLeft(child.clone())).toEqual(newData.left);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.left, function() {
                        expect(child.getGlobalEndLeft(child.clone())).toEqual(newData.left);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.left, function() {
                    expect(child.getGlobalEndLeft(genesis)).toEqual(nestedData.left);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getGlobalEndLeft(genesis)).toEqual(nestedNewData.left);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.left, function(__done) {
                        child.update();
                        expect(child.getGlobalEndLeft(genesis)).toEqual(nestedNewData.left);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.left, function() {
                        expect(child.getGlobalEndLeft(child.clone())).toEqual(nestedNewData.left);
                    });
                });
            });
        });
        //getGlobalEndTop
        describe('When using .getGlobalEndTop()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndTop).toBeDefined();
            });


            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.top, function() {
                    expect(child.getGlobalEndTop(child.clone())).toEqual(data.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getGlobalEndTop(child.clone())).toEqual(newData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.top, function(__done) {
                        child.update();
                        expect(child.getGlobalEndTop(child.clone())).toEqual(newData.top);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.top, function() {
                        expect(child.getGlobalEndTop(child.clone())).toEqual(newData.top);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.top, function() {
                    expect(child.getGlobalEndTop(genesis)).toEqual(nestedData.top);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getGlobalEndTop(genesis)).toEqual(nestedNewData.top);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.top, function(__done) {
                        child.update();
                        expect(child.getGlobalEndTop(genesis)).toEqual(nestedNewData.top);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.top, function() {
                        expect(child.getGlobalEndTop(child.clone())).toEqual(nestedNewData.top);
                    });
                });
            });
        });
        //getGlobalEndRight
        describe('When using .getGlobalEndRight()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndRight).toBeDefined();
            });



            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.right, function() {
                    expect(child.getGlobalEndRight(child.clone())).toEqual(data.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getGlobalEndRight(child.clone())).toEqual(newData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.right, function(__done) {
                        child.update();
                        expect(child.getGlobalEndRight(child.clone())).toEqual(newData.right);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.right, function() {
                        expect(child.getGlobalEndRight(child.clone())).toEqual(newData.right);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.right, function() {
                    expect(child.getGlobalEndRight(genesis)).toEqual(nestedData.right);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getGlobalEndRight(genesis)).toEqual(nestedNewData.right);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.right, function(__done) {
                        child.update();
                        expect(child.getGlobalEndRight(genesis)).toEqual(nestedNewData.right);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.right, function() {
                        expect(child.getGlobalEndRight(child.clone())).toEqual(nestedNewData.right);
                    });
                });
            });
        });
        //getGlobalEndBottom
        describe('When using .getGlobalEndBottom()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndBottom).toBeDefined();
            });


            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + data.bottom, function() {
                    expect(child.getGlobalEndBottom(child.clone())).toEqual(data.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getGlobalEndBottom(child.clone())).toEqual(newData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newData.bottom, function(__done) {
                        child.update();
                        expect(child.getGlobalEndBottom(child.clone())).toEqual(newData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newData.bottom, function() {
                        expect(child.getGlobalEndBottom(child.clone())).toEqual(newData.bottom);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedData.bottom, function() {
                    expect(child.getGlobalEndBottom(genesis)).toEqual(nestedData.bottom);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getGlobalEndBottom(genesis)).toEqual(nestedNewData.bottom);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewData.bottom, function(__done) {
                        child.update();
                        expect(child.getGlobalEndBottom(genesis)).toEqual(nestedNewData.bottom);
                        __done();
                    });
                });

                describe('When new values previously set at the beginning of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewData.bottom, function() {
                        expect(child.getGlobalEndBottom(child.clone())).toEqual(nestedNewData.bottom);
                    });
                });
            });
        });
        //getGlobalEndBounds
        describe('When using .getGlobalEndBounds()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndBounds).toBeDefined();
            });


            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + boundsStr, function() {
                    expect(child.getGlobalEndBounds(child.clone())).toEqual(bounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getGlobalEndBounds(child.clone())).toEqual(newBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newBoundsStr, function(__done) {
                        child.update();
                        expect(child.getGlobalEndBounds(child.clone())).toEqual(newBounds);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newBoundsStr, function() {
                        expect(child.getGlobalEndBounds(child.clone())).toEqual(newBounds);
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedBoundsStr, function() {
                    expect(child.getGlobalEndBounds(genesis)).toEqual(nestedBounds);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getGlobalEndBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedEndBoundsStr, function(__done) {
                        child.update();
                        expect(child.getGlobalEndBounds(genesis)).toEqual(nestedEndBounds);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedEndBoundsStr, function() {
                        expect(child.getGlobalEndBounds(genesis)).toEqual(nestedEndBounds);
                    });
                });
            });
        });
        //getGlobalEndData
        describe('When using .getGlobalEndData()', function() {
            beforeEach(orphantSetup.make);

            it('Should be defined', function() {
                expect(child.getGlobalEndData).toBeDefined();
            });


            describe('While orphant', function() {
                beforeEach(orphantSetup.make);

                it('Should return original data:' + dataStr, function() {
                    expect(child.getGlobalEndData(child.clone())).toEqual(data);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(orphantSetup.newBypass);

                    it('Should return new data :' + newDataStr, function() {
                        expect(child.getGlobalEndData(child.clone())).toEqual(newData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(orphantSetup.newEnded);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getGlobalEndData(child.clone())).toEqual(newData);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(orphantSetup.new);

                    it('Should return new data:' + newDataStr, function(__done) {
                        child.update();
                        expect(child.getGlobalEndData(child.clone())).toEqual(newData);
                        __done();
                    });
                });
            });

            describe('While nested', function() {
                beforeEach(nestedSetup.make);

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getGlobalEndData(genesis)).toEqual(nestedData);
                });

                it('Should return original data:' + nestedDataStr, function() {
                    expect(child.getGlobalEndData(genesis)).toEqual(nestedData);
                });

                it('Should return global position if parent is not part of lineage :' + nestedDataStr, function() {
                    expect(child.getGlobalEndData(child.clone())).toEqual(nestedData);
                });

                describe('When new values previously set bypassing curves', function() {
                    beforeEach(nestedSetup.newBypass);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getGlobalEndData(genesis)).toEqual(nestedNewData);
                    });
                });

                describe('When new values previously set at the end of curves', function() {
                    beforeEach(nestedSetup.newEnded);

                    it('Should return new data:' + nestedNewDataStr, function(__done) {
                        child.update();
                        expect(child.getGlobalEndData(genesis)).toEqual(nestedNewData);
                        __done();
                    });
                });

                describe('When new values previously set at the start of curves', function() {
                    beforeEach(nestedSetup.new);

                    it('Should return new data:' + nestedNewDataStr, function() {
                        expect(child.getGlobalEndData(genesis)).toEqual(nestedNewData);
                    });
                });
            });
        });
    });
});

