/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/position2d'], function($numerus, $curves, $position2d) {
    'use strict';

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 111;
    var OriginY = 333;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var translateOriginX = 60;
    var translateOriginY = 60;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var translateX = 50;
    var translateY = 50;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);
    var instance;



    describe('When using $position2d APIs with curving values', function() {
        describe('using setters and retrieving values on curve end', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end value of X (' + (X - newOriginX) + ') while calling .getLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual(X - newOriginX);
                __done();
            });

            it('Should return end value of Y (' + (Y - newOriginY) + ') while calling .getLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual(Y - newOriginY);
                __done();
            });

            it('Should return end values of coord object {left:' + (X - newOriginX)  + ', top:' + (Y - newOriginY) + '} while calling  getOrigin()', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: (X - newOriginX),
                    top: (Y - newOriginY)
                });
                __done();
            });
        });

        describe('using setters and retrieving values in a mid of curve', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not return end value of X (' + (X - newOriginX) + ') while calling .getLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).not.toEqual((X - newOriginX));
                __done();
            });

            it('Should not return end value of Y (' + (Y - newOriginY) + ') while calling .getLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).not.toEqual((Y - newOriginY));
                __done();
            });

            it('Should not return end values of coord object {left:' + (X - newOriginX) + ', top:' + (Y - newOriginY) + '} while calling  getPosition()', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).not.toEqual({
                    left: (X - newOriginX),
                    top: (Y - newOriginY)
                });
                __done();
            });
        });

        describe('using max*() setters', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.maxOrigin();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return max value of X (' + (X - maxOriginX) + ') after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual(X - maxOriginX);
                __done();
            });

            it('Should return max value of Y (' + (Y - maxOriginY) + ') after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual(Y - maxOriginY);
                __done();
            });

            it('Should return max values coord object {left:' + (X - maxOriginX) + ', top:' + (Y - maxOriginY) + '} while calling  getPosition() after  after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: X - maxOriginX,
                    top: Y - maxOriginY
                });
                __done();
            });
        });

        describe('using min*() setters', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.minOrigin();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return min value of X (' + (X - minOriginX) + ') after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual((X - minOriginX));
                __done();
            });

            it('Should return min value of Y (' + (Y - minOriginY) + ') after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual((Y - minOriginY));
                __done();
            });

            it('Should return min values coord object {left:' + (X - minOriginX) + ', top:' + (Y - minOriginY) + '} while calling  getPosition() after  after curve duration is expired', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: (X - minOriginX),
                    top: (Y - minOriginY)
                });
                __done();
            });
        });

        describe('using reset*() get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setOrigin(newOriginX, newOriginY, true).resetOrigin();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should set X to original value (' + (X - OriginX)  + ')', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual((X - OriginX));
                __done();
            });

            it('Should set Y to original value (' + (Y - OriginY) + ')', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual((Y - OriginY));
                __done();
            });

            it('Should set original values coord object {left:' + (X - OriginX) + ', top:' + (Y - OriginY) + '}', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: (X - OriginX),
                    top: (Y - OriginY)
                });
                __done();
            });
        });


        describe('using reset*() get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setOrigin(newOriginX, newOriginY, true).resetOrigin();
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not have X to equal to original value (' + (X - OriginX) + ')', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).not.toEqual(X - OriginX);
                __done();
            });

            it('Should not have Y to original value (' + (Y - OriginY) + ')', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).not.toEqual(Y - OriginY);
                __done();
            });

            it('Should not have original values coord object {left:' +  (X - OriginX) + ', top:' + (Y - OriginY) + '}', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).not.toEqual({
                    left: X - OriginX,
                    top: Y - OriginY
                });
                __done();
            });
        });

        describe('using translate*() - get values after curve had expired', function() {
            var _resultX = X - (OriginX + translateOriginX);
            var _resultY = Y - (OriginY + translateOriginY);

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.translateOrigin(translateOriginX, translateOriginY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return translated X value (' + _resultX + ') when calling getX()', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return translated Y value (' + _resultY + ') when calling getY()', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual(_resultY);
                __done();
            });

            it('Should return translated values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });


        describe('using translate*() - get values in the mid of curve', function() {

            var _resultX = X - (OriginX + translateOriginX);
            var _resultY = Y - (OriginY + translateOriginY);

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.translateOrigin(translateX, translateY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not have end translated X value (' + _resultX + ') when calling getX()', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).not.toEqual(_resultX);
                __done();
            });

            it('Should not have end translated Y value (' + (_resultY) + ') when calling getY()', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).not.toEqual(_resultY);
                __done();
            });

            it('Should not have end translated values coord object {left:' + (_resultX) + ', top:' + (_resultY) + '} while calling  getPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).not.toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });


        describe('using getEnd*() - get values after curve had expired', function() {
            var _resultX = newX - newOriginX;
            var _resultY = newY - newOriginY;

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setCoord(newX, newY).setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end X value (' + _resultX + ') when calling getEndLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getEndLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return end Y value (' + _resultY + ') when calling getEndTop()', function(__done) {
                instance.updatePosition();
                expect(instance.getEndTop()).toEqual(_resultY);
                __done();
            });

            it('Should return end values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getEndPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getEndPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });



        describe('using getEnd*() - get values in the mid of curve', function() {

            var _resultX = newX - newOriginX;
            var _resultY = newY - newOriginY;

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setCoord(newX, newY).setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should return end X value (' + _resultX + ') when calling getEndLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getEndLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return end Y value (' + _resultY + ') when calling getEndTop()', function(__done) {
                instance.updatePosition();
                expect(instance.getEndTop()).toEqual(_resultY);
                __done();
            });

            it('Should return end values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getEndPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getEndPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });

        describe('using getStart*() - get values after curve had expired', function() {

            var _resultX = X - OriginX;
            var _resultY = Y - OriginY;

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setCoord(newX, newY).setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return original X value (' + _resultX + ') when calling getStartLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getStartLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return original Y value (' + _resultY + ') when calling getStartTop()', function(__done) {
                instance.updatePosition();
                expect(instance.getStartTop()).toEqual(_resultY);
                __done();
            });

            it('Should return original values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getStartPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getStartPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });


        describe('using getStart*() - get values in the mid of curve', function() {

            var _resultX = X - OriginX;
            var _resultY = Y - OriginY;

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setCoord(newX, newY).setOrigin(newOriginX, newOriginY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should return original X value (' + _resultX + ') when calling getStartLeft()', function(__done) {
                instance.updatePosition();
                expect(instance.getStartLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return original Y value (' + _resultY + ') when calling getStartTop()', function(__done) {
                instance.updatePosition();
                expect(instance.getStartTop()).toEqual(_resultY);
                __done();
            });

            it('Should return original values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getStartPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getStartPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });

        describe('using end*() - get values after curve had expired', function() {
            var _resultX = newX - newOriginX;
            var _resultY = newY - newOriginY;

            beforeEach(function(__done) {
                instance = $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
                instance.setCoord(newX, newY).setOrigin(newOriginX, newOriginY).endOrigin();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end X value (' + _resultX + ') when calling getX()', function(__done) {
                instance.updatePosition();
                expect(instance.getLeft()).toEqual(_resultX);
                __done();
            });

            it('Should return end Y value (' + _resultY + ') when calling getY()', function(__done) {
                instance.updatePosition();
                expect(instance.getTop()).toEqual(_resultY);
                __done();
            });

            it('Should return end values coord object {left:' + _resultX + ', top:' + _resultY + '} while calling  getPosition() ', function(__done) {
                instance.updatePosition();
                expect(instance.getPosition()).toEqual({
                    left: _resultX,
                    top: _resultY
                });
                __done();
            });
        });
    });
});
