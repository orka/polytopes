/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/position2d'], function($numerus, $curves, $position2d) {
    'use strict';

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    var maxOriginX = 1000;
    var minOriginX = -1000;
    var maxOriginY = 500;
    var minOriginY = -500;
    var OriginX = 222;
    var OriginY = 444;
    var newOriginX = maxOriginX / 2;
    var newOriginY = maxOriginY / 2;
    var numOriginX = $numerus.number(OriginX, minOriginX, maxOriginX, curve);
    var numOriginY = $numerus.number(OriginY, minOriginY, maxOriginY, curve);

    //
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);
    var instance;

    var result = {
        position: {
            left: X - OriginX,
            top: Y - OriginY
        },
        coord: {
            x: X,
            Y: Y
        },
        origin: {
            x: OriginX,
            y: OriginY
        }
    };

    var newResult = {
        position: {
            left: newX - newOriginX,
            top: newY - newOriginY
        },
        coord: {
            x: newX,
            Y: newY
        },
        origin: {
            x: newOriginX,
            y: newOriginY
        }
    };

    var resultMax = {
        position: {
            left: maxX - maxOriginX,
            top: maxY - maxOriginY
        },
        coord: {
            x: maxX,
            Y: maxY
        },
        origin: {
            x: maxOriginX,
            y: maxOriginY
        }
    };

    var resultMin = {
        position: {
            left: minX - minOriginX,
            top: minY - minOriginY
        },
        coord: {
            x: minX,
            Y: minY
        },
        origin: {
            x: minOriginX,
            y: minOriginY
        }
    };

    var setup = {
        make: function() {
            instance = make();
        },
        newBypass: function() {
            setup.make();
            setNewValues(instance, true);
        },
        newEnded: function(__done) {
            setup.make();
            setNewValues(instance);
            setTimeout(__done, curveDuration);
        }
    };


    function setNewValues(__child, __bypass) {
        return __child.setCoord(newX, newY, __bypass).setOrigin(newOriginX, newOriginY, __bypass);
    }

    function make() {
        return $position2d(numX.clone(), numY.clone(), numOriginX.clone(), numOriginY.clone());
    }

    describe('When using $position2d APIs getters', function() {

        describe('When using origin getters', function() {
            //testing getOriginX()
            describe('When using getOriginX()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginX).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginX.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.x, function() {
                    expect(instance.getOriginX()).toEqual(result.origin.x);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.x, function() {
                        expect(instance.getOriginX()).toEqual(newResult.origin.x);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin.x, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginX()).toEqual(newResult.origin.x);
                        __done();
                    });
                });
            });

            //testing getOriginY()
            describe('When using getOriginY()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginY).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginY.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.y, function() {
                    expect(instance.getOriginY()).toEqual(result.origin.y);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.y, function() {
                        expect(instance.getOriginY()).toEqual(newResult.origin.y);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin.y, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginY()).toEqual(newResult.origin.y);
                        __done();
                    });
                });
            });

            //testing getOrigin()
            describe('When using getOrigin()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOrigin).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOrigin.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin, function() {
                    expect(instance.getOrigin()).toEqual(result.origin);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin, function() {
                        expect(instance.getOrigin()).toEqual(newResult.origin);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOrigin()).toEqual(newResult.origin);
                        __done();
                    });
                });
            });
        });

        describe('When using origin end getters', function() {
            //testing getOriginEndX()
            describe('When using getOriginEndX()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginEndX).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginEndX.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.x, function() {
                    expect(instance.getOriginEndX()).toEqual(result.origin.x);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.x, function() {
                        expect(instance.getOriginEndX()).toEqual(newResult.origin.x);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin.x, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginEndX()).toEqual(newResult.origin.x);
                        __done();
                    });
                });
            });

            //testing getOriginEndY()
            describe('When using getOriginEndY()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginEndY).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginEndY.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.y, function() {
                    expect(instance.getOriginEndY()).toEqual(result.origin.y);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.y, function() {
                        expect(instance.getOriginEndY()).toEqual(newResult.origin.y);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin.y, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginEndY()).toEqual(newResult.origin.y);
                        __done();
                    });
                });
            });

            //testing getOriginEnd()
            describe('When using getOriginEnd()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginEnd).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginEnd.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin, function() {
                    expect(instance.getOriginEnd()).toEqual(result.origin);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin, function() {
                        expect(instance.getOriginEnd()).toEqual(newResult.origin);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.origin, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginEnd()).toEqual(newResult.origin);
                        __done();
                    });
                });
            });
        });

        describe('When using origin start getters', function() {
            //testing getOriginStartX()
            describe('When using getOriginStartX()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginStartX).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginStartX.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.x, function() {
                    expect(instance.getOriginStartX()).toEqual(result.origin.x);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.x, function() {
                        expect(instance.getOriginStartX()).toEqual(newResult.origin.x);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + result.origin.x, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginStartX()).toEqual(result.origin.x);
                        __done();
                    });
                });
            });

            //testing getOriginStartY()
            describe('When using getOriginStartY()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginStartY).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginStartY.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin.y, function() {
                    expect(instance.getOriginStartY()).toEqual(result.origin.y);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin.y, function() {
                        expect(instance.getOriginStartY()).toEqual(newResult.origin.y);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + result.origin.y, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginStartY()).toEqual(result.origin.y);
                        __done();
                    });
                });
            });

            //testing getOriginStart()
            describe('When using getOriginStart()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getOriginStart).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getOriginStart.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.origin, function() {
                    expect(instance.getOriginStart()).toEqual(result.origin);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.origin, function() {
                        expect(instance.getOriginStart()).toEqual(newResult.origin);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + result.origin, function(__done) {
                        instance.updatePosition();
                        expect(instance.getOriginStart()).toEqual(result.origin);
                        __done();
                    });
                });
            });
        });

        describe('When using position getters', function() {
            //testing getLeft()
            describe('When using getLeft()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getLeft).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getLeft.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.left, function() {
                    expect(instance.getLeft()).toEqual(result.position.left);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.left, function() {
                        expect(instance.getLeft()).toEqual(newResult.position.left);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position.left, function(__done) {
                        instance.updatePosition();
                        expect(instance.getLeft()).toEqual(newResult.position.left);
                        __done();
                    });
                });
            });

            //testing getTop()
            describe('When using getTop()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getTop).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getTop.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.top, function() {
                    expect(instance.getTop()).toEqual(result.position.top);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.top, function() {
                        expect(instance.getTop()).toEqual(newResult.position.top);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position.top, function(__done) {
                        instance.updatePosition();
                        expect(instance.getTop()).toEqual(newResult.position.top);
                        __done();
                    });
                });
            });

            //testing getPosition()
            describe('When using getPosition()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getPosition).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getPosition.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position, function() {
                    expect(instance.getPosition()).toEqual(result.position);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position, function() {
                        expect(instance.getPosition()).toEqual(newResult.position);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position, function(__done) {
                        instance.updatePosition();
                        expect(instance.getPosition()).toEqual(newResult.position);
                        __done();
                    });
                });
            });
        });

        describe('When using position end getters', function() {
            //testing getEndLeft()
            describe('When using getEndLeft()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getEndLeft).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getEndLeft.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.left, function() {
                    expect(instance.getEndLeft()).toEqual(result.position.left);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.left, function() {
                        expect(instance.getEndLeft()).toEqual(newResult.position.left);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position.left, function(__done) {
                        instance.updatePosition();
                        expect(instance.getEndLeft()).toEqual(newResult.position.left);
                        __done();
                    });
                });
            });

            //testing getEndTop()
            describe('When using getEndTop()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getEndTop).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getEndTop.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.top, function() {
                    expect(instance.getEndTop()).toEqual(result.position.top);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.top, function() {
                        expect(instance.getEndTop()).toEqual(newResult.position.top);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position.top, function(__done) {
                        instance.updatePosition();
                        expect(instance.getEndTop()).toEqual(newResult.position.top);
                        __done();
                    });
                });
            });

            //testing getEndPosition()
            describe('When using getEndPosition()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getEndPosition).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getEndPosition.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position, function() {
                    expect(instance.getEndPosition()).toEqual(result.position);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position, function() {
                        expect(instance.getEndPosition()).toEqual(newResult.position);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newEnded);

                    it('Should return position:' + newResult.position, function(__done) {
                        instance.updatePosition();
                        expect(instance.getEndPosition()).toEqual(newResult.position);
                        __done();
                    });
                });
            });
        });

        describe('When using position start getters', function() {
            //testing getStartLeft()
            describe('When using getStartLeft()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getStartLeft).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getStartLeft.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.left, function() {
                    expect(instance.getStartLeft()).toEqual(result.position.left);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.left, function() {
                        expect(instance.getStartLeft()).toEqual(newResult.position.left);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newStarted);

                    it('Should return position:' + newResult.position.left, function(__done) {
                        instance.updatePosition();
                        expect(instance.getStartLeft()).toEqual(newResult.position.left);
                        __done();
                    });
                });
            });

            //testing getStartTop()
            describe('When using getStartTop()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getStartTop).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getStartTop.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position.top, function() {
                    expect(instance.getStartTop()).toEqual(result.position.top);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position.top, function() {
                        expect(instance.getStartTop()).toEqual(newResult.position.top);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newStarted);

                    it('Should return position:' + newResult.position.top, function(__done) {
                        instance.updatePosition();
                        expect(instance.getStartTop()).toEqual(newResult.position.top);
                        __done();
                    });
                });
            });

            //testing getStartPosition()
            describe('When using getStartPosition()', function() {
                beforeEach(setup.make);

                it('Should be defined', function() {
                    expect(instance.getStartPosition).toBeDefined();
                });

                it('Should not throw', function() {
                    expect(instance.getStartPosition.bind(instance)).not.toThrow();
                });

                it('Should return position:' + result.position, function() {
                    expect(instance.getStartPosition()).toEqual(result.position);
                });

                describe('When setting new values bypassing curves', function() {
                    beforeEach(setup.newBypass);

                    it('Should return position:' + newResult.position, function() {
                        expect(instance.getStartPosition()).toEqual(newResult.position);
                    });
                });

                describe('When getting new values after curve ended', function() {
                    beforeEach(setup.newStarted);

                    it('Should return position:' + newResult.position, function(__done) {
                        instance.updatePosition();
                        expect(instance.getStartPosition()).toEqual(newResult.position);
                        __done();
                    });
                });
            });
        });
    });


});
