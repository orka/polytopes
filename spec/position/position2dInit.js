/*
global describe,
global it,
global expect,
 */
define(['p!-numerus', 'lib/position2d'], function($numerus, $position2d) {
    'use strict';



    describe('When instantiating 2D Size object', function() {

        it('$position2d Class should exist', function() {
            expect($position2d).toBeDefined();
        });

        describe('When using constructor method $position2d(Numerus, Numerus)', function() {
            function create() {
                return $position2d.bind(null, $numerus.number(0), $numerus.number(0), arguments[0], arguments[1]);
            }

            it('Should not throw if no origin arguments were supplied', function() {
                expect(create()).not.toThrow();
            });

            it('Should throw if numbers supplied', function() {
                expect(create(1, 2)).toThrow();
                expect(create(1.1, 2.2)).toThrow();
                expect(create(Infinity)).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when only one Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should not throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).not.toThrow();
            });

            it('Should have X, Y origin values equal to arguments supplied', function() {
                var _instance = create($numerus.number(1), $numerus.number(2))();
                expect(_instance.getOriginX()).toEqual(1);
                expect(_instance.getOriginY()).toEqual(2);
            });
        });

        describe('When using constructor method $position2d.fromCoord(#,#)', function() {

            function create() {
                return $position2d.fromCoord.bind(null, 0, 0, arguments[0], arguments[1]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).toThrow();
            });

            it('Should not throw if numbers supplied', function() {
                expect(create(1, 2)).not.toThrow();
                expect(create(1.1, 2.2)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(1, 2)();
                expect(_instance.getOriginX()).toEqual(1);
                expect(_instance.getOriginY()).toEqual(2);
            });
        });

        describe('When using constructor method $position2d.fromBounds({})', function() {
            var _bounds = {
                x: 0,
                y: 0,
                originX: 0,
                originY: 0
            };

            function create(__bounds) {
                return $position2d.fromBounds.bind(null, __bounds);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create([])).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_bounds)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_bounds)();
                expect(_instance.getOriginX()).toEqual(_bounds.originX);
                expect(_instance.getOriginY()).toEqual(_bounds.originY);
            });
        });

        describe('When using constructor method $position2d.fromArray([])', function() {
            var _array = [1, 2, 3, 4];

            function create(__array) {
                return $position2d.fromArray.bind(null, __array);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create({})).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_array)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_array)();
                expect(_instance.getOriginX()).toEqual(_array[2]);
                expect(_instance.getOriginY()).toEqual(_array[3]);
            });
        });

        describe('When using deconstructor() method', function() {
            var instance;

            beforeEach(function() {
                instance = $position2d($numerus.number(0), $numerus.number(0), $numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.deconstructor).toBeDefined();
            });

            it('Should derefference .originX, .originY', function() {
                instance.deconstructor();
                expect(instance.originX).toEqual(null);
                expect(instance.originY).toEqual(null);
            });
        });

        describe('When using clone() method', function() {
            var instance;

            beforeEach(function() {
                instance = $position2d($numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.clone).toBeDefined();
            });

            it('Should create new instance with original properies of Size', function() {
                var _new = instance.clone();
                expect(_new === instance).toEqual(false);
                expect(_new.originY === instance.originY).toEqual(true);
                expect(_new.originX === instance.originX).toEqual(true);
                expect(_new.__originYNumerus === instance.__originYNumerus).toEqual(false);
                expect(_new.__originXNumerus === instance.__originXNumerus).toEqual(false);
                expect($position2d.isOffspring(_new)).toEqual(true);
            });
        });
    });
});
