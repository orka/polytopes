/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/size2d'], function($numerus, $curves, $size2d) {
    'use strict';

    var curveDuration = 50;
    var curve = $curves.bezier(null, curveDuration);
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var addWidth = 50;
    var addHeight = 50;
    var Width = 222;
    var Height = 444;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);
    var instance;



    describe('When using $size2d APIs with curving values', function() {
        describe('When using setters and retrieving values on curve end', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return end value of Width (' + newWidth + ') while calling .getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(newWidth);
                __done();
            });

            it('Should return end value of Height (' + newHeight + ') while calling .getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(newHeight);
                __done();
            });

            it('Should return end values of coord object {width:' + newWidth + ', height:' + newHeight + '} while calling  getSize()', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
                __done();
            });
        });

        describe('When using setters and retrieving values in a mid of curve', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration / 2);
            });

            //Width|Height set->get tests
            it('Should not return end value of Width (' + newWidth + ') while calling .getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).not.toEqual(newWidth);
                __done();
            });

            it('Should not return end value of Height (' + newHeight + ') while calling .getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).not.toEqual(newHeight);
                __done();
            });

            it('Should not return end values of coord object {width:' + newWidth + ', height:' + newHeight + '} while calling  getSize()', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).not.toEqual({
                    width: newWidth,
                    height: newHeight
                });
                __done();
            });
        });

        describe('When using max*() setters', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.maxSize();
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return max value of Width (' + maxWidth + ') after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(maxWidth);
                __done();
            });

            it('Should return max value of Height (' + maxHeight + ') after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(maxHeight);
                __done();
            });

            it('Should return max values coord object {width:' + maxWidth + ', height:' + maxHeight + '} while calling  getSize() after  after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: maxWidth,
                    height: maxHeight
                });
                __done();
            });
        });

        describe('When using min*() setters', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.minSize();
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return min value of Width (' + minWidth + ') after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(minWidth);
                __done();
            });

            it('Should return min value of Height (' + minHeight + ') after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(minHeight);
                __done();
            });

            it('Should return min values coord object {width:' + minWidth + ', height:' + minHeight + '} while calling  getSize() after  after curve duration is expired', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: minWidth,
                    height: minHeight
                });
                __done();
            });
        });

        describe('When using reset*() get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight, true).resetSize();
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should set Width to original value (' + Width + ')', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(Width);
                __done();
            });

            it('Should set Height to original value (' + Height + ')', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(Height);
                __done();
            });

            it('Should set original values coord object {width:' + Width + ', height:' + Height + '}', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: Width,
                    height: Height
                });
                __done();
            });
        });

        describe('When using reset*() get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight, true).resetSize();
                setTimeout(__done, curveDuration / 2);
            });

            //Width|Height set->get tests
            it('Should not have Width to equal to original value (' + Width + ')', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).not.toEqual(Width);
                __done();
            });

            it('Should not have Height to original value (' + Height + ')', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).not.toEqual(Height);
                __done();
            });

            it('Should not have original values coord object {width:' + Width + ', height:' + Height + '}', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).not.toEqual({
                    width: Width,
                    height: Height
                });
                __done();
            });
        });


        describe('When using add*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.addSize(addWidth, addHeight);
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return addd Width value (' + (Width + addWidth) + ') when calling getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(Width + addWidth);
                __done();
            });

            it('Should return addd Height value (' + (Height + addHeight) + ') when calling getHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(Height + addHeight);
                __done();
            });

            it('Should return addd values coord object {width:' + (Width + addWidth) + ', height:' + (Height + addHeight) + '} while calling  getSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: Width + addWidth,
                    height: Height + addHeight
                });
                __done();
            });
        });

        describe('When using add*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.addSize(addWidth, addHeight);
                setTimeout(__done, curveDuration / 2);
            });

            //Width|Height set->get tests
            it('Should not have end addd Width value (' + (Width + addWidth) + ') when calling getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).not.toEqual(Width + addWidth);
                __done();
            });

            it('Should not have end addd Height value (' + (Height + addHeight) + ') when calling getHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).not.toEqual(Height + addHeight);
                __done();
            });

            it('Should not have end addd values coord object {width:' + (Width + addWidth) + ', height:' + (Height + addHeight) + '} while calling  getSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).not.toEqual({
                    width: Width + addWidth,
                    height: Height + addHeight
                });
                __done();
            });
        });

        describe('When using getEnd*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return end Width value (' + newWidth + ') when calling getEndWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getEndWidth()).toEqual(newWidth);
                __done();
            });

            it('Should return end Height value (' + newHeight + ') when calling getEndHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getEndHeight()).toEqual(newHeight);
                __done();
            });

            it('Should return end values coord object {width:' + newWidth + ', height:' + newHeight + '} while calling  getEndSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getEndSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
                __done();
            });
        });

        describe('When using getEnd*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration / 2);
            });

            //Width|Height set->get tests
            it('Should return end Width value (' + newWidth + ') when calling getEndWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getEndWidth()).toEqual(newWidth);
                __done();
            });

            it('Should return end Height value (' + newHeight + ') when calling getEndHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getEndHeight()).toEqual(newHeight);
                __done();
            });

            it('Should return end values coord object {width:' + newWidth + ', height:' + newHeight + '} while calling  getEndSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getEndSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
                __done();
            });
        });


        describe('When using getStart*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return original Width value (' + Width + ') when calling getStartWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getStartWidth()).toEqual(Width);
                __done();
            });

            it('Should return original Height value (' + Height + ') when calling getStartHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getStartHeight()).toEqual(Height);
                __done();
            });

            it('Should return original values coord object {width:' + Width + ', height:' + Height + '} while calling  getStartSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getStartSize()).toEqual({
                    width: Width,
                    height: Height
                });
                __done();
            });
        });

        describe('When using getStart*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight);
                setTimeout(__done, curveDuration / 2);
            });

            //Width|Height set->get tests
            it('Should return original Width value (' + Width + ') when calling getStartWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getStartWidth()).toEqual(Width);
                __done();
            });

            it('Should return original Height value (' + Height + ') when calling getStartHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getStartHeight()).toEqual(Height);
                __done();
            });

            it('Should return original values coord object {width:' + Width + ', height:' + Height + '} while calling  getStartSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getStartSize()).toEqual({
                    width: Width,
                    height: Height
                });
                __done();
            });
        });

        describe('When using end*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $size2d(numWidth.clone(), numHeight.clone());
                instance.setSize(newWidth, newHeight).endSize();
                setTimeout(__done, curveDuration);
            });

            //Width|Height set->get tests
            it('Should return end Width value (' + newWidth + ') when calling getWidth()', function(__done) {
                instance.updateSize();
                expect(instance.getWidth()).toEqual(newWidth);
                __done();
            });

            it('Should return end Height value (' + newHeight + ') when calling getHeight()', function(__done) {
                instance.updateSize();
                expect(instance.getHeight()).toEqual(newHeight);
                __done();
            });

            it('Should return end values coord object {width:' + newWidth + ', height:' + newHeight + '} while calling  getSize() ', function(__done) {
                instance.updateSize();
                expect(instance.getSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
                __done();
            });
        });
    });
});
