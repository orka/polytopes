/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/size2d'], function($numerus, $curves, $size2d) {
    'use strict';

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    var maxWidth = 1000;
    var minWidth = -1000;
    var maxHeight = 500;
    var minHeight = -500;
    var Width = 222;
    var Height = 444;
    var newWidth = maxWidth / 2;
    var newHeight = maxHeight / 2;
    var numWidth = $numerus.number(Width, minWidth, maxWidth, curve);
    var numHeight = $numerus.number(Height, minHeight, maxHeight, curve);
    var instance;



    describe('When using $size2d APIs (with curve argument) bypassing curves', function() {
        function beforeStub() {
            instance = $size2d(numWidth.clone(), numHeight.clone());
        }

        describe('When using main getters', function() {
            beforeEach(beforeStub);

            //Width|Height get tests
            it('Has to have getWidth() defined', function() {
                expect(instance.getWidth).toBeDefined();
            });

            it('Has to have getHeight() defined', function() {
                expect(instance.getHeight).toBeDefined();
            });

            it('Has to have getSize() defined', function() {
                expect(instance.getSize).toBeDefined();
            });
            //usage
            it('Should return Width: ' + Width + ' when calling .getWidth()', function() {
                expect(instance.getWidth()).toEqual(Width);
            });

            it('Should return Height: ' + Height + ' when calling .getHeight()', function() {
                expect(instance.getHeight()).toEqual(Height);
            });

            it('Should return Object: {width:' + Width + ', height:' + Height + '} when calling .getSize()', function() {
                expect(instance.getSize()).toEqual({
                    width: Width,
                    height: Height
                });
            });
        });

        describe('When using main setters', function() {
            beforeEach(beforeStub);
            //Width|Height set APIs
            it('Has to have setWidth() defined', function() {
                expect(instance.setWidth).toBeDefined();
            });

            it('Has to have setHeight() defined', function() {
                expect(instance.setHeight).toBeDefined();
            });

            it('Has to have setSize() defined', function() {
                expect(instance.setSize).toBeDefined();
            });

            //Width|Height set tests
            it('Should return instance when calling .setWidth()', function() {
                expect(instance.setWidth(newWidth)).toEqual(instance);
            });

            it('Should return instance when calling .setHeight()', function() {
                expect(instance.setHeight(newHeight)).toEqual(instance);
            });

            it('Should return instance when cvalling .setSize()', function() {
                expect(instance.setSize(Width, Height)).toEqual(instance);
            });

            //Width|Height set->get tests
            it('Should return new value instance when calling .getWidth() after .setWidth(' + newWidth + ', true) is called', function() {
                expect(instance.getWidth()).toEqual(Width);
                expect(instance.setWidth(newWidth, true).getWidth()).toEqual(newWidth);
            });

            it('Should return new value instance when calling .getHeight() after .setHeight(' + newHeight + ', true) is called', function() {
                expect(instance.getHeight()).toEqual(Height);
                expect(instance.setHeight(newHeight, true).getHeight()).toEqual(newHeight);
            });

            it('Should return new coord object when calling .getSize() after .setSize(' + newWidth + ',' + newHeight + ', true) is called', function() {
                expect(instance.getSize()).toEqual({
                    width: Width,
                    height: Height
                });
                expect(instance.setSize(newWidth, newHeight, true).getSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
            });

            //Width|Height set->get tests while curving (false tests)
            it('Should not return new value instance when calling .getWidth() after .setWidth(' + newWidth + ') is called', function() {
                expect(instance.getWidth()).toEqual(Width);
                expect(instance.setWidth(newWidth).getWidth()).not.toEqual(newWidth);
            });

            it('Should not return new value instance when calling .getHeight() after .setHeight(' + newHeight + ') is called', function() {
                expect(instance.getHeight()).toEqual(Height);
                expect(instance.setHeight(newHeight).getHeight()).not.toEqual(newHeight);
            });

            it('Should not return new coord object when calling .getSize() after .setSize(' + newWidth + ',' + newHeight + ') is called', function() {
                expect(instance.getSize()).toEqual({
                    width: Width,
                    height: Height
                });
                expect(instance.setSize(newWidth, newHeight).getSize()).not.toEqual({
                    width: newWidth,
                    height: newHeight
                });
            });


            //Width|Height set MAWidth cap tests
            it('Should return max capped value (width:' + maxWidth + ') when calling .setWidth(' + maxWidth * 2 + ', true) with exceeded max', function() {
                expect(instance.setWidth(maxWidth * 2, true).getWidth()).toEqual(maxWidth);
            });

            it('Should return max capped value (height:' + maxHeight + ') when calling .setHeight(' + maxHeight * 2 + ', true) with exceeded max', function() {
                expect(instance.setHeight(maxHeight * 2, true).getHeight()).toEqual(maxHeight);
            });

            it('Should return max capped values (height:' + maxHeight + ', width:' + maxWidth + ') when calling .setSize(' + maxWidth * 2 + ', ' + maxHeight * 2 + ', true) with exceeded max', function() {
                expect(instance.setSize(maxWidth * 2, maxHeight * 2, true).getSize()).toEqual({
                    width: maxWidth,
                    height: maxHeight
                });
            });

            //Width|Height set MAWidth cap tests (false tests)
            it('Should not return max capped value (width:' + maxWidth + ') when calling .setWidth(' + maxWidth * 2 + ') with exceeded max', function() {
                expect(instance.setWidth(maxWidth * 2).getWidth()).not.toEqual(maxWidth);
            });

            it('Should not return max capped value (height:' + maxHeight + ') when calling .setHeight(' + maxHeight * 2 + ') with exceeded max', function() {
                expect(instance.setHeight(maxHeight * 2).getHeight()).not.toEqual(maxHeight);
            });

            it('Should not return max capped values (height:' + maxHeight + ', width:' + maxWidth + ') when calling .setSize(' + maxWidth * 2 + ', ' + maxHeight * 2 + ') with exceeded max', function() {
                expect(instance.setSize(maxWidth * 2, maxHeight * 2).getSize()).not.toEqual({
                    width: maxWidth,
                    height: maxHeight
                });
            });

            //Width|Height set MIN cap tests
            it('Should return max capped value (width:' + minWidth + ') when calling .setWidth(' + minWidth * 2 + ', true) with exceeded max', function() {
                expect(instance.setWidth(minWidth * 2, true).getWidth()).toEqual(minWidth);
            });

            it('Should return max capped value (height:' + minHeight + ') when calling .setHeight(' + minHeight * 2 + ', true) with exceeded max', function() {
                expect(instance.setHeight(minHeight * 2, true).getHeight()).toEqual(minHeight);
            });

            it('Should return max capped values (height:' + minHeight + ', width:' + minWidth + ') when calling .setSize(' + minWidth * 2 + ', ' + minHeight * 2 + ', true) with exceeded max', function() {
                expect(instance.setSize(minWidth * 2, minHeight * 2, true).getSize()).toEqual({
                    width: minWidth,
                    height: minHeight
                });
            });

            //Width|Height set MIN cap tests (false tests)
            it('Should not return max capped value (width:' + minWidth + ') when calling .setWidth(' + minWidth * 2 + ') with exceeded max', function() {
                expect(instance.setWidth(minWidth * 2).getWidth()).not.toEqual(minWidth);
            });

            it('Should not return max capped value (height:' + minHeight + ') when calling .setHeight(' + minHeight * 2 + ') with exceeded max', function() {
                expect(instance.setHeight(minHeight * 2).getHeight()).not.toEqual(minHeight);
            });

            it('Should not return max capped values (height:' + minHeight + ', width:' + minWidth + ') when calling .setSize(' + minWidth * 2 + ', ' + minHeight * 2 + ') with exceeded max', function() {
                expect(instance.setSize(minWidth * 2, minHeight * 2).getSize()).not.toEqual({
                    width: minWidth,
                    height: minHeight
                });
            });
        });


        describe('When using MAWidth caps and snaps APIs', function() {
            beforeEach(beforeStub);

            it('Has to have maxWidth() defined', function() {
                expect(instance.maxWidth).toBeDefined();
            });

            it('Has to have maxHeight() defined', function() {
                expect(instance.maxHeight).toBeDefined();
            });

            it('Has to have maxSize() defined', function() {
                expect(instance.maxSize).toBeDefined();
            });

            //Width|Height max snap
            it('Should set Width value to max (' + maxWidth + ') when maxWidth(true) is called', function() {
                expect(instance.maxWidth(true).getWidth()).toEqual(maxWidth);
            });

            it('Should set Height value to max (' + maxHeight + ') when maxHeight(true) is called', function() {
                expect(instance.maxHeight(true).getHeight()).toEqual(maxHeight);
            });


            it('Should set coordinates to max (width:' + maxWidth + ', height:' + maxHeight + ') when maxSize(true) is called', function() {
                expect(instance.maxSize(true).getSize()).toEqual({
                    width: maxWidth,
                    height: maxHeight
                });
            });

             //Width|Height max snap
            it('Should not set Width value to max (' + maxWidth + ') when maxWidth() is called', function() {
                expect(instance.maxWidth().getWidth()).not.toEqual(maxWidth);
            });

            it('Should not set Height value to max (' + maxHeight + ') when maxHeight() is called', function() {
                expect(instance.maxHeight().getHeight()).not.toEqual(maxHeight);
            });


            it('Should not set coordinates to max (width:' + maxWidth + ', height:' + maxHeight + ') when maxSize() is called', function() {
                expect(instance.maxSize().getSize()).not.toEqual({
                    width: maxWidth,
                    height: maxHeight
                });
            });
        });


        describe('When using MIN caps and snaps APIs', function() {
            beforeEach(beforeStub);

            it('Has to have minWidth() defined', function() {
                expect(instance.minWidth).toBeDefined();
            });

            it('Has to have minHeight() defined', function() {
                expect(instance.minHeight).toBeDefined();
            });

            it('Has to have minSize() defined', function() {
                expect(instance.minSize).toBeDefined();
            });

            //Width|Height min snap
            it('Should set Width value to min (' + minWidth + ') when minWidth(true) is called', function() {
                expect(instance.minWidth(true).getWidth()).toEqual(minWidth);
            });

            it('Should set Height value to min (' + minHeight + ') when minHeight(true) is called', function() {
                expect(instance.minHeight(true).getHeight()).toEqual(minHeight);
            });

            it('Should set coordinates to min (width:' + minWidth + ', height:' + minHeight + ') when minSize(true) is called', function() {
                expect(instance.minSize(true).getSize()).toEqual({
                    width: minWidth,
                    height: minHeight
                });
            });

            //Width|Height min snap
            it('Should not set Width value to min (' + minWidth + ') when minWidth() is called', function() {
                expect(instance.minWidth().getWidth()).not.toEqual(minWidth);
            });

            it('Should not set Height value to min (' + minHeight + ') when minHeight() is called', function() {
                expect(instance.minHeight().getHeight()).not.toEqual(minHeight);
            });

            it('Should not set coordinates to min (width:' + minWidth + ', height:' + minHeight + ') when minSize() is called', function() {
                expect(instance.minSize().getSize()).not.toEqual({
                    width: minWidth,
                    height: minHeight
                });
            });
        });

        describe('When using Reset APIs', function() {
            beforeEach(beforeStub);

            it('Has to have resetWidth() defined', function() {
                expect(instance.resetWidth).toBeDefined();
            });

            it('Has to have resetHeight() defined', function() {
                expect(instance.resetHeight).toBeDefined();
            });

            it('Has to have resetSize() defined', function() {
                expect(instance.resetSize).toBeDefined();
            });

            //Width|Height reset tests
            it('Should return insatnce when .resetWidth() is called', function() {
                expect(instance.resetWidth()).toEqual(instance);
            });

            it('Should return insatnce when .resetHeight() is called', function() {
                expect(instance.resetHeight()).toEqual(instance);
            });

            it('Should return insatnce when .resetSize() is called', function() {
                expect(instance.resetSize()).toEqual(instance);
            });

            //Width|Height reset tests
            it('Should set Width value to original value of (' + Width + ') after .resetWidth(true) is called', function() {
                expect(instance.setWidth(newWidth, true).resetWidth(true).getWidth()).toEqual(Width);
            });

            it('Should set Height value to original value of (' + Height + ') after .resetHeight(true) is called', function() {
                expect(instance.setHeight(newHeight, true).resetHeight(true).getHeight()).toEqual(Height);
            });

            it('Should set coordinates values to original values of (width:' + Width + ', height:' + Height + ') after .resetSize(true) is called', function() {
                expect(instance.setSize(newWidth, newHeight, true).resetSize(true).getSize()).toEqual({
                    width: Width,
                    height: Height
                });
            });

            //Width|Height reset tests
            it('Should not set Width value to original value of (' + Width + ') after .resetWidth() is called', function() {
                expect(instance.setWidth(newWidth, true).resetWidth().getWidth()).not.toEqual(Width);
            });

            it('Should not set Height value to original value of (' + Height + ') after .resetHeight() is called', function() {
                expect(instance.setHeight(newHeight, true).resetHeight().getHeight()).not.toEqual(Height);
            });

            it('Should not set coordinates values to original values of (width:' + Width + ', height:' + Height + ') after .resetSize() is called', function() {
                expect(instance.setSize(newWidth, newHeight, true).resetSize().getSize()).not.toEqual({
                    width: Width,
                    height: Height
                });
            });
        });

        describe('When using Add APIs', function() {
            beforeEach(beforeStub);

            it('Has to have addWidth() defined', function() {
                expect(instance.addWidth).toBeDefined();
            });

            it('Has to have addHeight() defined', function() {
                expect(instance.addHeight).toBeDefined();
            });

            it('Has to have addSize() defined', function() {
                expect(instance.addSize).toBeDefined();
            });

            //Width|Height add tests
            it('Should set Width value to Width + newWidth when calling .addWidth(' + newWidth / 2 + ', true) is called', function() {
                expect(instance.setWidth(newWidth / 2, true).addWidth(newWidth, true).getWidth()).toEqual(newWidth * 1.5);
            });

            it('Should set Height value to Height + newHeight when calling .addHeight(' + newHeight / 2 + ', true) is called', function() {
                expect(instance.setHeight(newHeight / 2, true).addHeight(newHeight, true).getHeight()).toEqual(newHeight * 1.5);
            });

            it('Should set coordinates values to Width value to Width + newWidth and Height value to Height + newHeight when addSize(' + newWidth / 2 + ', ' + newHeight / 2 + ', true) is called', function() {
                expect(instance.setSize(newWidth / 2, newHeight / 2, true).addSize(newWidth, newHeight, true).getSize()).toEqual({
                    width: newWidth * 1.5,
                    height: newHeight * 1.5
                });
            });

             //Width|Height add tests (false tests)
            it('Should not set Width value to Width + newWidth when calling .addWidth(' + newWidth / 2 + ') is called', function() {
                expect(instance.setWidth(newWidth / 2, true).addWidth(newWidth).getWidth()).not.toEqual(newWidth * 1.5);
            });

            it('Should not set Height value to Height + newHeight when calling .addHeight(' + newHeight / 2 + ') is called', function() {
                expect(instance.setHeight(newHeight / 2, true).addHeight(newHeight).getHeight()).not.toEqual(newHeight * 1.5);
            });

            it('Should not set coordinates values to Width value to Width + newWidth and Height value to Height + newHeight when addSize(' + newWidth / 2 + ', ' + newHeight / 2 + ') is called', function() {
                expect(instance.setSize(newWidth / 2, newHeight / 2, true).addSize(newWidth, newHeight).getSize()).not.toEqual({
                    width: newWidth * 1.5,
                    height: newHeight * 1.5
                });
            });
        });

        describe('When using getEnd*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have getEndWidth() defined', function() {
                expect(instance.getEndWidth).toBeDefined();
            });

            it('Has to have getEndHeight() defined', function() {
                expect(instance.getEndHeight).toBeDefined();
            });

            it('Has to have getEndSize() defined', function() {
                expect(instance.getEndSize).toBeDefined();
            });

            //Width|Height end Curve tests
            it('Should return end value of Width curve when .getEndWidth() is called', function() {
                expect(instance.setWidth(newWidth).getEndWidth()).toEqual(newWidth);
            });

            it('Should return end value of Height curve when .getEndHeight() is called', function() {
                expect(instance.setHeight(newHeight).getEndHeight()).toEqual(newHeight);
            });

            it('Should return end value of Width and Height curves when .getEndSize() is called', function() {
                expect(instance.setSize(newWidth, newHeight).getEndSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
            });
        });

        describe('When using getStart*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have getStartWidth() defined', function() {
                expect(instance.getStartWidth).toBeDefined();
            });

            it('Has to have getStartHeight() defined', function() {
                expect(instance.getStartHeight).toBeDefined();
            });

            it('Has to have getStartSize() defined', function() {
                expect(instance.getStartSize).toBeDefined();
            });

            //Width|Height end Curve tests
            it('Should return start value of Width curve (' + Width + ') when .getStartWidth() is called', function() {
                expect(instance.setWidth(newWidth).getStartWidth()).toEqual(Width);
            });

            it('Should return start value of Height curve (' + Height + ') when .getStartHeight() is called', function() {
                expect(instance.setHeight(newHeight).getStartHeight()).toEqual(Height);
            });

            it('Should return start values of Width|Height curves (width:' + Width + ', height:' + Height + ') when .getStartSize() is called', function() {
                expect(instance.setSize(newWidth, newHeight).getStartSize()).toEqual({
                    width: Width,
                    height: Height
                });
            });
        });

        describe('When using end*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have endWidth() defined', function() {
                expect(instance.endWidth).toBeDefined();
            });

            it('Has to have endHeight() defined', function() {
                expect(instance.endHeight).toBeDefined();
            });

            it('Has to have endSize() defined', function() {
                expect(instance.endSize).toBeDefined();
            });

            //test returns
            it('Should return instance when .endWidth() is called', function() {
                expect(instance.endWidth()).toEqual(instance);
            });

            it('Should return instance when .endHeight() is called', function() {
                expect(instance.endHeight()).toEqual(instance);
            });

            it('Should return instance when .endSize() is called', function() {
                expect(instance.endSize()).toEqual(instance);
            });

            //test APIs
            it('Should return end value of Width curve when .getWidth() is called after .endWidth() is called', function() {
                expect(instance.setWidth(newWidth).endWidth().getWidth()).toEqual(newWidth);
            });

            it('Should return end value of Height curve when .getHeight() is called after .endHeight() is called', function() {
                expect(instance.setHeight(newHeight).endHeight().getHeight()).toEqual(newHeight);
            });

            it('Should return end values of Width,Height curve when .getSize() is called after .endSize() is called', function() {
                expect(instance.setSize(newWidth, newHeight).endSize().getSize()).toEqual({
                    width: newWidth,
                    height: newHeight
                });
            });
        });
    });
});
