/*
global describe,
global it,
global expect,
 */
define(['p!-numerus', 'lib/size2d'], function($numerus, $size2d) {
    'use strict';



    describe('When instantiating 2D Size object', function() {

        it('$size2d Class should exist', function() {
            expect($size2d).toBeDefined();
        });

        describe('When using constructor method $size2d(Numerus, Numerus)', function() {
            function create() {
                return $size2d.bind(null, arguments[0], arguments[1]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if numbers supplied', function() {
                expect(create(1, 2)).toThrow();
                expect(create(1.1, 2.2)).toThrow();
                expect(create(Infinity)).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when only one Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should not throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create($numerus.number(1), $numerus.number(2))();
                expect(_instance.getWidth()).toEqual(1);
                expect(_instance.getHeight()).toEqual(2);
            });
        });


        describe('When using constructor method $size2d.fromCoord(#,#)', function() {

            function create() {
                return $size2d.fromCoord.bind(null, arguments[0], arguments[1]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).toThrow();
            });

            it('Should not throw if numbers supplied', function() {
                expect(create(1, 2)).not.toThrow();
                expect(create(1.1, 2.2)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(1, 2)();
                expect(_instance.getWidth()).toEqual(1);
                expect(_instance.getHeight()).toEqual(2);
            });
        });


        describe('When using constructor method $size2d.fromBounds({})', function() {
            var _bounds = {
                width: 0,
                height: 0
            };

            function create(__bounds) {
                return $size2d.fromBounds.bind(null, __bounds);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create([])).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_bounds)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_bounds)();
                expect(_instance.getWidth()).toEqual(_bounds.width);
                expect(_instance.getHeight()).toEqual(_bounds.height);
            });
        });

        describe('When using constructor method $size2d.fromArray([])', function() {
            var _array = [1, 2];

            function create(__array) {
                return $size2d.fromArray.bind(null, __array);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create({})).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_array)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_array)();
                expect(_instance.getWidth()).toEqual(_array[0]);
                expect(_instance.getHeight()).toEqual(_array[1]);
            });
        });

        describe('When using deconstructor() method', function() {
            var instance;

            beforeEach(function() {
                instance = $size2d($numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.deconstructor).toBeDefined();
            });

            it('Should derefference .width, .height', function() {
                instance.deconstructor();
                expect(instance.width).toEqual(null);
                expect(instance.height).toEqual(null);
            });
        });


        describe('When using clone() method', function() {
            var instance;

            beforeEach(function() {
                instance = $size2d($numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.clone).toBeDefined();
            });

            it('Should create new instance with original properies of Size', function() {
                var _new = instance.clone();
                expect(_new === instance).toEqual(false);
                expect(_new.height === instance.height).toEqual(true);
                expect(_new.width === instance.width).toEqual(true);
                expect(_new.__heightNumerus === instance.__heightNumerus).toEqual(false);
                expect(_new.__widthNumerus === instance.__widthNumerus).toEqual(false);
                expect($size2d.isOffspring(_new)).toEqual(true);
            });
        });
    });
});
