/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/point2d'], function($numerus, $curves, $point2d) {
    'use strict';

    var curveDuration = 100;
    var curve = $curves.bezier(null, curveDuration);
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);
    var instance;



    describe('When using $point2d APIs (with curve argument) bypassing curves', function() {
        function beforeStub() {
            instance = $point2d(numX.clone(), numY.clone());
        }

        describe('When using main getters', function() {
            beforeEach(beforeStub);

            //X|Y get tests
            it('Has to have getX() defined', function() {
                expect(instance.getX).toBeDefined();
            });

            it('Has to have getY() defined', function() {
                expect(instance.getY).toBeDefined();
            });

            it('Has to have getCoord() defined', function() {
                expect(instance.getCoord).toBeDefined();
            });
            //usage
            it('Should return X: ' + X + ' when calling .getX()', function() {
                expect(instance.getX()).toEqual(X);
            });

            it('Should return Y: ' + Y + ' when calling .getY()', function() {
                expect(instance.getY()).toEqual(Y);
            });

            it('Should return Object: {x:' + X + ', y:' + Y + '} when calling .getCoord()', function() {
                expect(instance.getCoord()).toEqual({
                    x: X,
                    y: Y
                });
            });
        });

        describe('When using main setters', function() {
            beforeEach(beforeStub);
            //X|Y set APIs
            it('Has to have setX() defined', function() {
                expect(instance.setX).toBeDefined();
            });

            it('Has to have setY() defined', function() {
                expect(instance.setY).toBeDefined();
            });

            it('Has to have setCoord() defined', function() {
                expect(instance.setCoord).toBeDefined();
            });

            //X|Y set tests
            it('Should return instance when calling .setX()', function() {
                expect(instance.setX(newX)).toEqual(instance);
            });

            it('Should return instance when calling .setY()', function() {
                expect(instance.setY(newY)).toEqual(instance);
            });

            it('Should return instance when cvalling .setCoord()', function() {
                expect(instance.setCoord(X, Y)).toEqual(instance);
            });

            //X|Y set->get tests
            it('Should return new value instance when calling .getX() after .setX(' + newX + ', true) is called', function() {
                expect(instance.getX()).toEqual(X);
                expect(instance.setX(newX, true).getX()).toEqual(newX);
            });

            it('Should return new value instance when calling .getY() after .setY(' + newY + ', true) is called', function() {
                expect(instance.getY()).toEqual(Y);
                expect(instance.setY(newY, true).getY()).toEqual(newY);
            });

            it('Should return new coord object when calling .getCoord() after .setCoord(' + newX + ',' + newY + ', true) is called', function() {
                expect(instance.getCoord()).toEqual({
                    x: X,
                    y: Y
                });
                expect(instance.setCoord(newX, newY, true).getCoord()).toEqual({
                    x: newX,
                    y: newY
                });
            });

            //X|Y set->get tests while curving (false tests)
            it('Should not return new value instance when calling .getX() after .setX(' + newX + ') is called', function() {
                expect(instance.getX()).toEqual(X);
                expect(instance.setX(newX).getX()).not.toEqual(newX);
            });

            it('Should not return new value instance when calling .getY() after .setY(' + newY + ') is called', function() {
                expect(instance.getY()).toEqual(Y);
                expect(instance.setY(newY).getY()).not.toEqual(newY);
            });

            it('Should not return new coord object when calling .getCoord() after .setCoord(' + newX + ',' + newY + ') is called', function() {
                expect(instance.getCoord()).toEqual({
                    x: X,
                    y: Y
                });
                expect(instance.setCoord(newX, newY).getCoord()).not.toEqual({
                    x: newX,
                    y: newY
                });
            });


            //X|Y set MAX cap tests
            it('Should return max capped value (x:' + maxX + ') when calling .setX(' + maxX * 2 + ', true) with exceeded max', function() {
                expect(instance.setX(maxX * 2, true).getX()).toEqual(maxX);
            });

            it('Should return max capped value (y:' + maxY + ') when calling .setY(' + maxY * 2 + ', true) with exceeded max', function() {
                expect(instance.setY(maxY * 2, true).getY()).toEqual(maxY);
            });

            it('Should return max capped values (y:' + maxY + ', x:' + maxX + ') when calling .setCoord(' + maxX * 2 + ', ' + maxY * 2 + ', true) with exceeded max', function() {
                expect(instance.setCoord(maxX * 2, maxY * 2, true).getCoord()).toEqual({
                    x: maxX,
                    y: maxY
                });
            });

            //X|Y set MAX cap tests (false tests)
            it('Should not return max capped value (x:' + maxX + ') when calling .setX(' + maxX * 2 + ') with exceeded max', function() {
                expect(instance.setX(maxX * 2).getX()).not.toEqual(maxX);
            });

            it('Should not return max capped value (y:' + maxY + ') when calling .setY(' + maxY * 2 + ') with exceeded max', function() {
                expect(instance.setY(maxY * 2).getY()).not.toEqual(maxY);
            });

            it('Should not return max capped values (y:' + maxY + ', x:' + maxX + ') when calling .setCoord(' + maxX * 2 + ', ' + maxY * 2 + ') with exceeded max', function() {
                expect(instance.setCoord(maxX * 2, maxY * 2).getCoord()).not.toEqual({
                    x: maxX,
                    y: maxY
                });
            });

            //X|Y set MIN cap tests
            it('Should return max capped value (x:' + minX + ') when calling .setX(' + minX * 2 + ', true) with exceeded max', function() {
                expect(instance.setX(minX * 2, true).getX()).toEqual(minX);
            });

            it('Should return max capped value (y:' + minY + ') when calling .setY(' + minY * 2 + ', true) with exceeded max', function() {
                expect(instance.setY(minY * 2, true).getY()).toEqual(minY);
            });

            it('Should return max capped values (y:' + minY + ', x:' + minX + ') when calling .setCoord(' + minX * 2 + ', ' + minY * 2 + ', true) with exceeded max', function() {
                expect(instance.setCoord(minX * 2, minY * 2, true).getCoord()).toEqual({
                    x: minX,
                    y: minY
                });
            });

            //X|Y set MIN cap tests (false tests)
            it('Should not return max capped value (x:' + minX + ') when calling .setX(' + minX * 2 + ') with exceeded max', function() {
                expect(instance.setX(minX * 2).getX()).not.toEqual(minX);
            });

            it('Should not return max capped value (y:' + minY + ') when calling .setY(' + minY * 2 + ') with exceeded max', function() {
                expect(instance.setY(minY * 2).getY()).not.toEqual(minY);
            });

            it('Should not return max capped values (y:' + minY + ', x:' + minX + ') when calling .setCoord(' + minX * 2 + ', ' + minY * 2 + ') with exceeded max', function() {
                expect(instance.setCoord(minX * 2, minY * 2).getCoord()).not.toEqual({
                    x: minX,
                    y: minY
                });
            });
        });


        describe('When using MAX caps and snaps APIs', function() {
            beforeEach(beforeStub);

            it('Has to have maxX() defined', function() {
                expect(instance.maxX).toBeDefined();
            });

            it('Has to have maxY() defined', function() {
                expect(instance.maxY).toBeDefined();
            });

            it('Has to have maxCoord() defined', function() {
                expect(instance.maxCoord).toBeDefined();
            });

            //X|Y max snap
            it('Should set X value to max (' + maxX + ') when maxX(true) is called', function() {
                expect(instance.maxX(true).getX()).toEqual(maxX);
            });

            it('Should set Y value to max (' + maxY + ') when maxY(true) is called', function() {
                expect(instance.maxY(true).getY()).toEqual(maxY);
            });


            it('Should set coordinates to max (x:' + maxX + ', y:' + maxY + ') when maxCoord(true) is called', function() {
                expect(instance.maxCoord(true).getCoord()).toEqual({
                    x: maxX,
                    y: maxY
                });
            });

             //X|Y max snap
            it('Should not set X value to max (' + maxX + ') when maxX() is called', function() {
                expect(instance.maxX().getX()).not.toEqual(maxX);
            });

            it('Should not set Y value to max (' + maxY + ') when maxY() is called', function() {
                expect(instance.maxY().getY()).not.toEqual(maxY);
            });


            it('Should not set coordinates to max (x:' + maxX + ', y:' + maxY + ') when maxCoord() is called', function() {
                expect(instance.maxCoord().getCoord()).not.toEqual({
                    x: maxX,
                    y: maxY
                });
            });
        });


        describe('When using MIN caps and snaps APIs', function() {
            beforeEach(beforeStub);

            it('Has to have minX() defined', function() {
                expect(instance.minX).toBeDefined();
            });

            it('Has to have minY() defined', function() {
                expect(instance.minY).toBeDefined();
            });

            it('Has to have minCoord() defined', function() {
                expect(instance.minCoord).toBeDefined();
            });

            //X|Y min snap
            it('Should set X value to min (' + minX + ') when minX(true) is called', function() {
                expect(instance.minX(true).getX()).toEqual(minX);
            });

            it('Should set Y value to min (' + minY + ') when minY(true) is called', function() {
                expect(instance.minY(true).getY()).toEqual(minY);
            });

            it('Should set coordinates to min (x:' + minX + ', y:' + minY + ') when minCoord(true) is called', function() {
                expect(instance.minCoord(true).getCoord()).toEqual({
                    x: minX,
                    y: minY
                });
            });

            //X|Y min snap
            it('Should not set X value to min (' + minX + ') when minX() is called', function() {
                expect(instance.minX().getX()).not.toEqual(minX);
            });

            it('Should not set Y value to min (' + minY + ') when minY() is called', function() {
                expect(instance.minY().getY()).not.toEqual(minY);
            });

            it('Should not set coordinates to min (x:' + minX + ', y:' + minY + ') when minCoord() is called', function() {
                expect(instance.minCoord().getCoord()).not.toEqual({
                    x: minX,
                    y: minY
                });
            });
        });

        describe('When using Reset APIs', function() {
            beforeEach(beforeStub);

            it('Has to have resetX() defined', function() {
                expect(instance.resetX).toBeDefined();
            });

            it('Has to have resetY() defined', function() {
                expect(instance.resetY).toBeDefined();
            });

            it('Has to have resetCoord() defined', function() {
                expect(instance.resetCoord).toBeDefined();
            });

            //X|Y reset tests
            it('Should return insatnce when .resetX() is called', function() {
                expect(instance.resetX()).toEqual(instance);
            });

            it('Should return insatnce when .resetY() is called', function() {
                expect(instance.resetY()).toEqual(instance);
            });

            it('Should return insatnce when .resetCoord() is called', function() {
                expect(instance.resetCoord()).toEqual(instance);
            });

            //X|Y reset tests
            it('Should set X value to original value of (' + X + ') after .resetX(true) is called', function() {
                expect(instance.setX(newX, true).resetX(true).getX()).toEqual(X);
            });

            it('Should set Y value to original value of (' + Y + ') after .resetY(true) is called', function() {
                expect(instance.setY(newY, true).resetY(true).getY()).toEqual(Y);
            });

            it('Should set coordinates values to original values of (x:' + X + ', y:' + Y + ') after .resetCoord(true) is called', function() {
                expect(instance.setCoord(newX, newY, true).resetCoord(true).getCoord()).toEqual({
                    x: X,
                    y: Y
                });
            });

            //X|Y reset tests
            it('Should not set X value to original value of (' + X + ') after .resetX() is called', function() {
                expect(instance.setX(newX, true).resetX().getX()).not.toEqual(X);
            });

            it('Should not set Y value to original value of (' + Y + ') after .resetY() is called', function() {
                expect(instance.setY(newY, true).resetY().getY()).not.toEqual(Y);
            });

            it('Should not set coordinates values to original values of (x:' + X + ', y:' + Y + ') after .resetCoord() is called', function() {
                expect(instance.setCoord(newX, newY, true).resetCoord().getCoord()).not.toEqual({
                    x: X,
                    y: Y
                });
            });
        });

        describe('When using Translate APIs', function() {
            beforeEach(beforeStub);

            it('Has to have translateX() defined', function() {
                expect(instance.translateX).toBeDefined();
            });

            it('Has to have translateY() defined', function() {
                expect(instance.translateY).toBeDefined();
            });

            it('Has to have translateCoord() defined', function() {
                expect(instance.translateCoord).toBeDefined();
            });

            //X|Y translate tests
            it('Should set X value to X + newX when calling .translateX(' + newX / 2 + ', true) is called', function() {
                expect(instance.setX(newX / 2, true).translateX(newX, true).getX()).toEqual(newX * 1.5);
            });

            it('Should set Y value to Y + newY when calling .translateY(' + newY / 2 + ', true) is called', function() {
                expect(instance.setY(newY / 2, true).translateY(newY, true).getY()).toEqual(newY * 1.5);
            });

            it('Should set coordinates values to X value to X + newX and Y value to Y + newY when translateCoord(' + newX / 2 + ', ' + newY / 2 + ', true) is called', function() {
                expect(instance.setCoord(newX / 2, newY / 2, true).translateCoord(newX, newY, true).getCoord()).toEqual({
                    x: newX * 1.5,
                    y: newY * 1.5
                });
            });

             //X|Y translate tests (false tests)
            it('Should not set X value to X + newX when calling .translateX(' + newX / 2 + ') is called', function() {
                expect(instance.setX(newX / 2, true).translateX(newX).getX()).not.toEqual(newX * 1.5);
            });

            it('Should not set Y value to Y + newY when calling .translateY(' + newY / 2 + ') is called', function() {
                expect(instance.setY(newY / 2, true).translateY(newY).getY()).not.toEqual(newY * 1.5);
            });

            it('Should not set coordinates values to X value to X + newX and Y value to Y + newY when translateCoord(' + newX / 2 + ', ' + newY / 2 + ') is called', function() {
                expect(instance.setCoord(newX / 2, newY / 2, true).translateCoord(newX, newY).getCoord()).not.toEqual({
                    x: newX * 1.5,
                    y: newY * 1.5
                });
            });
        });

        describe('When using getEnd*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have getEndX() defined', function() {
                expect(instance.getEndX).toBeDefined();
            });

            it('Has to have getEndY() defined', function() {
                expect(instance.getEndY).toBeDefined();
            });

            it('Has to have getEndCoord() defined', function() {
                expect(instance.getEndCoord).toBeDefined();
            });

            //X|Y end Curve tests
            it('Should return end value of X curve when .getEndX() is called', function() {
                expect(instance.setX(newX).getEndX()).toEqual(newX);
            });

            it('Should return end value of Y curve when .getEndY() is called', function() {
                expect(instance.setY(newY).getEndY()).toEqual(newY);
            });

            it('Should return end value of X and Y curves when .getEndCoord() is called', function() {
                expect(instance.setCoord(newX, newY).getEndCoord()).toEqual({
                    x: newX,
                    y: newY
                });
            });
        });

        describe('When using getStart*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have getStartX() defined', function() {
                expect(instance.getStartX).toBeDefined();
            });

            it('Has to have getStartY() defined', function() {
                expect(instance.getStartY).toBeDefined();
            });

            it('Has to have getStartCoord() defined', function() {
                expect(instance.getStartCoord).toBeDefined();
            });

            //X|Y end Curve tests
            it('Should return start value of X curve (' + X + ') when .getStartX() is called', function() {
                expect(instance.setX(newX).getStartX()).toEqual(X);
            });

            it('Should return start value of Y curve (' + Y + ') when .getStartY() is called', function() {
                expect(instance.setY(newY).getStartY()).toEqual(Y);
            });

            it('Should return start values of X|Y curves (x:' + X + ', y:' + Y + ') when .getStartCoord() is called', function() {
                expect(instance.setCoord(newX, newY).getStartCoord()).toEqual({
                    x: X,
                    y: Y
                });
            });
        });

        describe('When using end*() APIs', function() {
            beforeEach(beforeStub);

            it('Has to have endX() defined', function() {
                expect(instance.endX).toBeDefined();
            });

            it('Has to have endY() defined', function() {
                expect(instance.endY).toBeDefined();
            });

            it('Has to have endCoord() defined', function() {
                expect(instance.endCoord).toBeDefined();
            });

            //test returns
            it('Should return instance when .endX() is called', function() {
                expect(instance.endX()).toEqual(instance);
            });

            it('Should return instance when .endY() is called', function() {
                expect(instance.endY()).toEqual(instance);
            });

            it('Should return instance when .endCoord() is called', function() {
                expect(instance.endCoord()).toEqual(instance);
            });

            //test APIs
            it('Should return end value of X curve when .getX() is called after .endX() is called', function() {
                expect(instance.setX(newX).endX().getX()).toEqual(newX);
            });

            it('Should return end value of Y curve when .getY() is called after .endY() is called', function() {
                expect(instance.setY(newY).endY().getY()).toEqual(newY);
            });

            it('Should return end values of X,Y curve when .getCoord() is called after .endCoord() is called', function() {
                expect(instance.setCoord(newX, newY).endCoord().getCoord()).toEqual({
                    x: newX,
                    y: newY
                });
            });
        });
    });
});
