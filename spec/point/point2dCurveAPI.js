/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'p!-curves', 'lib/point2d'], function($numerus, $curves, $point2d) {
    'use strict';

    var curveDuration = 50;
    var curve = $curves.bezier(null, curveDuration);
    var maxX = 1000;
    var minX = -1000;
    var maxY = 500;
    var minY = -500;
    var translateX = 50;
    var translateY = 50;
    var X = 222;
    var Y = 444;
    var newX = maxX / 2;
    var newY = maxY / 2;
    var numX = $numerus.number(X, minX, maxX, curve);
    var numY = $numerus.number(Y, minY, maxY, curve);
    var instance;



    describe('When using $point2d APIs with curving values', function() {
        describe('using setters and retrieving values on curve end', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end value of X (' + newX + ') while calling .getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(newX);
                __done();
            });

            it('Should return end value of Y (' + newY + ') while calling .getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(newY);
                __done();
            });

            it('Should return end values of coord object {x:' + newX + ', y:' + newY + '} while calling  getCoord()', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: newX,
                    y: newY
                });
                __done();
            });
        });

        describe('using setters and retrieving values in a mid of curve', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not return end value of X (' + newX + ') while calling .getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).not.toEqual(newX);
                __done();
            });

            it('Should not return end value of Y (' + newY + ') while calling .getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).not.toEqual(newY);
                __done();
            });

            it('Should not return end values of coord object {x:' + newX + ', y:' + newY + '} while calling  getCoord()', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).not.toEqual({
                    x: newX,
                    y: newY
                });
                __done();
            });
        });

        describe('using max*() setters', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.maxCoord();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return max value of X (' + maxX + ') after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(maxX);
                __done();
            });

            it('Should return max value of Y (' + maxY + ') after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(maxY);
                __done();
            });

            it('Should return max values coord object {x:' + maxX + ', y:' + maxY + '} while calling  getCoord() after  after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: maxX,
                    y: maxY
                });
                __done();
            });
        });

        describe('using min*() setters', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.minCoord();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return min value of X (' + minX + ') after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(minX);
                __done();
            });

            it('Should return min value of Y (' + minY + ') after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(minY);
                __done();
            });

            it('Should return min values coord object {x:' + minX + ', y:' + minY + '} while calling  getCoord() after  after curve duration is expired', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: minX,
                    y: minY
                });
                __done();
            });
        });

        describe('using reset*() get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY, true).resetCoord();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should set X to original value (' + X + ')', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(X);
                __done();
            });

            it('Should set Y to original value (' + Y + ')', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(Y);
                __done();
            });

            it('Should set original values coord object {x:' + X + ', y:' + Y + '}', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: X,
                    y: Y
                });
                __done();
            });
        });

        describe('using reset*() get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY, true).resetCoord();
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not have X to equal to original value (' + X + ')', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).not.toEqual(X);
                __done();
            });

            it('Should not have Y to original value (' + Y + ')', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).not.toEqual(Y);
                __done();
            });

            it('Should not have original values coord object {x:' + X + ', y:' + Y + '}', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).not.toEqual({
                    x: X,
                    y: Y
                });
                __done();
            });
        });


        describe('using translate*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.translateCoord(translateX, translateY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return translated X value (' + (X + translateX) + ') when calling getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(X + translateX);
                __done();
            });

            it('Should return translated Y value (' + (Y + translateY) + ') when calling getY()', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(Y + translateY);
                __done();
            });

            it('Should return translated values coord object {x:' + (X + translateX) + ', y:' + (Y + translateY) + '} while calling  getCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: X + translateX,
                    y: Y + translateY
                });
                __done();
            });
        });

        describe('using translate*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.translateCoord(translateX, translateY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should not have end translated X value (' + (X + translateX) + ') when calling getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).not.toEqual(X + translateX);
                __done();
            });

            it('Should not have end translated Y value (' + (Y + translateY) + ') when calling getY()', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).not.toEqual(Y + translateY);
                __done();
            });

            it('Should not have end translated values coord object {x:' + (X + translateX) + ', y:' + (Y + translateY) + '} while calling  getCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).not.toEqual({
                    x: X + translateX,
                    y: Y + translateY
                });
                __done();
            });
        });

        describe('using getEnd*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end X value (' + newX + ') when calling getEndX()', function(__done) {
                instance.updateCoord();
                expect(instance.getEndX()).toEqual(newX);
                __done();
            });

            it('Should return end Y value (' + newY + ') when calling getEndY()', function(__done) {
                instance.updateCoord();
                expect(instance.getEndY()).toEqual(newY);
                __done();
            });

            it('Should return end values coord object {x:' + newX + ', y:' + newY + '} while calling  getEndCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getEndCoord()).toEqual({
                    x: newX,
                    y: newY
                });
                __done();
            });
        });

        describe('using getEnd*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should return end X value (' + newX + ') when calling getEndX()', function(__done) {
                instance.updateCoord();
                expect(instance.getEndX()).toEqual(newX);
                __done();
            });

            it('Should return end Y value (' + newY + ') when calling getEndY()', function(__done) {
                instance.updateCoord();
                expect(instance.getEndY()).toEqual(newY);
                __done();
            });

            it('Should return end values coord object {x:' + newX + ', y:' + newY + '} while calling  getEndCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getEndCoord()).toEqual({
                    x: newX,
                    y: newY
                });
                __done();
            });
        });


        describe('using getStart*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return original X value (' + X + ') when calling getStartX()', function(__done) {
                instance.updateCoord();
                expect(instance.getStartX()).toEqual(X);
                __done();
            });

            it('Should return original Y value (' + Y + ') when calling getStartY()', function(__done) {
                instance.updateCoord();
                expect(instance.getStartY()).toEqual(Y);
                __done();
            });

            it('Should return original values coord object {x:' + X + ', y:' + Y + '} while calling  getStartCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getStartCoord()).toEqual({
                    x: X,
                    y: Y
                });
                __done();
            });
        });

        describe('using getStart*() - get values in the mid of curve', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY);
                setTimeout(__done, curveDuration / 2);
            });

            //X|Y set->get tests
            it('Should return original X value (' + X + ') when calling getStartX()', function(__done) {
                instance.updateCoord();
                expect(instance.getStartX()).toEqual(X);
                __done();
            });

            it('Should return original Y value (' + Y + ') when calling getStartY()', function(__done) {
                instance.updateCoord();
                expect(instance.getStartY()).toEqual(Y);
                __done();
            });

            it('Should return original values coord object {x:' + X + ', y:' + Y + '} while calling  getStartCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getStartCoord()).toEqual({
                    x: X,
                    y: Y
                });
                __done();
            });
        });

        describe('using end*() - get values after curve had expired', function() {
            beforeEach(function(__done) {
                instance = $point2d(numX.clone(), numY.clone());
                instance.setCoord(newX, newY).endCoord();
                setTimeout(__done, curveDuration);
            });

            //X|Y set->get tests
            it('Should return end X value (' + newX + ') when calling getX()', function(__done) {
                instance.updateCoord();
                expect(instance.getX()).toEqual(newX);
                __done();
            });

            it('Should return end Y value (' + newY + ') when calling getY()', function(__done) {
                instance.updateCoord();
                expect(instance.getY()).toEqual(newY);
                __done();
            });

            it('Should return end values coord object {x:' + newX + ', y:' + newY + '} while calling  getCoord() ', function(__done) {
                instance.updateCoord();
                expect(instance.getCoord()).toEqual({
                    x: newX,
                    y: newY
                });
                __done();
            });
        });
    });
});
