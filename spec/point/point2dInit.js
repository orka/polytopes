/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['p!-numerus', 'lib/point2d'], function($numerus, $point2d) {
    'use strict';



    describe('When instantiating 2D Point object', function() {

        it('$point2d Class should exist', function() {
            expect($point2d).toBeDefined();
        });

        describe('When using constructor method $point2d(Numerus, Numerus)', function() {
            function create() {
                return $point2d.bind(null, arguments[0], arguments[1]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if numbers supplied', function() {
                expect(create(1, 2)).toThrow();
                expect(create(1.1, 2.2)).toThrow();
                expect(create(Infinity)).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when only one Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should not throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create($numerus.number(1), $numerus.number(2))();
                expect(_instance.getX()).toEqual(1);
                expect(_instance.getY()).toEqual(2);
            });
        });


        describe('When using constructor method $point2d.fromCoord(#,#)', function() {

            function create() {
                return $point2d.fromCoord.bind(null, arguments[0], arguments[1]);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Object|Array|String supplied', function() {
                expect(create({}, {})).toThrow();
                expect(create([], [])).toThrow();
                expect(create('qw', 'er')).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0), 1)).toThrow();
                expect(create(1, $numerus.number(0))).toThrow();
            });

            it('Should throw when both Numerus objects supplied', function() {
                expect(create($numerus.number(0), $numerus.number(0))).toThrow();
            });

            it('Should not throw if numbers supplied', function() {
                expect(create(1, 2)).not.toThrow();
                expect(create(1.1, 2.2)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(1, 2)();
                expect(_instance.getX()).toEqual(1);
                expect(_instance.getY()).toEqual(2);
            });
        });


        describe('When using constructor method $point2d.fromBounds({})', function() {
            var _bounds = {
                x: 0,
                y: 0
            };

            function create(__bounds) {
                return $point2d.fromBounds.bind(null, __bounds);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create([])).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_bounds)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_bounds)();
                expect(_instance.getX()).toEqual(_bounds.x);
                expect(_instance.getY()).toEqual(_bounds.y);
            });
        });

        describe('When using constructor method $point2d.fromArray([])', function() {
            var _array = [1, 2];

            function create(__array) {
                return $point2d.fromArray.bind(null, __array);
            }

            it('Should throw if no arguments were supplied', function() {
                expect(create()).toThrow();
            });

            it('Should throw if Number|Array|String|Function supplied', function() {
                expect(create(1)).toThrow();
                expect(create({})).toThrow();
                expect(create('qw')).toThrow();
                expect(create(create)).toThrow();
            });

            it('Should throw when Numerus objects supplied', function() {
                expect(create($numerus.number(0))).toThrow();
            });

            it('Should throw if invalid bounds object supplied', function() {
                expect(create({})).toThrow();
            });

            it('Should not throw if bounds object supplied', function() {
                expect(create(_array)).not.toThrow();
            });

            it('Should have X, Y values equal to arguments supplied', function() {
                var _instance = create(_array)();
                expect(_instance.getX()).toEqual(_array[0]);
                expect(_instance.getY()).toEqual(_array[1]);
            });
        });

        describe('When using deconstructor() method', function() {
            var instance;

            beforeEach(function() {
                instance = $point2d($numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.deconstructor).toBeDefined();
            });

            it('Should derefference .x, .y', function() {
                instance.deconstructor();
                expect(instance.x).toEqual(null);
                expect(instance.y).toEqual(null);
            });
        });


        describe('When using clone() method', function() {
            var instance;

            beforeEach(function() {
                instance = $point2d($numerus.number(0), $numerus.number(0));
            });

            it('Should be defined', function() {
                expect(instance.clone).toBeDefined();
            });

            it('Should create new instance with original properies of Point', function() {
                var _new = instance.clone();
                expect(_new === instance).toEqual(false);
                expect(_new.y === instance.y).toEqual(true);
                expect(_new.x === instance.x).toEqual(true);
                expect(_new.__yNumerus === instance.__yNumerus).toEqual(false);
                expect(_new.__xNumerus === instance.__xNumerus).toEqual(false);
                expect($point2d.isOffspring(_new)).toEqual(true);
            });
        });
    });
});
