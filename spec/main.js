//must specify current path
//this is done to ensure a standalone package dependancies and tests
//are found by requirejs
this.$PROJECT_NAME = 'polytopes';

require.config({
    map: {
        '*': {
            p: 'packages/requirejs/src/package',
            json: 'packages/requirejs/src/json',
            text: 'packages/requirejs/src/text'
        }
    },
    paths: {
        lib: '../lib',
        packages: '../packages'
    }
});
/**
 * Spec must be wrapped in a define function
 * then be followed by a window.onload() to trigger jasmine tests
 * window.onload() is a hack around jasmin not being triggerred when using requirejs
 * firt args array is the specs to be executed
 */
define(
    [
        'point/point2dInit',
        'point/point2dAPI',
        'point/point2dCurveAPI',
        'size/size2dInit',
        'size/size2dAPI',
        'size/size2dCurveAPI',
        'position/position2dInit',
        'position/position2dAPI',
        'position/position2dCurveAPI',
        'rectangle/rectangleInit',
        'rectangle/rectangleSimpleGetters'
    ],
    function() {
        'use strict';
        window.onload();
    });
